import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {

  invokeFirstComponentFunction = new EventEmitter();
  invokeLoadComponentFunction = new EventEmitter();
  recentModulComponentFunction = new EventEmitter();
  public dataPerusahaanTitleEvent = new EventEmitter();
  subsVar: Subscription;

  constructor() { }

  onFirstComponentButtonClick(nameUrl) {
    this.invokeFirstComponentFunction.emit(nameUrl);
  }
  onFirstComponentLoad(nameUrl) {
    this.invokeLoadComponentFunction.emit(nameUrl);
  }
  onRecentModul(nameUrl) {
    this.recentModulComponentFunction.emit(nameUrl);
  }
  changeTitle(title: string) {
    this.dataPerusahaanTitleEvent.emit(title);
  }
}