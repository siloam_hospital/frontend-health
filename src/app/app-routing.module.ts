// Angular
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
// Components
import { BaseComponent } from "./views/theme/base/base.component";
import { ErrorPageComponent } from "./views/theme/content/error-page/error-page.component";
// Auth
import { AuthGuard } from "./core/auth";
import { UserResolver } from "./core/auth/_data-sources/user.resolver";

const routes: Routes = [{
		path: "privacy-policy",
		loadChildren: () =>
			import(
				"./views/pages/public/privacy-policy/privacy-policy.module"
			).then((m) => m.PrivacyPolicyModule),
	},
	{
		path: "support",
		loadChildren: () =>
			import("./views/pages/public/support/support.module").then(
				(m) => m.SupportModule
			),
	},
	{
		path: "",
		component: BaseComponent,
		children: [{
				path: "",
				loadChildren: () =>
					import("./views/pages/home/home.module").then(
						(m) => m.HomeModule
					),
			},
			{
				path: "error",
				loadChildren: () =>
					import("./views/pages/error/error.module").then(
						(m) => m.ErrorModule
					),
			},
			{
				path: "",
				redirectTo: "home",
				pathMatch: "full"
			},
			{
				path: "**",
				redirectTo: "error",
				pathMatch: "full"
			},

		],
	},

	{
		path: "**",
		redirectTo: "error/403",
		pathMatch: "full"
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {
		relativeLinkResolution: 'corrected',
	})],
	exports: [RouterModule],
})
export class AppRoutingModule {}
