import { Injectable, OnDestroy } from '@angular/core';
import { CiseaHttpContract } from '../models';
import { HttpClient } from '@angular/common/http';
import { EncrDecrService } from '../../../EncrDecr/encr-decr.service';
import { environment as env } from '../../../../../environments/environment';
import { LocationService } from './location.service';
import { Subscription, BehaviorSubject, of, Observable } from 'rxjs';
import { map, distinctUntilChanged, switchMap, catchError, tap, takeWhile, filter } from 'rxjs/operators';

interface MinifiedCoord {
  lat: number;
  lon: number;
}
export interface CurrentWeatherType {
  locationId: number;
  locationName: string;
  utcTime: string | Date;
  utcSunrise: string | Date;
  utcSunset: string | Date;
  temp: number;
  feelsLike: number;
  pressure: number;
  humidity: number;
  dewPoint: number;
  clouds: number;
  uvIndex: number;
  visibility: number;
  windSpeed: number;
  windGust?: number;
  windDeg: number;
  rain?: number;
  snow?: number;
  weatherSummary: string;
  weatherDescription: string;
  weatherIcon: string;
}
export interface HourlyForecast {
  utcTime: string | Date;
  temp: number;
  feelsLike: number;
  pressure: number;
  humidity: number;
  dewPoint: number;
  clouds: number;
  uvIndex: number;
  visibility: number;
  windSpeed: number;
  windGust?: number;
  windDeg: number;
  probabilityofPrecipitation: number;
  rain?: number;
  snow?: number;
  weatherSummary: string;
  weatherDescription: string;
  weatherIcon: string;
}
export interface DailyForecast {
  utcTime: string | Date;
  utcSunrise: string | Date;
  utcSunset: string | Date;
  utcMoonrise: string | Date;
  utcMoonset: string | Date;
  moonPhase: number;
  tempMorn: number;
  tempDay: number;
  tempEve: number;
  tempNight: number;
  tempMin: number;
  tempMax: number;
  feelsLikeMorn: number;
  feelsLikeDay: number;
  feelsLikeEve: number;
  feelsLikeNight: number;
  pressure: number;
  humidity: number;
  dewPoint: number;
  windSpeed: number;
  windGust?: number;
  windDeg: number;
  clouds: number;
  uvIndex: number;
  probabilityOfPrecipitation: number;
  rain?: number;
  snow?: number;
  weatherSummary: string;
  weatherDescription: string;
  weatherIcon: string;
}

@Injectable({
  providedIn: 'root'
})
export class WeatherService extends CiseaHttpContract implements OnDestroy {
  private _subscriptions = new Array<Subscription>();
  private set subscription(value: Subscription) {
    this._subscriptions.push(value);
  }

  private _savedLocationId: number | null = null;
  private _current = new BehaviorSubject<CurrentWeatherType>(null);
  private get currentWeather() {
    return this._current.getValue();
  }
  private set currentWeather(value: CurrentWeatherType) {
    this._current.next(value);
  }
  public get currentWeather$() {
    return this._current.asObservable();
  }

  constructor(
    protected client: HttpClient,
    protected ed: EncrDecrService,
    private locSvc: LocationService,
  ) {
    super(client, ed);
    this.host = env.APIUrl + 'weather-manager';
    this.subscription = this.locSvc.position$.pipe(
      // tap(v => console.log('Loc:', v)),
      filter(v => v !== null),
      map<Position, MinifiedCoord>(v => ({ lat: v.coords.latitude, lon: v.coords.longitude })),
      distinctUntilChanged((p, c) => JSON.stringify(p) === JSON.stringify(c)),
      // tap(v => console.log('GeoPipe:', v)),
      takeWhile(v => v !== null),
      switchMap(v => this.getCurrentWeather(v.lat, v.lon).pipe(tap(x => {
        if (x) {
          this._savedLocationId = x.locationId;
        }
      }), catchError(() => of<CurrentWeatherType | null>(null)))),
    ).subscribe(v => {
      this.currentWeather = v;
    });
  }

  public getCurrentWeather(lat: number, lon: number) {
    return this.get<CurrentWeatherType>('/current', { lat: lat.toString(), lon: lon.toString() }).pipe(map(v => v.data));
  }

  public getForecast(scope: 'hourly'): Observable<HourlyForecast[]>
  public getForecast(scope: 'daily'): Observable<DailyForecast[]>
  public getForecast(scope: 'hourly' | 'daily'): Observable<(HourlyForecast | DailyForecast)[]> {
    return this.get<(HourlyForecast | DailyForecast)[]>(`/forecast/${this._savedLocationId}/${scope}`).pipe(map(v => v.data));
  }

  ngOnDestroy() {
    for (let subs of this._subscriptions) {
      subs.unsubscribe();
    }
  }
}
