import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService implements OnDestroy {
  private _watcher: number | null = null;
  private _posPtr = new BehaviorSubject<Position>(null);
  private get position() {
    return this._posPtr.getValue();
  }
  private set position(value: Position) {
    if ((this.position === null && value !== null) || (this.position !== null && value ===null) || (this.position !== null && value !== null && JSON.stringify(this.position.coords) === JSON.stringify(value.coords))) {
      this._posPtr.next(value);
    }
  }
  public get position$() {
    return this._posPtr.asObservable()
  }
  public get isStarted() {
    return this._watcher !== null;
  }

  public startWatch() {
    if ('geolocation' in navigator) {
      if (this._watcher !== null) {
        throw new ReferenceError('Watcher already started');
      }
      this._watcher = navigator.geolocation.watchPosition(pos => {
        this.position = pos;
      }, e => {
        // console.warn('Location Error:', e);
      }, {
        enableHighAccuracy: false,
        timeout: 5000,
        maximumAge: 0,
      });
    } else {
      throw new ReferenceError('Geolocation is not available');
    }
  }

  public stopWatch() {
    if ('geolocation' in navigator && this._watcher !== null) {
      navigator.geolocation.clearWatch(this._watcher);
    }
  }

  public ngOnDestroy() {
    this._posPtr.complete();
    this.stopWatch();
  }
}
