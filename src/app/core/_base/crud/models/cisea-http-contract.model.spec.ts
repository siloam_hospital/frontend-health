import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { EncrDecrService } from '../../../EncrDecr/encr-decr.service';
import { CiseaHttpContract } from './cisea-http-contract.model';

class MockContract extends CiseaHttpContract { }

describe('CiseaHttpContract', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [ HttpClient, EncrDecrService ] });
  });

  it('should create an instance', () => {
    const http: HttpClient = TestBed.get(HttpClient);
    const encr: EncrDecrService = TestBed.get(EncrDecrService);
    expect(new MockContract(http, encr)).toBeTruthy();
  });
});
