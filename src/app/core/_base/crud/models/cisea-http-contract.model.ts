import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { environment as env } from "../../../../../environments/environment";
import { EncrDecrService } from "../../../EncrDecr/encr-decr.service";

export type Envelope<T = any> = {
	message: string;
	status: boolean;
	data: T;
	response_code?: number;
};
type Page<T = any> = {
	total: number;
	pagedData: T[];
};
type QueryOptions<T = any> = {
	page: number;
	perPage: number;
	sortBy: string;
	sortDirection: 'ASC' | 'DESC' | null;
	filterBy: Partial<T>;
};

export abstract class CiseaHttpContract {
    constructor(
        protected client: HttpClient,
        protected ed: EncrDecrService,
    ) { }

    protected host: string =  env.APIUrl;

    private errorHandler<T = any>(defaultValue: T = null) {
        return catchError<Envelope<T>, Observable<Envelope<T>>>((e: HttpErrorResponse) => {
            throw {
                status: false,
                data: defaultValue,
                message: env.production ? 'Error': ((e instanceof HttpErrorResponse) ? e.error : e),
                response_code: (e instanceof HttpErrorResponse) ? e.status : 500,
            };
        });
    }

    protected generateForm(pairs: { key: string; value: string | Blob }[]) {
      const fd = new FormData();
      for (let n of pairs) {
        fd.set(n.key, n.value);
      }
      return fd;
    }

    protected objTrimStr<T = any>(obj: T): T {
      switch (typeof obj) {
        case 'string': return obj.trim() as unknown as T;
        case 'object': {
          for (let k in obj) {
            if ((typeof obj[k]) === 'string') {
              obj[k] = (obj[k] as unknown as string).trim() as unknown as T[Extract<keyof T, string>];
            } else if ((typeof obj[k]) === 'object') {
              if (Array.isArray(obj[k])) {
                obj[k] = (obj[k] as unknown as any[]).map(v => this.objTrimStr(v)) as unknown as T[Extract<keyof T, string>];
              } else {
                obj[k] = this.objTrimStr(obj[k]);
              }
            }
          }
          return obj;
        }
        default: return obj;
      }
    }

    protected getPage<T = any, Q = any>(endpoint: string, params?: Partial<QueryOptions<Q>>, optionalParams: {[key: string]: string | string[]} = {}, optionalHeader?: HttpHeaders): Observable<Envelope<Page<T>>> {
        params = params || {};
        const loadedOptions: QueryOptions<Q> = {
          page: params.page || 1,
          perPage: params.perPage || 10,
          sortBy: params.sortBy || null,
          sortDirection: params.sortDirection || null,
          filterBy: params.filterBy || null,
        };
        let p = new HttpParams()
          .set('_page', loadedOptions.page.toString())
          .set('_perPage', loadedOptions.perPage.toString());
        if (loadedOptions.sortBy && loadedOptions.sortDirection) {
          p = p
            .set('_sortBy', loadedOptions.sortBy)
            .set('_sortDir', loadedOptions.sortDirection);
        }
        if (loadedOptions.filterBy) {
          for (const k in loadedOptions.filterBy) {
            if (loadedOptions.filterBy.hasOwnProperty(k)) {
              p = p.set(`${k}_like`, loadedOptions.filterBy[k].toString());
            }
          }
        }
        for (const k in optionalParams) {
          const currentPtr = optionalParams[k];
          if (Array.isArray(currentPtr)) {
            for (let n of currentPtr) {
              p = p.append(k, n);
            }
          } else {
            p = p.set(k, currentPtr);
          }
        }
        return this.client.get<string>(this.host + endpoint, { headers: { ...optionalHeader}, params: p }).pipe(
          map(res => this.objTrimStr<Envelope<Page<T>>>(this.ed.response(res))),
          this.errorHandler<Page<T>>({ total: 0, pagedData: [] }),
        );
      }
    
      protected get<T = any>(endpoint: string, optionalParams: {[key: string]: string | string[]} = {}, optionalHeader?: HttpHeaders): Observable<Envelope<T>> {
        return this.client.get<string>(this.host + endpoint, { headers: { ...optionalHeader}, params: optionalParams }).pipe(
          map(res => this.objTrimStr<Envelope<T>>(this.ed.response(res))),
          this.errorHandler<T>(),
        );
      }
    
      protected post<T = any>(endpoint: string, data: any, optionalHeader?: HttpHeaders): Observable<Envelope<T>> {
        const targetHost = data instanceof FormData ? this.host.split('/').map(v => v === 'v2' ? 'v1' : v).join('/') : this.host;
        return this.client.post<string>(
          targetHost + endpoint,
          targetHost.split('/').includes('v2') ? { data: this.ed.bodyEncrypt(data, true) } : data,
          { headers: { ...optionalHeader} }
        ).pipe(
          map(res => this.objTrimStr<Envelope<T>>(this.ed.response(res))),
          this.errorHandler<T>(),
        )
      }
    
      protected put<T = any>(endpoint: string, data: any, optionalHeader?: HttpHeaders): Observable<Envelope<T>> {
        const targetHost = data instanceof FormData ? this.host.split('/').map(v => v === 'v2' ? 'v1' : v).join('/') : this.host;
        return this.client.put<string>(
          targetHost + endpoint,
          targetHost.split('/').includes('v2') ? { data: this.ed.bodyEncrypt(data, true) } : data,
          { headers: { ...optionalHeader} }
        ).pipe(
          map(res => this.objTrimStr<Envelope<T>>(this.ed.response(res))),
          this.errorHandler<T>(),
        )
      }
    
      protected patch<T = any>(endpoint: string, data: any, optionalHeader?: HttpHeaders) {
        const targetHost = data instanceof FormData ? this.host.split('/').map(v => v === 'v2' ? 'v1' : v).join('/') : this.host;
        return this.client.patch<string>(
          targetHost + endpoint,
          targetHost.split('/').includes('v2') ? { data: this.ed.bodyEncrypt(data, true) } : data,
          { headers: { ...optionalHeader} }
        ).pipe(
          map(res => this.objTrimStr<Envelope<T>>(this.ed.response(res))),
          this.errorHandler<T>(),
        )
      }
    
      protected delete<T = any>(endpoint: string, optionalParams: {[key: string]: string} = {}, optionalHeader?: HttpHeaders): Observable<Envelope<T>> {
        let p = new HttpParams();
        for (const k in optionalParams) {
          p = p.set(k, optionalParams[k]);
        }
        return this.client.delete<string>(this.host + endpoint, { headers: { ...optionalHeader}, observe: 'response' }).pipe(
          map(res => ({ ...this.objTrimStr<Envelope<T>>(this.ed.response(res.body)), response_code: res.status })),
          this.errorHandler<T>(),
        );
      }
    
      protected head(endpoint: string, optionalParams: {[key: string]: string} = {}, optionalHeader?: HttpHeaders) {
        return this.client.head(this.host + endpoint, { headers: { ...optionalHeader}, params: optionalParams, observe: 'response' });
      }
}
