// Angular
import { Injectable } from '@angular/core';
// RxJS
import { BehaviorSubject } from 'rxjs';
// Object path
import * as objectPath from 'object-path';
// Services
import { MenuConfigService } from './menu-config.service';

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, map } from 'rxjs/operators';
import { environment } from "../../../../../environments/environment";
import { EncrDecrService } from "../../../EncrDecr/encr-decr.service";
@Injectable()
export class MenuAsideService {
	httpOptions: any = [];
	apiToken: any;
	pathname = window.location.pathname.split("/");
	// Public properties
	menuList$: BehaviorSubject < any[] > = new BehaviorSubject < any[] > ([]);

	/**
	 * Service constructor
	 *
	 * @param menuConfigService: MenuConfigService
	 */
	constructor(private menuConfigService: MenuConfigService, private http: HttpClient, private EncrDecr: EncrDecrService) {
		this.loadMenu();
	}

	/**
	 * Load menu list
	 */
	loadMenu() {
		// get menu list
		let get_magnet = this.get("magnet/master/unit").toPromise();
		let get_lbm = this.get("lbm/menu_location").toPromise();
		Promise.all([get_magnet,get_lbm]).then(res => {		
			
			this.createMenu(res[0],res[1]);
		})
	}
	createMenu(res,res_lbm) {
		const menuItems: any[] = objectPath.get(this.menuConfigService.getMenus(), 'aside.items');
		Object.keys(res['data']).forEach(key => {
			if (res['data'][key]['access']) {
				menuItems.push({
					title: res['data'][key]['name'],
					root: true,
					icon: "fa fa-map-marker",
					page: "/magnet/district/"+res['data'][key]['id'],
					bullet: "dot",
					module: "magnet",
					prod: true,
				});
			}
		})
		Object.keys(res_lbm['data']).forEach(key => {
			let tamp = {
				title: res_lbm['data'][key]['nama_departemen'].replace('Penambangan',''),
				root: true,
				icon: "fa fa-map-marker",
				page: "/log-book-digital/district/"+res_lbm['data'][key]['id'],
				bullet: "dot",
				module: "log-book-digital",
				prod: true
			};
			if(typeof res_lbm['data'][key]['submenu'] !=='undefined'){
				Object.keys(res_lbm['data'][key]['submenu']).forEach(key2 => {
					if(typeof tamp['submenu'] == 'undefined'){
						tamp['submenu']=[];
					}
					tamp['submenu'].push({
						title: res_lbm['data'][key]['submenu'][key2]['name'],
						page:"/log-book-digital/district/"+res_lbm['data'][key]['id']+"/"+res_lbm['data'][key]['submenu'][key2]['id_master'],
					});
				})
			}
			menuItems.push(tamp);
		})
		let dataTamp = [];
		if (window.location.hostname != 'localhost') {
			if (this.pathname[1] === 'health') {
				Object.keys(menuItems).forEach(key => {
					if (menuItems[key]['prod']) {
						dataTamp.push(menuItems[key]);
					}
				})
			} else {
				dataTamp = menuItems;
			}
		} else {
			dataTamp = menuItems;
		}
		this.menuList$.next(dataTamp);
	}
	get(endPoint): Observable < any > {
		return this.http.get < any > (environment.APIUrl + endPoint, this.httpOptions).pipe(
			map((res) => {
				return this.EncrDecr.response(res);
			}),
			catchError(err => {
				return err;
			})
		);
	}
}
