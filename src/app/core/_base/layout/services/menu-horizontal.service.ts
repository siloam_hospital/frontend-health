// Angular
import { Injectable } from '@angular/core';
// RxJS
import { BehaviorSubject } from 'rxjs';
// Object path
import * as objectPath from 'object-path';
// Services
import { MenuConfigService } from './menu-config.service';

@Injectable()
export class MenuHorizontalService {
	// Public properties
	menuList$: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

	/**
	 * Service constructor
	 *
	 * @param menuConfigService: MenuConfigService
	 */
	constructor(private menuConfigService: MenuConfigService) {
		this.loadMenu();
	}

	/**
	 * Load menu list
	 */
	loadMenu() {
		// get menu list
		//const menuItems: any[] = objectPath.get(this.menuConfigService.getMenus(), 'header.items');
		// let menuItems: any[] = [
		// 	{
		// 		title: 'Welcome',
		// 		root: true,
		// 		icon: 'fa fa-home',
		// 		alignment: 'left',
		// 		page: '/home',
		// 	},
		// 	{
		// 		title: 'Legis',
		// 		root: true,
		// 		alignment: 'left',
		// 		page: '/legis'
		// 	},
		// 	{
		// 		title: 'Legis2',
		// 		root: true,
		// 		alignment: 'left',
		// 		page: '/legis'
		// 	}
		// ];
		// localStorage.setItem('menuItem', JSON.stringify(menuItems));
		let menuItem = JSON.parse(localStorage.getItem('menuItem'));
		this.menuList$.next(menuItem);
	}
}
