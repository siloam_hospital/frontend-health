// Angular
import { Injectable } from '@angular/core';
// RxJS
import { Subject } from 'rxjs';
import { MenuConfiguration, MenuItem } from '../../../_config/menu.config';

@Injectable()
export class MenuConfigService {
	// Public properties
	onConfigUpdated$: Subject<MenuConfiguration>;
	// Private properties
	private menuConfig: MenuConfiguration;

	/**
	 * Service Constructor
	 */
	constructor() {
		// register on config changed event and set default config
		this.onConfigUpdated$ = new Subject<MenuConfiguration>();
	}

	/**
	 * Returns the menuConfig
	 */
	getMenus() {
		return this.menuConfig;
	}

	/**
	 * Get MenuItem Reference from current path, return null if path not found or disabled.
	 * 
	 * Warning! Modify this References means you edit the real one! Always make a clone when needed!
	 * @param path Menu path (e.g. /modulename/subpath/../anotherpath)
	 * @returns Menu Item Reference
	 */
	getAsideMenuReferenceByPath(path: string) {
		return this.recursiveSearch(path, this.menuConfig.aside.items);
	}

	isExist(path: string) {
		return this.recursiveSearch(path, this.menuConfig.aside.items) !== null;
	}

	private recursiveSearch(path: string, hive: MenuItem[]): MenuItem {
		let found: boolean = false;
		let i: number = 0;
		let result: MenuItem = null;
		while (!found && hive.length > i) {
			if (hive[i].page === path) {
				found = true;
				result = hive[i];
			} else {
				if (hive[i].submenu) {
					result = this.recursiveSearch(path, hive[i].submenu);
					found = result !== null;
				}
				i += 1;
			}
		}
		return result;
	}

	/**
	 * Programmatically add menu on runtime
	 * @param menu Menu to be added or updated if exists
	 * @param parent Set null to set it as root. Otherwise, newly menu will be set as child of parent
	 */
	addMenu(menu: MenuItem, parent: string = null) {
		if (parent) {
			let parentRef = this.recursiveSearch(parent, this.menuConfig.aside.items);
			let oldChildRef = this.recursiveSearch(menu.page, parentRef.submenu);
			if (oldChildRef) {
				let nRef = oldChildRef.submenu && menu.submenu ? [...oldChildRef.submenu, ...menu.submenu] : (oldChildRef.submenu && !menu.submenu ? oldChildRef.submenu : (!oldChildRef.submenu && menu.submenu ? menu.submenu : null));
				oldChildRef = { ...oldChildRef, ...menu, submenu: nRef };
			} else {
				if (!parentRef.submenu) {
					parentRef.submenu = [];
				}
				parentRef.submenu.push(menu);
			}
		} else {
			let oldChildRef = this.recursiveSearch(menu.page, this.menuConfig.aside.items);
			if (oldChildRef) {
				let nRef = oldChildRef.submenu && menu.submenu ? [...oldChildRef.submenu, ...menu.submenu] : (oldChildRef.submenu && !menu.submenu ? oldChildRef.submenu : (!oldChildRef.submenu && menu.submenu ? menu.submenu : null));
				oldChildRef = { ...oldChildRef, ...menu, submenu: nRef };
			} else {
				if (!this.menuConfig.aside.items) {
					this.menuConfig.aside.items = [];
				}
				this.menuConfig.aside.items.push(menu);
			}
		}
		this.onConfigUpdated$.next(this.menuConfig);
	}

	/**
	 * Recuresively remove path (and its children)
	 * @param path Path to be removed
	 * @returns Detached Menu Item
	 */
	removeMenu(path: string | RegExp) {
		let result = this.recursiveRemove(path, this.menuConfig.aside.items);
		this.menuConfig.aside.items = result.hive;
		this.onConfigUpdated$.next(this.menuConfig);
		return result.result;
	}

	private recursiveRemove(path: string | RegExp, hive: MenuItem[]): { result:MenuItem; hive: MenuItem[] } {
		let result: MenuItem = null;
		hive = hive.filter((v) => {
			let matcher = new RegExp(path);
			if (matcher.test(v.page)) {
				result = v;
				return false;
			} else {
				return true;
			}
		});
		if (result) {
			return { result, hive };
		} else {
			return hive.map(v => v.submenu ? this.recursiveRemove(path, v.submenu) : null).reduce((p, c) => {
				let d = parseInt([p ? p.result : null, c.result].map(v => v !== null ? '1' : '0').join(''), 2);
				switch (d) {
					case 3: return c;
					case 2: return p;
					case 1: return c;
					default: return null;
				}
			}, null);
		}
	}

	/**
	 * Load config
	 *
	 * @param {MenuConfiguration} config: Menu Configuration
	 */
	loadConfigs(config: MenuConfiguration) {
		this.menuConfig = config;
		this.onConfigUpdated$.next(this.menuConfig);
	}
}
