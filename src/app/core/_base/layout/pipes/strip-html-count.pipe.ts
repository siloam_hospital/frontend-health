import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stripHtmlCount'
})
export class StripHtmlCountPipe implements PipeTransform {

  transform(value: any): any {
    return value.replace(/<.*?>/g, '').length;
  }

}
