import {
    Directive,
    ElementRef,
    HostListener,
    Input,
    AfterViewInit,
    OnInit,
    OnDestroy,
    TemplateRef,
    ViewContainerRef,
    ComponentFactoryResolver,
    ComponentFactory,
} from "@angular/core";
import { BehaviorSubject, Subscription, fromEvent } from 'rxjs';
import { NoticeComponent } from '../../../../views/partials/content/general/notice/notice.component';
import { distinctUntilChanged } from 'rxjs/operators';

export enum OrientationVisibility {
    XS = 'xs',
    SM = 'sm',
    MD = 'md',
    LG = 'lg',
    XL = 'xl',
}

@Directive({
    selector: '[gOrientationVisibility]'
})
export class OrientationDirective implements OnDestroy, AfterViewInit, OnInit {
    @Input() gOrientationVisibility: OrientationVisibility = OrientationVisibility.MD;

    widthLimit: {[key in OrientationVisibility]: number;} = {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200,
    };

    elementCondition = new BehaviorSubject<number>(1366);
    elementCondition$ = this.elementCondition.asObservable();
    elementConditionSubscription: Subscription;
    lockElement = new BehaviorSubject<boolean>(false);
    lockElement$ = this.lockElement.asObservable();
    lockElementSubscription: Subscription;
    resizer: Subscription;
    staticFactory: ComponentFactory<NoticeComponent>;
    androidLink = 'https://play.google.com/store/apps/details?id=';


    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private factoryResolver: ComponentFactoryResolver,
    ) {
        // this.detectWindowSize();
        this.staticFactory = this.factoryResolver.resolveComponentFactory(NoticeComponent);
        
        this.elementConditionSubscription = this.elementCondition$.subscribe(currentWidth => {
            // console.log('Condition:', { condition, visibleOn: this.gOrientationVisibility });
            this.lockElement.next(currentWidth < this.widthLimit[this.gOrientationVisibility]);
        });
        const alertMsg = document.createElement('div');
        alertMsg.className = 'text-center alert alert-warning';
        alertMsg.style.display = 'block';
        alertMsg.innerHTML = `<h6 class="alert-heading">You cannot view this content using current orientation!</h6>
        <p>
            Please switch your <strong>orientation</strong>,
            Open this page on proper device,
            or use <a href="${this.androidLink}">Mobile Apps</a> to access this content properly.
        </p>`;
        this.lockElementSubscription = this.lockElement$.pipe(distinctUntilChanged()).subscribe(lockCurrentElement => {
            // console.log('Lock Mode:', { lockCurrentElement, visibleOn: this.gOrientationVisibility });
            this.viewContainer.clear();
            if (lockCurrentElement) {
                this.viewContainer.createComponent(this.staticFactory, 0, undefined, [
                    [alertMsg],
                ]);
            } else {
                this.viewContainer.createEmbeddedView(this.templateRef);
            }
        });
        this.resizer = fromEvent(window, 'resize').subscribe(() => {
            this.detectWindowSize();
        });
    }

    ngOnInit() {
        this.detectWindowSize();
    }

    ngAfterViewInit() {
        this.detectWindowSize();
    }

    detectWindowSize() {
        let base = getComputedStyle(document.documentElement);
        let width = parseInt(base.width, 10);
        // let height = parseInt(base.height, 10);
        // console.log('Status:', { navi: window.navigator });
        this.elementCondition.next(width);
    }

    ngOnDestroy() {
        this.elementCondition.complete();
        this.lockElement.complete();
        this.resizer.unsubscribe();
        if (this.elementConditionSubscription) {
            this.elementConditionSubscription.unsubscribe();
        }
        if (this.lockElementSubscription) {
            this.lockElementSubscription.unsubscribe();
        }
    }
}
