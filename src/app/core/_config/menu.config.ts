import { getMatIconFailedToSanitizeLiteralError } from "@angular/material/icon";
import {
	RulesConstant as RULES
} from "../../core/auth/_permissions/rules.constant";
export type MenuItem = {
	title?: string;
	root?: boolean;
	icon?: string;
	page?: string;
	badge?: {};
	bullet?: string;
	module?: string;
	prod?: boolean;
	alignment?: string;
	rules?: number | boolean;
	submenu?: MenuItem[];
	img?: string;
	width?: string;
};
export type MenuConfiguration = {
	header: {
		self: {};
		items: {
			title: string;
			root: boolean;
			icon?: string;
			alignment: string;
			page: string;
		}[];
	};
	aside: {
		self: {};
		items: MenuItem[];
	};
};

let SpendAccessLevel: any = localStorage.getItem("Spend_my_level_access") ? localStorage.getItem("Spend_my_level_access").split(',') : '';
export class MenuConfig {
	//menu kiri
	public defaults: MenuConfiguration = {
		header: {
			self: {},
			items: [{
				title: "Welcome",
				root: true,
				icon: "fa fa-home",
				alignment: "left",
				page: "/home",
			},
				{
					title: "Legis",
					root: true,
					alignment: "left",
					page: "/legis",
				},
				{
					title: "Eis",
					root: true,
					alignment: "left",
					page: "/eis",
				},
			],
		},
		aside: {
			self: {},
			items: [{
				title: "DASHBOARD",
				root: true,
				icon: "fa fa-th-large",
				page: "/legis/dashboard",
				bullet: "dot",
				module: "legis",
				prod: true,
			},
				{
					title: "TO DO LIST",
					root: true,
					icon: "fa fa-clipboard-check",
					page: "/legis/todo-list",
					bullet: "dot",
					module: "legis",
					prod: true,
				},
				{
					title: "TASK",
					root: true,
					icon: "fa fa-tasks",
					alignment: "left",
					bullet: "dot",
					module: "legis",
					rules: RULES.LEGIS_TASK_ACCESS,
					prod: true,
					submenu: [{
						title: "Top Down",
						page: "/legis/task/top-down",
						rules: RULES.LEGIS_TASK_TOP_DOWN_ACCESS,
					},
						{
							title: "Bottom Up",
							page: "/legis/task/bottom-up",
							rules: RULES.LEGIS_TASK_BOTTOM_UP_ACCESS,
						},
					],
				},
				{
					title: "ELSA",
					page: "/legis/elsa-dashboard",
					root: true,
					icon: "fa fa-tachometer-alt",
					alignment: "left",
					module: "legis",
					rules: RULES.LEGIS_ELSA_ACCESS,
					prod: true,
				},
				{
					title: "VIEW LARS",
					page: "/legis/lars",
					root: true,
					icon: "fa fa-sitemap",
					alignment: "left",
					module: "legis",
					prod: true,
				},
				{
					title: "LARS",
					page: "/legis/lars",
					root: true,
					icon: "fa fa-cog",
					alignment: "left",
					bullet: "dot",
					module: "legis",
					rules: RULES.LEGIS_LARS_ACCESS,
					prod: true,
					submenu: [{
						title: "Sejarah",
						page: "/legis/lars/sejarah",
						rules: RULES.LEGIS_LARS_SEJARAH_ACCESS,
					},
						{
							title: "Due Diligence",
							page: "/legis/lars/create-due-diligence",
							rules: RULES.LEGIS_LARS_DUE_DILIGENCE_ACCESS,
						},
						{
							title: "Approval Due Diligence",
							page: "/legis/lars/due-diligence",
							rules: RULES.LEGIS_LARS_DUE_DILIGENCE_APPROVAL,
						},
						{
							title: "Legal Audit",
							page: "/legis/lars/legal-audit-admin-spv",
							rules: RULES.LEGIS_LARS_LEGAL_AUDIT_ACCESS,
						},
						{
							title: "Approval Legal Audit",
							page: "/legis/lars/legal-audit",
							rules: RULES.LEGIS_LARS_LEGAL_AUDIT_APPROVAL,
						},
						{
							title: "Compliance",
							page: "/legis/lars/compliance-admin-spv",
							rules: RULES.LEGIS_LARS_COMPLIANCE_ACCESS,
						},
						{
							title: "Approval Compliance",
							page: "/legis/lars/compliance",
							rules: RULES.LEGIS_LARS_COMPLIANCE_APPROVAL,
						},
						{
							title: "Data Storage",
							page: "/legis/lars/data-storage",
							rules: RULES.LEGIS_LARS_DATA_STORAGE_ACCESS,
						},
					],
				},
				{
					title: "REGULATION",
					page: "/legis/regulation",
					root: true,
					icon: "fa fa-folder-plus",
					alignment: "left",
					module: "legis",
					rules: RULES.LEGIS_REGULATION_ACCESS,
					prod: true,
				},
				{
					title: "LIST REGULATION",
					icon: "fa fa-list-alt",
					page: "/legis/list-regulation",
					root: true,
					alignment: "left",
					module: "legis",
					prod: true,
				},
				{
					title: "MASTER DATA",
					root: true,
					icon: "fa fa-database",
					alignment: "left",
					bullet: "dot",
					module: "legis",
					rules: RULES.LEGIS_MASTER_DATA_ACCESS,
					prod: true,
					submenu: [
						// {
						// 	title: "Satuan Kerja",
						// 	page: "/legis/master-data/satuan-kerja"
						// },
						{
							title: "Jenis Pekerjaan",
							page: "/legis/master-data/jenis-pekerjaan",
						},
						{
							title: "Jenis Regulation",
							page: "/legis/master-data/jenis-regulation",
						},
						{
							title: "Aspek Regulation",
							page: "/legis/master-data/aspek-regulation",
						},
						{
							title: "Penerbit Regulation",
							page: "/legis/master-data/penerbit-regulation",
						},
						{
							title: "Aspek Lars",
							page: "/legis/master-data/aspek-lars",
						},
						{
							title: "Sub Aspek Lars",
							page: "/legis/master-data/sub-aspek-lars",
						},
						{
							title: "Jenis Sub Aspek",
							page: "/legis/master-data/jenis-sub-aspek",
						},
						{
							title: "Aspek Compliance",
							page: "/legis/master-data/aspek-compliance",
						},
						{
							title: "Jenis Compliance",
							page: "/legis/master-data/jenis-compliance",
						},
						{
							title: "Bidang Usaha",
							page: "/legis/master-data/bidang-usaha",
						},
						{
							title: "Perusahaan",
							page: "/legis/master-data/perusahaan",
						},
						// {
						// 	title: "Sejarah Perusahaan",
						// 	page: "/legis/master-data/sejarah-perusahaan"
						// }
					],
				},
				{
					title: "USER GUIDE",
					page: "/legis/panduan",
					root: true,
					icon: "fa fa-file-pdf",
					alignment: "left",
					module: "legis",
					prod: true,
				},
				// EIS
				{
					title: "EXSUM",
					root: true,
					icon: "fa fa-th-large",
					page: "/eis/dashboard",
					bullet: "dot",
					module: "eis",
					rules: RULES.EIS_EXSUM_ACCESS,
					prod: true,
				},
				// {
				// 	title: "Stats",
				// 	root: true,
				// 	icon: "fa fa-chart-line",
				// 	page: "/eis/stats",
				// 	bullet: "dot",
				// 	module: "eis",
				// 	rules: RULES.EIS_STATS_ACCESS,
				// },
				{
					title: "HPP",
					root: true,
					icon: "fas fa-area-chart",
					page: "/eis/hpp",
					bullet: "dot",
					module: "eis",
					rules: RULES.EIS_HPP,
					prod: true,
				},
				{
					title: "Produksi",
					root: true,
					icon: "fas fa-cog",
					page: "/eis/produksi",
					bullet: "dot",
					module: "eis",
					rules: RULES.EIS_PRODUKSI_ACCESS,
					prod: true,
				},

				// {
				// 	title: "Proper",
				// 	root: true,
				// 	icon: "fas fa-cog",
				// 	page: "/eis/proper",
				// 	bullet: "dot",
				// 	module: "eis",
				// 	rules: RULES.EIS_PRODUKSI_ACCESS,
				// 	prod: false,
				// },
				{
					title: "Angkutan",
					root: true,
					//icon: "fas fa-bus-alt",
					img: "assets/media/icons/modules/Angbat_menu.png",
					width: "25px",
					page: "/eis/angkutan",
					bullet: "dot",
					module: "eis",
					rules: RULES.EIS_ANGKUTAN_ACCESS,
					prod: true,
				},
				{
					title: "Keuangan",
					root: true,
					icon: "fas fa-dollar-sign",
					page: "/eis/keuangan",
					bullet: "dot",
					module: "eis",
					rules: RULES.EIS_KEUANGAN_ACCESS,
					prod: true,
				},
				{
					title: "Pemasaran",
					root: true,
					icon: "fas fa-chart-bar",
					page: "/eis/pemasaran",
					bullet: "dot",
					module: "eis",
					rules: RULES.EIS_PEMASARAN_ACCESS,
					prod: true,
				},
				{
					title: "SDM",
					root: true,
					icon: "fas fa-user-friends",
					page: "/eis/sdm",
					bullet: "dot",
					module: "eis",
					rules: RULES.EIS_SDM_ACCESS,
					prod: true,
				},
				// Link Slip Gaji
				{
					title: "Dashboard",
					root: true,
					icon: "fa fa-home",
					page: "/slip-gaji/dashboard",
					bullet: "dot",
					module: "slip-gaji",
					prod: true,
				},
				{
					title: "Dashboard",
					root: true,
					icon: "fa fa-home",
					page: "/spt/dashboard",
					bullet: "dot",
					module: "spt",
					prod: true,
				},
				// {
				// 	title: "Dashboard",
				// 	root: true,
				// 	icon: "fa fa-th-large",
				// 	page: "/non-swakelola/dashboard",
				// 	bullet: "dot",
				// 	module: "non-swakelola",
				//	prod:false
				// },
				// {
				// 	title: "Produk Tanah",
				// 	root: true,
				// 	icon: "fas fa-cog",
				// 	page: "/non-swakelola/produk-tanah",
				// 	bullet: "dot",
				// 	module: "non-swakelola",
				//	prod:false
				// },
				// {
				// 	title: "Produk Batubara",
				// 	root: true,
				// 	icon: "fas fa-bus-alt",
				// 	page: "/non-swakelola/produk-batubara",
				// 	bullet: "dot",
				// 	module: "non-swakelola",
				//	prod:false
				// },
				{
					title: "Jam Jalan",
					root: true,
					// icon: "fas fa-dollar-sign",
					icon: "fas fa-snowplow",
					// img : "assets/media/icons/modules/PNS.png",
					// width : "25px",
					page: "/non-swakelola/jam-jalan",
					bullet: "dot",
					module: "non-swakelola",
					prod: true,
					submenu: [{
						title: "Data Jam Jalan",
						page: "/non-swakelola/jam-jalan/report-jam-jalan",
					},
						{
							title: "Input Jam Jalan",
							page: "/non-swakelola/jam-jalan/data-jam-jalan",
						},
					],
				},
				// {
				// 	title: "Dashboard",
				// 	root: true,
				// 	icon: "fas fa-chart-area",
				// 	page: "/epa/dashboard",
				// 	bullet: "dot",
				// 	module: "epa",
				// 	prod: true,
				// },
				{
					title: "Jam Jalan",
					root: true,
					icon: "fas fa-snowplow",
					page: "/epa/dashboard",
					bullet: "dot",
					module: "epa",
					prod: true,
					submenu: [{
						title: "Dashboard",
						page: "/epa/dashboard",
					},
						{
							title: "Data Jam Jalan",
							page: "/epa/data",
						},
						{
							title: "History",
							page: "/epa/history/jam-jalan",
						},
						// {
						// 	title: "Laporan",
						// 	page: "/epa/laporan",
						// }
						{
							title: "Laporan",
							page: "/epa/laporan",
							bullet: "dot",
							module: "epa",
							prod: true,
							submenu: [{
								title: "Create Laporan",
								page: "/epa/laporan",
							},
								{
									title: "History Laporan",
									page: "/epa/laporan-history"
								}
							]
						},
						{
							title: "Penggantian Unit",
							page: "/epa/penggantian-unit"
						},

					],
				},
				{
					title: "Hambatan",
					root: true,
					icon: "fas fa-stopwatch",
					page: "/epa/obstacle/add",
					bullet: "dot",
					module: "epa",
					prod: true,
					submenu: [{
						title: "Dashboard",
						page: "/epa/obstacle/add",
					},
						{
							title: "Data Hambatan",
							page: "/epa/obstacle/report",
						},
						{
							title: "History",
							page: "/epa/history/hambatan",
						},
					],
				},
				{
					title: "Ritase",
					root: true,
					icon: "fas fa-truck-monster",
					page: "/epa/ritase/add-data",
					bullet: "dot",
					module: "epa",
					prod: true,
					submenu: [{
						title: "Dashboard",
						page: "/epa/ritase/dashboard"
					},
						{
							title: "Data Ritase",
							page: "/epa/ritase/add-data"
						},
						{
							title: "History",
							page: "/epa/history/ritase"
						},
					]
				},
				// {
				// 	title: "Manajemen Kontrak",
				// 	root: true,
				// 	icon: "fas fa-file-contract",
				// 	page: "/epa/manajemen-kontrak/",
				// 	module:"epa",
				// 	prod: false,
				// 	submenu: [
				// 		{
				// 			title:"Penggantian Unit",
				// 			page:"/epa/manajemen-kontrak/penggantian-unit"
				// 		},
				// 	]
				// },
				{
					title: "Data Master",
					root: true,
					icon: "fas fa-database",
					page: "/epa/data-master/" || "epa/datamaster/",
					module: "epa",
					prod: true,
					submenu: [{
						title: "Unit Kontrak",
						page: "/epa/datamaster/unit-kontrak"
					},
						{
							title: "Unit Assignment",
							page: "/epa/datamaster/assign-unit"
						},
						{
							title: "Satuan Kerja",
							page: "/epa/data-master/satker",
						},

						{
							title: "Jenis Material",
							page: "/epa/data-master/kualitas",
						},



						{
							title: "Company",
							page: "/epa/data-master/company",
						},
						{
							title: "Pengawas",
							page: "/epa/data-master/pengawas",
						},
						{
							title: "Group Kerja",
							bullet: "dot",
							submenu: [{
								title: "Shift",
								page: "/epa/data-master/shift",
							},
								{
									title: "Groups",
									page: "/epa/data-master/group",
								},
							]
						},
						{
							title: "Equipment",
							bullet: "dot",
							submenu: [{
								title: "Merk",
								page: "/epa/data-master/merk",
							},
								{
									title: "Jenis",
									page: "/epa/data-master/jenis",
								},
								{
									title: "Tipe Model",
									page: "/epa/datamaster/tipe",
								},
								{
									title: "Model",
									page: "/epa/datamaster/model",
								},
								{
									title: "Equipment",
									page: "/epa/datamaster/equipment"
								},
								{
									title: "Hauler",
									page: "/epa/data-master/hauler",
								},
							],
						},

						{
							title: "Lokasi Kerja",
							bullet: "dot",
							page: "/epa/data-master/lokasi",
							submenu: [{
								title: "Lokasi",
								page: "/epa/data-master/lokasi",
							},
								{
									title: "Distrik",
									page: "/epa/data-master/distrik",
								},
								{
									title: "Area",
									page: "/epa/data-master/area",
								},
								{
									title: "Blok",
									page: "/epa/data-master/blok",
								},
								{
									title: "Lokasi Loading",
									page: "/epa/data-master/loading",
								},
								{
									title: "Lokasi Dumping",
									page: "/epa/data-master/dumping",
								},
								{
									title: "Seam",
									page: "/epa/data-master/seam",
								},
							]
						},

						{
							title: "SPPH",
							page: "/epa/datamaster/spph",
						},
						{
							title: "Loss Time",
							bullet: "dot",
							submenu: [
								{
									title: "Tipe Loss Time",
									page: "/epa/datamaster/tipe-losstime"
								},
								{
									title: "Kategori Loss Time",
									page: "/epa/datamaster/kategori-losstime",
								},
								{
									title: "Nama Loss Time",
									page: "/epa/datamaster/nama-losstime"
								},

							]

						}
					],
				},
				{
					title: "Data Master",
					root: true,
					icon: "fas fa-database",
					page: "/non-swakelola/data-master",
					bullet: "dot",
					module: "non-swakelola",
					prod: true,
					rules: RULES.MRBA_PNS_ADMIN,
					submenu: [{
						title: "Company Profile",
						page: "/non-swakelola/data-master/company-profile",
					},
						{
							title: "Kontrak",
							page: "/non-swakelola/data-master/kontrak",
						},
						{
							title: "Tipe Alat",
							page: "/non-swakelola/data-master/tipe-alat",
						},
						{
							title: "Alat",
							page: "/non-swakelola/data-master/alat",
						},
						{
							title: "Koreksi",
							page: "/non-swakelola/data-master/koreksi",
						},
						{
							title: "Lokasi",
							page: "/non-swakelola/data-master/lokasi",
						},
					],
				},

				//intax
				/*
			{
				title: "Dashboard",
				root: true,
				icon: "fas fa-th-large",
				page: "/in-tax/dashboard",
				bullet: "dot",
				module: "in-tax",
				prod: false,
			},
			*/
				{
					title: "Todo List",
					root: true,
					icon: "fas fa-clipboard-check",
					page: "/in-tax/todolist",
					bullet: "dot",
					module: "in-tax",
					prod: true,
				},
				{
					title: "Master Data",
					root: true,
					icon: "fas fa-database",
					page: "/in-tax/jenispph",
					bullet: "dot",
					module: "in-tax",
					prod: true,
					submenu: [{
						title: "Jenis Pajak",
						page: "/in-tax/jenispph",
						prod: true,
					},
						/*{
					title: "Payset",
					page: "/in-tax/payset",
					prod: false
				},*/
						{
							title: "Mapping PO",
							page: "/in-tax/mappingpo",
							prod: true,
						},
						/*{
						title: "Komponen PPH21",
						page: "/in-tax/komponen",
						prod: false
					} */
					],
				},
				/*
			{
				title: "Penerimaan Pajak",
				root: true,
				icon: "fas fa-money-check-alt",
				page: "/in-tax/#",
				bullet: "dot",
				module: "in-tax",
				prod: false,
				submenu: [
					{
						title: "Domestik",
						page: "/in-tax/penerimaan/domestik",
						prod: false,
					},
					{
						title: "Ekspor",
						page: "/in-tax/penerimaan/ekspor",
						prod: false,
					},
				],
			},

			{
				title: "PPH21",
				root: true,
				icon: "fas fa-money-check-alt",
				page: "/in-tax/posisi-pajak",
				bullet: "dot",
				module: "in-tax",
				prod: false,
			},
			*/
				{
					title: "INVOICE",
					root: true,
					icon: "fas fa-file-invoice-dollar",
					page: "/in-tax/invoice",
					bullet: "dot",
					module: "in-tax",
					prod: true,
				},
				{
					title: "Report",
					root: true,
					icon: "fas fa-money-bill-wave",
					page: "/in-tax/setorpajak",
					bullet: "dot",
					module: "in-tax",
					prod: true,
					submenu: [{
						title: "Setor Pajak",
						page: "/in-tax/setorpajak",
						prod: true,
					},
						{
							title: "PPN Masa",
							page: "/in-tax/ppnmasa",
							prod: true,
						},
						{
							title: "PPN Wapu",
							page: "/in-tax/ppnwapu",
							prod: true,
						},
					],
				},
				/*
			{
				title: "Pelaporan",
				root: true,
				icon: "fas fa-file",
				page: "/in-tax/pelaporan",
				bullet: "dot",
				module: "in-tax",
				prod: false,
			},
			*/
				{
					title: "Role Access",
					root: true,
					icon: "fas fa-users",
					page: "/in-tax/roleaccess",
					bullet: "dot",
					module: "in-tax",
					prod: true,
					rules: RULES.ITS_ADMIN_ACCESS != parseInt(localStorage.getItem("In-tax_idHakAkses")),
				},

				//end intax
				// for module SPEND
				{
					title: "Dashboard",
					root: true,
					icon: "fas fa-th-large",
					page: "/spend/dashboard",
					bullet: "dot",
					module: "spend",
					prod: true,
				},

				{
					title: "To do List",
					root: true,
					icon: "fas fa-tasks",
					badge: { type: 'kt-badge--primary', value: localStorage.getItem("Spend_badge_todolist") },
					page: "/spend/todo-list",
					bullet: "dot",
					module: "spend",
					prod: true,
				},
				{
					title: "Delegasi",
					root: true,
					icon: "fas fa-edit",
					page: "/spend/delegation",
					bullet: "dot",
					module: "spend",
					prod: true,
				},

				{
					title: "Commodity Management",
					root: true,
					icon: "fas fa-folder-open",
					page: "/non-swakelola/data-master",
					bullet: "dot",
					module: "spend",
					prod: true,
					submenu: [{
						title: "Commodity Group",
						page: "/spend/comodity/group",
						rules: SpendAccessLevel.indexOf('3') >= 0 ? false : true && SpendAccessLevel.indexOf('4') >= 0 ? false : true && SpendAccessLevel.indexOf('2') >= 0 ? false : true
					},
						{
							title: "Commodity Registration",
							page: "/spend/comodity/registration",
						},
						{
							title: "Commodity Review",
							page: "/spend/comodity/review",
						},
						// {
						// 	title: "Reporting",
						// 	page: "/non-swakelola/data-master/alat"
						// }
					],
				},
				{
					title: "Vendor Management",
					root: true,
					icon: "fas fa-folder-open",
					page: "/spend/vendor-management/vendor-review",
					bullet: "dot",
					module: "spend",
					prod: true,
					submenu: [{
						title: "Vendor Registration",
						page: "/spend/vendor-management/vendor-registration",
					},
						{
							title: "Vendor Review",
							page: "/spend/vendor-management/vendor-review",
						},
						{
							title: "Vendor Monitoring",
							page: "/spend/vendor-management/vendor-monitoring",
						},
						{
							title: "Vendor Performing",
							page: "/spend/vendor-management/vendor-performing",
						},
					],
				},
				{
					title: "Procurement Management",
					root: true,
					icon: "fas fa-folder-open",
					page: "/non-swakelola/data-master",
					bullet: "dot",
					module: "spend",
					prod: true,
					submenu: [
						{
						title: "Purchase Requisition",
						page: "/spend/procurement/purchase-requisition",
						},
						{
							title: "Purchase History",
							page: "/spend/procurement/purchase-history",
						},
						{
							title: "SPPH Review",
							page: "/spend/procurement/spph-review",
						},
						{
							title: "SPPH History",
							page: "/spend/procurement/spph-history",
						},
						{
							title: "e-Auction External",
							page: "/spend/procurement/eauction-eksternal/dashboard",
						},
						{
							title: "e-Auction History",
							page: "/spend/procurement/eauction-eksternal/history",
						},
					],
				},
				{
					title: 'Contract Management',
					root: true,
					icon: 'fas fa-folder-open',
					bullet: 'dot',
					module: 'spend',
					prod: false,
					submenu: [
						{
							title: 'Contracts',
							page: '/spend/contract/dashboard'
						},
						{
							title: 'New Contract',
							page: '/spend/contract/new'
						},
						{
							title: 'Contract Template',
							page: '/spend/contract/template'
						},
						{
							title: 'Monitoring',
							page: '/spend/contract/monitoring'
						},
						{
							title: 'Contracts List',
							page: '/spend/contract/contract-monitoring'
						}
					],
				},
				{
					title: "Reporting",
					root: true,
					icon: "fas fa-file-alt",
					page: "/non-swakelola/data-master",
					bullet: "dot",
					module: "spend",
					prod: true,
					submenu: [{
						title: "Commodity Management",
						page: "/spend/reporting/commodity-management",
					},
						{
							title: "Spend Management System",
							page: "/spend/reporting/vendor-management",
						},
						{
							title: "Procurement Management",
							page: "/spend/reporting/procurement-management",
						},
						{
							title: "Contract Management",
							page: "/spend/reporting/contract-management",
						},
					],
				},
				{
					title: "Spend Analysis",
					root: true,
					icon: "fa fa-chart-line",
					page: "/non-swakelola/data-master",
					bullet: "dot",
					module: "spend",
					prod: true,
					submenu: [{
						title: "Purchase by Vendor",
						page: "/spend/analysis/vendor",
					},
						{
							title: "Purchase by Product",
							page: "/spend/analysis/product",
						},
						{
							title: "Purchase by Period",
							page: "/spend/analysis/period",
						},

					],
				},
				{
					title: "Configuration",
					root: true,
					icon: "fa fa-cog",
					//page: "/non-swakelola/data-master",
					bullet: "dot",
					module: "spend",
					prod: true,
					submenu: [{
						title: "Role Access",
						page: "/spend/role-access",
					},

						{
							title: "Cost Center",
							page: "/spend/cost-center",

						},
						{
							title: "Authority PR",
							page: "/spend/authority-PR",

						},
						{
							title: "Auction Announcement",
							page:"/spend/pengumuman-lelang"
						},
						{
							title: "Update Tanggal",
							page:"/spend/procurement/update-tanggal"
						},
						{
							title: "Hierarchical Process",
							page: "/spend/process",
						}
					]
				},
				// MODULE COAL UPTE
				{
					title: "TOTAL",
					root: true,
					icon: "fas fa-chart-line",
					page: "/coal-upte/total",
					bullet: "dot",
					module: "coal-upte",
					prod: true,
				},
				{
					title: "TAL",
					root: true,
					icon: "fas fa-chart-line",
					page: "/coal-upte/TAL",
					bullet: "dot",
					module: "coal-upte",
					prod: true,
				},
				{
					title: "BANKO",
					root: true,
					icon: "fas fa-chart-line",
					page: "/coal-upte/BANKO",
					bullet: "dot",
					module: "coal-upte",
					prod: true,
				},
				{
					title: "MTB",
					root: true,
					icon: "fas fa-chart-line",
					page: "/coal-upte/MTB",
					bullet: "dot",
					module: "coal-upte",
					prod: true,
				},

				// MODULE OB UPTE
				{
					title: "TOTAL",
					root: true,
					icon: "fas fa-chart-line",
					page: "/ob-upte/total",
					bullet: "dot",
					module: "ob-upte",
					prod: true,
				},
				{
					title: "TAL",
					root: true,
					icon: "fas fa-chart-line",
					page: "/ob-upte/TAL",
					bullet: "dot",
					module: "ob-upte",
					prod: true,
				},
				{
					title: "BANKO",
					root: true,
					icon: "fas fa-chart-line",
					page: "/ob-upte/BANKO",
					bullet: "dot",
					module: "ob-upte",
					prod: true,
				},
				{
					title: "MTB",
					root: true,
					icon: "fas fa-chart-line",
					page: "/ob-upte/MTB",
					bullet: "dot",
					module: "ob-upte",
					prod: true,
				},

				// MODULE ANGBAT
				{
					title: "TOTAL",
					root: true,
					icon: "fas fa-chart-line",
					page: "/angbat/total",
					bullet: "dot",
					module: "angbat",
					prod: true,
				},
				{
					title: "TARAHAN",
					root: true,
					icon: "fas fa-chart-line",
					page: "/angbat/TARAHAN",
					bullet: "dot",
					module: "angbat",
					prod: true,
				},
				{
					title: "KERTAPATI",
					root: true,
					icon: "fas fa-chart-line",
					page: "/angbat/KERTAPATI",
					bullet: "dot",
					module: "angbat",
					prod: true,
				},

				// MODULE PEMASARAN
				{
					title: "TOTAL",
					root: true,
					icon: "fas fa-chart-line",
					page: "/pemasaran/total",
					bullet: "dot",
					module: "pemasaran",
					prod: true,
				},
				{
					title: "KERTAPATI",
					root: true,
					icon: "fas fa-chart-line",
					page: "/pemasaran/KERTAPATI",
					bullet: "dot",
					module: "pemasaran",
					prod: true,
				},
				{
					title: "TARAHAN",
					root: true,
					icon: "fas fa-chart-line",
					page: "/pemasaran/TARAHAN",
					bullet: "dot",
					module: "pemasaran",
					prod: true,
				},
				{
					title: "UPTE",
					root: true,
					icon: "fas fa-chart-line",
					page: "/pemasaran/UPTE",
					bullet: "dot",
					module: "pemasaran",
					prod: true,
				},

				// MODULE SR

				{
					title: "TOTAL",
					root: true,
					icon: "fas fa-chart-line",
					page: "/sr/total",
					bullet: "dot",
					module: "sr",
					prod: true,
				},

				// MODULE FOR ITS (INVOICE TRACKING SYSTEM)

				{
					title: "Dashboard",
					root: true,
					icon: "fas fa-th-large",
					page: "/its/dashboard",
					bullet: "dot",
					module: "its",
					prod: false,
				},

				{
					title: "Access District",
					root: true,
					icon: "fas fa-key",
					page: "/its/access",
					bullet: "dot",
					module: "its",
					prod: true,
					// rules:
					// 	RULES.ITS_ADMIN_ACCESS !=
					// 	parseInt(localStorage.getItem("ITS_idHakAkses"))
				},

				{
					title: "Todo List",
					root: true,
					icon: "fas fa-list-ul",
					page: "/its/todo-list",
					bullet: "dot",
					module: "its",
					prod: true,
					// rules:
					// 	RULES.ITS_ASMEN_ACCESS !=
					// 		parseInt(localStorage.getItem("ITS_idHakAkses")) &&
					// 	RULES.ITS_KEUANGAN_ACCESS !=
					// 		parseInt(localStorage.getItem("ITS_idHakAkses")) &&
					// 	RULES.ITS_OPERATOR_ACCESS !=
					// 		parseInt(localStorage.getItem("ITS_idHakAkses")),
				},
				// {
				// 	title: "Monitoring Invoice",
				// 	root: true,
				// 	icon: "fas fa-search-location",
				// 	page: "/its/monitoring-invoice",
				// 	bullet: "dot",
				// 	module: "its",
				// 	rules:
				// 		RULES.ITS_MANAGER_ACCESS !=
				// 		parseInt(localStorage.getItem("ITS_idHakAkses"))
				// },
				{
					title: "Daftar Invoice",
					root: true,
					icon: "fas fa-file-invoice-dollar",
					page: "/its/invoice",
					bullet: "dot",
					module: "its",
					prod: true,
					// rules:
					// 	RULES.ITS_OPERATOR_ACCESS !=
					// 	parseInt(localStorage.getItem("ITS_idHakAkses"))
				},
				// {
				// 	title: "Disposisi Invoice",
				// 	root: true,
				// 	icon: "fas fa-hand-pointer",
				// 	page: "/its/disposisi-invoice",
				// 	bullet: "dot",
				// 	module: "its",
				// 	rules:
				// 		RULES.ITS_ASMEN_ACCESS !=
				// 		parseInt(localStorage.getItem("ITS_idHakAkses"))
				// },
				// {
				// 	title: "Verifikasi Invoice",
				// 	root: true,
				// 	icon: "fas fa-user-check",
				// 	page: "/its/verifikasi-invoice",
				// 	bullet: "dot",
				// 	module: "its",
				// 	rules:
				// 		RULES.ITS_KEUANGAN_ACCESS !=
				// 		parseInt(localStorage.getItem("ITS_idHakAkses"))
				// },
				{
					title: "Role Access",
					root: true,
					icon: "fas fa-users",
					page: "/its/role-access",
					bullet: "dot",
					module: "its",
					prod: true,
					rules: RULES.ITS_ADMIN_ACCESS !=
						parseInt(localStorage.getItem("ITS_idHakAkses")),
				},

				// MODULE SPEND EXT

				{
					title: "Portal Login",
					root: true,
					icon: "fas fa-sign-in-alt",
					page: "/spend-ext/login",
					bullet: "dot",
					module: "spend-ext",
					prod: true,
				},

				{
					title: "Portal Registrasi",
					root: true,
					icon: "fas fa-user-plus",
					page: "/spend-ext/regis",
					bullet: "dot",
					module: "spend-ext",
					prod: true,
				},

				//Mod
				//MOdule SME
				{
					title: "Dashboard",
					root: true,
					icon: "fa fa-dashboard",
					page: "/sme",
					bullet: "dot",
					module: "sme",
					prod: true,
				},
				{
					title: "Fuel Management",
					root: true,
					icon: "fa fa-bitbucket",
					page: "/sme",
					bullet: "dot",
					module: "sme",
					prod: true,
					submenu: [{
						title: "Monitor BBM",
						icon: "fa fa-truck",
						page: "/sme/fuel-mon-bbm",
					},

					]
				},
				{
					title: "Data Master",
					root: true,
					icon: "fa fa-database",
					page: "/sme",
					bullet: "dot",
					module: "sme",
					prod: true,
					submenu: [{
						title: "Fuel Man Data",
						icon: "fa fa-users",
						page: "/sme/data-fuel-man",
					},
						// {
						// 	title: "Fuel Man Conf",
						// 	icon: "fa fa-truck",
						// 	page: "/sme/conf-fuel-man",
						// },
						{
							title: "Warehouse Conf",
							icon: "fa fa-archive",
							page: "/sme/conf-warehouse"
						},
						{
							title: "Location",
							icon: "fa fa-map-marker",
							page: "/sme/data-location"
						},

					]
				},
				// Module SME END

				//MOdule Perangkat kerja
				{
					title: "Dashboard",
					root: true,
					icon: "fa fa-dashboard",
					page: "/perangkat-kerja",
					bullet: "dot",
					module: "perangkat-kerja",
					prod: false,
				},
				{
					title: "Monitor",
					root: true,
					icon: "fa fa-bitbucket",
					page: "",
					bullet: "dot",
					module: "perangkat-kerja",
					prod: false,
					submenu: [{
						title: "Monitor Perangkat",
						icon: "fa fa-truck",
						page: "/perangkat-kerja/monitor-perangkat",
					},

					]
				},
				{
					title: "Data Master",
					root: true,
					icon: "fa fa-database",
					page: "/perangkat-kerja",
					bullet: "dot",
					module: "perangkat-kerja",
					prod: false,
					submenu: [{
						title: "Konfigurasi Asset",
						icon: "fa fa-bahai",
						page: "",
					}, {
						title: "Tipe Asset",
						icon: "fa fa-users",
						page: "",
					},
						{
							title: "Staff",
							icon: "fa fa-truck",
							page: "",
						},
						{
							title: "Merk",
							icon: "fa fa-truck",
							page: "",
						},
						{
							title: "Jenis Perangkat",
							icon: "fa fa-truck",
							page: "",
						},
						{
							title: "Kontrak",
							icon: "fa fa-truck",
							page: "",
						}

					]
				},
				// Module Perangkat kerja END
				// MODULE LEAVE
				{
					title: "Dashboard",
					root: true,
					icon: "fas fa-user-check",
					page: "/leave",
					bullet: "dot",
					module: "leave",
					prod: false,
				},
				{
					title: "To do list",
					root: true,
					icon: "fa fa-book",
					page: "/leave/todo",
					bullet: "dot",
					module: "leave",
					prod: false,
				},
				{
					title: "History",
					root: true,
					icon: "fas flaticon2-paper",
					page: "/leave/history",
					bullet: "dot",
					module: "leave",
					prod: false,
				},
				{
					title: "Uang Cuti",
					root: true,
					icon: "fa fa-money",
					page: "/leave/leave-uang",
					bullet: "dot",
					module: "leave",
					prod: false,
				},
				{
					title: "Uang Tanpa Cuti",
					root: true,
					icon: "fa fa-money",
					page: "/leave/leave-nocutiuang",
					bullet: "dot",
					module: "leave",
					prod: false,
				},
				{
					title: "Pending",
					root: true,
					icon: "fa fa-files-o",
					page: "/leave/leave-pending",
					bullet: "dot",
					module: "leave",
					prod: false,
				},
				{
					title: "Report",
					root: true,
					icon: "fa fa-book",
					page: "/leave/leave-list",
					bullet: "dot",
					module: "leave",
					prod: false,
				},

				{
					title: "Role Access",
					root: true,
					icon: "fas fa-users",
					page: "/leave/role-access",
					bullet: "dot",
					module: "leave",
					prod: false,
				},
				// {
				// 	title: "Master",
				// 	root: true,
				// 	icon: "fa fa-database",
				// 	page: "/leave/master/",
				// 	bullet: "dot",
				// 	module: "leave",
				// 	prod: false,
				// 	submenu: [
				// 		// {
				// 		// 	title: "Role Access",
				// 		// 	page: "/leave/master/role-access",
				// 		// },
				// 		{
				// 			title: "Reason Leave",
				// 			page: "/leave/master/leave-reason"
				// 		},
				// 		// {
				// 		// 	title: "Pembatalan Leave",
				// 		// 	page: "/leave/master/leave-pembatalan"
				// 		// },
				// 		// {
				// 		// 	title:"List Leave",
				// 		// 	page : "/leave/master/leave-list"
				// 		// }
				// 	]
				// },

				//END MODULE LEAVE
				// Module Admin
				{
					title: "Users",
					root: true,
					icon: "fas fa-users",
					bullet: "dot",
					module: "admin",
					page: "/admin/users",
					prod: true,
				},
				{
					title: "Roles",
					root: true,
					icon: "fas fa-user-cog",
					bullet: "dot",
					module: "admin",
					page: "/admin/roles",
					prod: true,
				},
				{
					title: "Iklan",
					root: true,
					icon: "fas fa-newspaper",
					bullet: "dot",
					module: "admin",
					page: "/admin/iklan",
					prod: true,
				},
				// ------------------------------ TRAVEL -----------------------
				{
					title: "To do list",
					root: true,
					icon: "fas fa-th-list",
					page: "/travel/todolist",
					bullet: "dot",
					module: "travel",
					prod: false,
				},
				{
					title: "Data Pengajuan",
					root: true,
					icon: "fas fa-th-list",
					page: "/travel/data-pengajuan",
					bullet: "dot",
					module: "travel",
					prod: false,
				},

				{
					title: "Pengajuan",
					root: true,
					icon: "fas fa-pencil-alt",
					page: "/travel/pengajuan",
					bullet: "dot",
					module: "travel",
					prod: false,
				},
				{
					title: "Report",
					root: true,
					icon: "fas fa-book",
					page: "/travel/report",
					bullet: "dot",
					module: "travel",
					prod: false,
				},
				{
					title: "History",
					root: true,
					icon: "fas flaticon2-paper",
					page: "/travel/history",
					bullet: "dot",
					module: "travel",
					prod: false,
				},

				{
					title: "Role Access",
					root: true,
					icon: "fas fa-users",
					page: "/travel/role-access",
					bullet: "dot",
					module: "travel",
					prod: false,
				},
				// ------------------------------ TRAVEL END -----------------------
				// ABSENSI
				{
					title: "Dashboard Absensi",
					root: true,
					// icon: "fas fa-dollar-sign",
					icon: "fa fa-th-large",
					// img : "assets/media/icons/modules/PNS.png",
					// width : "25px",
					page: "/absensi/dashboard",
					bullet: "dot",
					module: "absensi",
					prod: true,
				},
				{
					title: "Data Master",
					root: true,
					icon: "fas fa-database",
					page: "/absensi/master-data",
					bullet: "dot",
					module: "absensi",
					prod: true,
					submenu: [{
						title: "Lokasi Absen",
						page: "/absensi/master-data/lokasi-absen",
					},
						// {
						// 	title: "Kontrak",
						// 	page: "/non-swakelola/data-master/kontrak",
						// },
						// {
						// 	title: "Tipe Alat",
						// 	page: "/non-swakelola/data-master/tipe-alat",
						// },
						// {
						// 	title: "Alat",
						// 	page: "/non-swakelola/data-master/alat",
						// },
						// {
						// 	title: "Koreksi",
						// 	page: "/non-swakelola/data-master/koreksi",
						// },
						// {
						// 	title: "Lokasi",
						// 	page: "/non-swakelola/data-master/lokasi",
						// },
					],
				},
				// ------------------------------ MY TUTOR -----------------------
				{
					title: "Dashboard",
					root: true,
					icon: "fas fa-book",
					page: "/batutor/dashboard",
					bullet: "dot",
					module: "batutor",
					prod: true,
					rules: RULES.BATUTOR_ADMIN,
				},
				{
					title: "My Course",
					root: true,
					icon: "fas fa-book",
					page: "/batutor/mycourse",
					bullet: "dot",
					module: "batutor",
					prod: true,
					rules: RULES.BATUTOR_ADMIN,
				},
				{
					title: "Data Master",
					root: true,
					icon: "fas fa-database",
					page: "/batutor/datamaster",
					bullet: "dot",
					module: "batutor",
					rules: RULES.BATUTOR_ADMIN,
					prod: true,
					submenu: [{
						title: "Bidang",
						page: "/batutor/datamaster/data-bidang",
					},
						// {
						// 	title: "Kontrak",
						// 	page: "/non-swakelola/data-master/kontrak",
						// },
						// {
						// 	title: "Tipe Alat",
						// 	page: "/non-swakelola/data-master/tipe-alat",
						// },
						// {
						// 	title: "Alat",
						// 	page: "/non-swakelola/data-master/alat",
						// },
						// {
						// 	title: "Koreksi",
						// 	page: "/non-swakelola/data-master/koreksi",
						// },
						// {
						// 	title: "Lokasi",
						// 	page: "/non-swakelola/data-master/lokasi",
						// },
					],
				},
				//SSR
				{
					title: "SSR 01",
					root: true,
					icon: "fa fa-th-large",
					page: "/ssr/ssr01",
					bullet: "dot",
					module: "ssr",
					prod: true,
				},
				// ------------------------------ Pegawai Teladan -----------------------
				{
					title: "Todolist",
					root: true,
					icon: "fas fa-th-list",
					page: "/pegawai-teladan/todolist",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},
				{
					title: "Data Calon Pegawai <br> Teladan",
					root: true,
					icon: "fas fa-th-list",
					page: "/pegawai-teladan/datapengajuan",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},
				{
					title: "Ranking Jenjang I",
					root: true,
					icon: "fa fa-list-ol",
					page: "/pegawai-teladan/ranking-jj1",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					rules: RULES.PEGAWAI_TELADAN_RANKINGJJ1,
				},
				{
					title: "Hasil Ranking",
					root: true,
					icon: "fa fa-chart-line",
					page: "/pegawai-teladan/hasil-rankingjj1",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},
				{
					title: "CPT Ranking",
					root: true,
					icon: "fa fa-chart-line",
					page: "/pegawai-teladan/cpt-rangking",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},
				{
					title: "Form CPT",
					root: true,
					icon: "fa fa-pencil-square-o",
					page: "/pegawai-teladan/form-data-diri",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},

				{
					title: "Rekapitulasi Penilaian <br> Calon Pegawai Teladan",
					root: true,
					icon: "fa fa-pencil-square-o",
					page: "/pegawai-teladan/list-penilaian-satker",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},
				{
					title: "Form Uji Publik <br> Tempat Kerja",
					root: true,
					icon: "fa fa-pencil-square-o",
					page: "/pegawai-teladan/uji-publik-tempat-kerja",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},
				{
					title: "Form Ujian Tertulis",
					root: true,
					icon: "fa fa-pencil-square-o",
					page: "/pegawai-teladan/form-ujian-tertulis",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},
				{
					title: "Config Uji Publik <br> Tempat Kerja",
					root: true,
					icon: "fas fa-cog",
					page: "/pegawai-teladan/config-uji-publik-tempat-kerja",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},
				{
					title: "Config Ujian Tertulis",
					root: true,
					icon: "fas fa-cog",
					page: "/pegawai-teladan/config-ujian-tertulis",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},
				{
					title: "Role Access",
					root: true,
					icon: "fas fa-users",
					page: "/pegawai-teladan/role-access",
					bullet: "dot",
					module: "pegawai-teladan",
					prod: true,
					//rules: RULES.PEGAWAI_TELADAN_HASIL_RANKING,
				},

				// ------------------------------ Pegawai Teladan END -----------------------
				//InspeksiK3L Modules

				{
					title: "Dashboard",
					root: true,
					icon: "fas fa-user-check",
					page: "/inspeksi-k3l",
					bullet: "dot",
					module: "inspeksi-k3l",
					prod: true
				},
				{
					title: "Swabakar",
					root: true,
					icon: "fas fa-cog",
					page: "/inspeksi-k3l/swabakar",
					bullet: "dot",
					module: "inspeksi-k3l",
					rules: RULES.MRBA_K3L_ACCESS,
					prod: true
					// submenu: [
					// 	{
					// 		title: "Input Swabakar",
					// 		page: "/inspeksi-k3l/swabakar/inputswabakar",
					// 	},
					// 	{
					// 		title: "Output Swabakar",
					// 		page: "/inspeksi-k3l/swabakar/outputswabakar",
					// 	},
					// ],
				},
				{
					title: "Tanah Pucuk",
					root: true,
					icon: "fas fa-cog",
					page: "/inspeksi-k3l/tanahpucuk",
					bullet: "dot",
					module: "inspeksi-k3l",
					rules: RULES.MRBA_K3L_ACCESS,
					prod: true
					// submenu: [
					// 	{
					// 		title: "Input Tanahpucuk",
					// 		page: "/inspeksi-k3l/tanahpucuk/inputtanahpucuk",
					// 	},
					// 	{
					// 		title: "Output Tanahpucuk",
					// 		page: "/inspeksi-k3l/tanahpucuk/outputtanahpucuk",
					// 	},
					// ],
				},
				{
					title: "Erosi",
					root: true,
					icon: "fas fa-cog",
					page: "/inspeksi-k3l/erosi",
					bullet: "dot",
					module: "inspeksi-k3l",
					rules: RULES.MRBA_K3L_ACCESS,
					prod: true
					// submenu: [
					// 	{
					// 		title: "Input Erosi",
					// 		page: "/inspeksi-k3l/erosi/inputerosi",
					// 	},
					// 	{
					// 		title: "Output Erosi",
					// 		page: "/inspeksi-k3l/erosi/outputerosi",
					// 	},
					// ],
				},
				{
					title: "Workshop",
					root: true,
					icon: "fas fa-cog",
					page: "/inspeksi-k3l/workshop",
					bullet: "dot",
					module: "inspeksi-k3l",
					rules: RULES.MRBA_K3L_ACCESS,
					prod: true
					// submenu: [
					// 	{
					// 		title: "Input Workshop",
					// 		page: "/inspeksi-k3l/workshop/inputworkshop",
					// 	},
					// 	{
					// 		title: "Output Workshop",
					// 		page: "/inspeksi-k3l/workshop/outputworkshop",
					// 	},
					// ],
				},
				{
					title: "Air Inlet & Air Outlet",
					root: true,
					icon: "fas fa-cog",
					page: "/inspeksi-k3l/airinlet",
					bullet: "dot",
					module: "inspeksi-k3l",
					rules: RULES.MRBA_K3L_ACCESS,
					prod: true
					// submenu: [
					// 	{
					// 		title: "Input Air Inlet",
					// 		page: "/inspeksi-k3l/airinlet/inputairinlet",
					// 	},
					// 	{
					// 		title: "Output Air Inlet",
					// 		page: "/inspeksi-k3l/airinlet/outputairinlet",
					// 	},
					// ],
				},
				// {
				// 	title: "Air Outlet",
				// 	root: true,
				// 	icon: "fas fa-cog",
				// 	page: "/inspeksi-k3l/airoutlet",
				// 	bullet: "dot",
				// 	module: "inspeksi-k3l",
				// 	rules: RULES.MRBA_K3L_ACCESS,
				// 	// submenu: [
				// 	// 	{
				// 	// 		title: "Input Air Outlet",
				// 	// 		page: "/inspeksi-k3l/airoutlet/inputairoutlet",
				// 	// 	},
				// 	// 	{
				// 	// 		title: "Output Air Outlet ",
				// 	// 		page: "/inspeksi-k3l/airoutlet/outputairoutlet",
				// 	// 	},
				// 	// ],
				// },
				{
					title: "Data Master",
					root: true,
					icon: "fas fa-database",
					page: "/inspeksi-k3l/data-master",
					module: "inspeksi-k3l",
					rules: RULES.MRBA_K3L_ADMIN,
					prod: true,
					submenu: [{
						title: "Lokasi",
						root: true,
						icon: "fas fa-database",
						page: "/inspeksi-k3l/data-master/lokasi",
						bullet: "dot",
						module: "inspeksi-k3l",
						submenu: [{
							title: "Swabakar",
							page: "/inspeksi-k3l/data-master/lokasi/lokasiswabakar",
						},
							{
								title: "Tanah Pucuk",
								page: "/inspeksi-k3l/data-master/lokasi/lokasitanahpucuk",
							},
							{
								title: "Erosi",
								page: "/inspeksi-k3l/data-master/lokasi/lokasierosi",
							},
							{
								title: "Workshop",
								page: "/inspeksi-k3l/data-master/lokasi/lokasiworkshop",
							},
							{
								title: "Air Inlet",
								page: "/inspeksi-k3l/data-master/lokasi/lokasiinlet",
							},
							{
								title: "Air Outlet",
								page: "/inspeksi-k3l/data-master/lokasi/lokasioutlet",
							},
						],
					},
						{
							title: "Sublokasi",
							root: true,
							icon: "fas fa-database",
							page: "/inspeksi-k3l/data-master/sublokasi",
							bullet: "dot",
							module: "inspeksi-k3l",
							prod: true,
							submenu: [{
								title: "Swabakar",
								page: "/inspeksi-k3l/data-master/sublokasi/sublokasiswabakar",
							},
								{
									title: "Tanah Pucuk",
									page: "/inspeksi-k3l/data-master/sublokasi/sublokasitanahpucuk",
								},
								{
									title: "Erosi",
									page: "/inspeksi-k3l/data-master/sublokasi/sublokasierosi",
								},

								{
									title: "Workshop",
									page: "/inspeksi-k3l/data-master/sublokasi/sublokasiworkshop",
								},
								{
									title: "Air Inlet",
									page: "/inspeksi-k3l/data-master/sublokasi/sublokasiinlet",
								},
								{
									title: "Air Outlet",
									page: "/inspeksi-k3l/data-master/sublokasi/sublokasioutlet",
								},
							],
						},
						{
							title: "Kode sta Air Inlet",
							icon: "fas fa-database",
							bullet: "dot",
							page: "/inspeksi-k3l/data-master/sta-inlet",
							prod: true
						},
						{
							title: "Kode sta Air Outlet",
							icon: "fas fa-database",
							bullet: "dot",
							page: "/inspeksi-k3l/data-master/sta-outlet",
							prod: true
						},
						{
							title: "Jenis Erosi",
							icon: "fas fa-database",
							bullet: "dot",
							page: "/inspeksi-k3l/data-master/jeniserosi",
							prod: true
						},
						{
							title: "Kriteria Workshop",
							icon: "fas fa-database",
							bullet: "dot",
							page: "/inspeksi-k3l/data-master/kriteria-workshop",
							prod: true
						},
					],
				},

				// ------------------------------ Layanan SDM -----------------------
				{
					title: "To Do List",
					root: true,
					icon: "fas fa-list-ul",
					page: "/layanan-sdm/todolist",
					bullet: "dot",
					module: "layanan-sdm",
					prod: true,
				},

				{
					title: "Data Pengajuan",
					root: true,
					icon: "fas flaticon2-paper",
					page: "/layanan-sdm/data-pengajuan",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
				},
				{
					title: "UBP",
					root: true,
					icon: "fa fa-file",
					page: "/layanan-sdm/UBP",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
				},
				{
					title: "Pernikahan",
					root: true,
					icon: "fa fa-file",
					page: "/layanan-sdm/pernikahan",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
					submenu: [{
						title: "Permohonan Izin Menikah",
						page: "/layanan-sdm/pernikahan/izin-menikah",
					},
						{
							title: "Laporan Pernikahan",
							page: "/layanan-sdm/pernikahan/laporan-pernikahan",
						},
					],
				},
				{
					title: "Uang Duka",
					root: true,
					icon: "fa fa-file",
					page: "/layanan-sdm/uangduka",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
				},
				{
					title: "Reimbursement",
					root: true,
					icon: "fa fa-file",
					page: "/layanan-sdm/reimbursement-kesehatan",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
				},
				{
					title: "SPB",
					root: true,
					icon: "fa fa-file",
					page: "/layanan-sdm/form-spb",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm"
				},
				{
					title: "Data Keluarga",
					root: true,
					icon: "fa fa-file",
					page: "/layanan-sdm/data-keluarga",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
				},
				{
					title: "Pemindahan Fasilitas",
					root: true,
					icon: "fa fa-file",
					page: "/layanan-sdm/pemindahan-fasilitas",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
				},
				{
					title: "Izin Persalinan",
					root: true,
					icon: "fa fa-file",
					page: "/layanan-sdm/izin-persalinan",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
				},
				{
					title: "History",
					root: true,
					icon: "fas fa-clock",
					page: "/layanan-sdm/history",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
				},
				{
					title: "Role Access",
					root: true,
					icon: "fas fa-users",
					page: "/layanan-sdm/role-access",
					bullet: "dot",
					prod: true,
					module: "layanan-sdm",
					// rules: (localStorage.getItem("levelAccessSDM") ? (RULES.LAYANANSDM_ADMIN == parseInt(JSON.parse(localStorage.getItem("levelAccessSDM")).adminSdm) ? RULES.LAYANANSDM_ADMIN : parseInt(JSON.parse(localStorage.getItem("levelAccessSDM")).adminSdm)) : RULES.LAYANANSDM_ADMIN),

				},
				// ------------------------------ DRILL REPORT (DDR)-----------------------
				{
					title: "Dashboard",
					root: true,
					icon: "fa fa-th-large",
					page: "/drill/dashboard",
					bullet: "dot",
					module: "drill",
					prod: true,
				},
				{
					title: "Data Drill",
					root: true,
					icon: "fa fa-list-alt",
					page: "/drill/data",
					bullet: "dot",
					module: "drill",
					prod: true,
				},
				// MODULE AMS

				{
					title: "Todo List",
					root: true,
					icon: "fas fa-th-list",
					page: "/ams/todo-list",
					bullet: "dot",
					module: "ams",
					prod: true,
				},
				{
					title: "Planning",
					root: true,
					icon: "fas fa-calendar-alt",
					page: "/ams/planning",
					bullet: "dot",
					module: "ams",
					prod: false,
					submenu: [
						// {
						// 	title: "Planning Audit",
						// 	page: "/ams/planning/planning-audit",
						// },
						{
							title: "Planning",
							page: "/ams/planning/dataPlanning",
						},
					],
				},
				{
					title: "Konfigurasi",
					root: true,
					icon: "fas fa-cog",
					page: "/ams/konfigurasi",
					bullet: "dot",
					module: "ams",
					prod: true,
					submenu: [
						// {
						// 	title: "Audit Periode",
						// 	page: "/ams/konfigurasi/audit-periode",
						// },
						// {
						// 	title: "Kriteria Audit",
						// 	page: "/ams/konfigurasi/kriteria-audit",
						// },
						// {
						// 	title: "Tipe Audit",
						// 	page: "/ams/konfigurasi/tipe-audit",
						// },
						// {
						// 	title: "Prosedur Audit",
						// 	page: "/ams/konfigurasi/procedure-audit",
						// },
						{
							title: "Perubahan Satker",
							page: "/ams/konfigurasi/perubahan-satker",
						},
					],
				},
				{
					title: "Input Audit",
					root: true,
					icon: "fas fa-edit",
					page: "/ams/input-audit",
					bullet: "dot",
					module: "ams",
					prod: true,
				},
				{
					title: "Monitoring",
					root: true,
					icon: "fas fa-desktop",
					page: "/ams/monitoring",
					bullet: "dot",
					module: "ams",
					prod: true,
					submenu: [{
						title: "Dashboard",
						page: "/ams/dashboard"
					},
						{
							title: "Monitoring",
							page: "/ams/monitoring"
						},
						{
							title: "Berita Acara",
							page: "/ams/berita-acara"
						},
						{
							title: "Report",
							page: "/ams/report"
						}
					]
				},
				{
					title: "Role Access",
					root: true,
					icon: "fas fa-users",
					page: "/ams/role-access",
					bullet: "dot",
					module: "ams",
					prod: true,
					rules: (localStorage.getItem("hakAksesAms") ? (JSON.parse(localStorage.getItem("hakAksesAms")).isAdmin ? true : false) : RULES.AMS_ADMIN)
				},

				//END AMS
				// ------------------------------ EHA-----------------------
				{
					title: "Dashboard",
					root: true,
					icon: "fa fa-th-large",
					page: "/eha/dashboard",
					bullet: "dot",
					module: "eha",
					prod: true,
					submenu: [
						// {
						// 	title: "Maintenance Performe",
						// 	page: "/eha/breakdown"
						// },
						{
							title: "Frekuensi Breakdown",
							page: "/eha/dash-frekuensi"
						},
						{
							title: "Durasi Breakdown",
							page: "/eha/dash-durasi"
						},
						{
							title: "Komponen Breakdown",
							page: "/eha/dash-komponen"
						},
						{
							title: "Jam Jalan",
							page: "/eha/dashboard/jam-jalan"
						}
					]
				},
				{
					title: "Breakdown",
					root: true,
					icon: "fa fa-th-large",
					page: "/eha/breakdown",
					bullet: "dot",
					module: "eha",
					prod: true,
					submenu: [{
						title: "Breakdown On Progress",
						page: "/eha/breakdown"
					},
						{
							title: "History Breakdown",
							page: "/eha/history-breakdown"
						}
					]
				},
				{
					title: "Preventive",
					root: true,
					icon: "fa fa-th-large",
					page: "/eha/maintenance-planning",
					bullet: "dot",
					module: "eha",
					prod: true,
					submenu: [{
						title: "Maintenance Planning",
						page: "/eha/maintenance-planning"
					},

						{
							title: "Backlog",
							page: "/eha/backlog"
						},
						{
							title: "Additional Task",
							page: "/eha/additional-task"
						}

					]
				},
				// {
				// 	title: "Predictive",
				// 	page: "/eha/predictive",
				// 	root: true,
				// 	icon: "fa fa-th-large",
				// 	bullet: "dot",
				// 	module: "eha",
				// 	prod: false,
				// },
				{
					title: "Perawatan",
					root: true,
					icon: "fa fa-th-large",
					page: "/eha/perawatan/dashboard",
					bullet: "dot",
					module: "eha",
					prod: true,
					submenu:[
						{
							title:'APPT1',
							submenu: [
								{
							// 	title: "Dashboard",
							// 	page: "/eha/perawatan/dashboard/APPT1"
							// },{
								title: "Timesheet",
								page: "/eha/perawatan/timesheet/APPT1"
							},{
								title: "Timesheet History",
								page: "/eha/perawatan/timesheet-history/APPT1"
							}]
						},
						{
							title:'APPT2',
							submenu: [{
							// 	title: "Dashboard",
							// 	page: "/eha/perawatan/dashboard/APPT2"
							// },{
								title: "Timesheet",
								page: "/eha/perawatan/timesheet/APPT2"
							},{
								title: "Timesheet History",
								page: "/eha/perawatan/timesheet-history/APPT2"
							}]
						}
					]
				},
				// {
				// 	title: "Tire",
				// 	root: true,
				// 	icon: "fa fa-th-large",
				// 	page: "/eha/tire",
				// 	bullet: "dot",
				// 	module: "eha",
				// 	prod: true,
				// },
				{
					title: "Master Data",
					root: true,
					icon: "fa fa-th-large",
					page: "/eha/master",
					bullet: "dot",
					module: "eha",
					prod: true,
					submenu: [{
							title: "Equipment",
							page: "/eha/data-master/equipment"
						},
						{
							title: "Komponen",
							page: "/eha/data-master/komponen"
						},
						{
							title: "Mekanik",
							page: "/eha/data-master/mekanik"
						},
						{
							title: "Lokasi Timesheet",
							page: "/eha/datamaster/timesheet/lokasi"
						},
						// {
						// 	title: "Operator",
						// 	page: "/eha/datamaster/operator"
						// }
					]
				},
				// ------------------------------ END EHA-----------------------
				// ------------------------------ Technical Draw --------------------
				{
					title: "UPTE",
					root: true,
					// icon: "fas fa-dollar-sign",
					icon: "fas fa-snowplow",
					// img : "assets/media/icons/modules/PNS.png",
					// width : "25px",
					page: "/technical-draw",
					bullet: "dot",
					module: "technicaldraw",
					prod: false,
					submenu: [{
						title: "Mechanical",
						page: "/technicaldraw/upte-mekanik",
					},
						{
							title: "Civil",
							page: "/technicaldraw/upte-sipil",
						},
						{
							title: "Electrical",
							page: "/technicaldraw/upte-elektronik",
						},
					],
				},
				{
					title: "DERTI",
					root: true,
					// icon: "fas fa-dollar-sign",
					icon: "fas fa-snowplow",
					// img : "assets/media/icons/modules/PNS.png",
					// width : "25px",
					page: "/technical-draw",
					bullet: "dot",
					module: "technicaldraw",
					prod: false,
					submenu: [{
						title: "Mechanical",
						page: "/technicaldraw/derti-mekanik",
					},
						{
							title: "Civil",
							page: "/technicaldraw/derti-sipil",
						},
						{
							title: "Electrical",
							page: "/technicaldraw/derti-elektronik",
						},
					],
				},
				{
					title: "TARAHAN",
					root: true,
					// icon: "fas fa-dollar-sign",
					icon: "fas fa-snowplow",
					// img : "assets/media/icons/modules/PNS.png",
					// width : "25px",
					page: "/technical-draw",
					bullet: "dot",
					module: "technicaldraw",
					prod: false,
					submenu: [{
						title: "Mechanical",
						page: "/technicaldraw/tarahan-mekanik",
					},
						{
							title: "Civil",
							page: "/technicaldraw/tarahan-sipil",
						},
						{
							title: "Electrical",
							page: "/technicaldraw/tarahan-elektronik",
						},
					],
				},
				// ------------------------------ end Technical Draw ------------------------
				// ------------------------------------- SDMO -------------------------------
				{
					title: "To-Do List",
					module: "sdmo",
					icon: "fas fa-list-ul",
					root: true,
					page: "/sdmo/to-do-list",
					prod: true
				},
				{
					title: "Simulasi Pensiun",
					module: "sdmo",
					icon: "fas fa-list-ul",
					root: true,
					bullet: "dot",
					page: "/sdmo/simulasi-pensiun",
					submenu: [{
						title: "Estimasi Pensiun",
						page: "/sdmo/estimasi-pensiun",
						prod: true
					},
						{
							title: "Konfigurasi Jenis Pensiun",
							page: "/sdmo/konfigurasi-jenis-pensiun",
							prod: true
						}
					],
					prod: true
				},
				{
					title: "Perhitungan UBP",
					module: "sdmo",
					icon: "fas fa-list-ul",
					root: true,
					bullet: "dot",
					page: "/sdmo/hitung-ubp",
					submenu: [{
						title: "Pengajuan UBP",
						page: "/sdmo/sdmo-pengajuan-ubp",
						prod: true
					},
						{
							title: "Daftar Rumah Tinggal",
							page: "/sdmo/sdmo-daftar-rumah-tinggal",
							prod: true
						},
						{
							title: "Report",
							page: "/sdmo/sdmo-ubp-report",
							prod: true
						}
					],
					prod: true
				},
				{
					title: "Uang Duka Pegawai",
					module: "sdmo",
					icon: "fas fa-list-ul",
					root: true,
					page: "/sdmo/pengajuan-uang-duka-pegawai",
					prod: true
				},
				{
					title: "Pengajuan Reimburse",
					module: "sdmo",
					icon: "fas fa-list-ul",
					root: true,
					page: "/sdmo/pengajuan-reimburse",
					prod: true
				},
				{
					title: "Pengajuan Izin Menikah",
					module: "sdmo",
					icon: "fas fa-list-ul",
					root: true,
					page: "/sdmo/pengajuan-izin-nikah",
					prod: true
				},
				{
					title: "Pengajuan Pernikahan",
					module: "sdmo",
					icon: "fas fa-list-ul",
					root: true,
					page: "/sdmo/pengajuan-pernikahan",
					prod: true
				},
				{
					title: "Pengajuan UBP",
					module: "sdmo",
					icon: "fas fa-list-ul",
					root: true,
					page: "/sdmo/pengajuan-ubp",
					prod: true
				},
				{
					title: "Role Access",
					module: "sdmo",
					icon: "fas fa-list-ul",
					root: true,
					page: "/sdmo/sdmo-role-access",
					prod: true
				},
				// ----------------------------------- end SDMO -----------------------------
				// ----------------------------- start Pandemic -----------------------------
				{
					title: 'Dashboard',
					module: 'pandemics',
					icon: 'fas fa-chart-line',
					bullet: 'dot',
					root: true,
					page: '/pandemics/dashboard',
					prod: true,
					// submenu: [],
				},
				{
					title: 'Confirmed Suspect',
					module: 'pandemics',
					icon: 'fas fa-list',
					bullet: 'dot',
					root: true,
					page: '/pandemics/suspect/confirmed',
					prod: true,
					// submenu: [],
				},
				{
					title: 'Treatment Data',
					module: 'pandemics',
					icon: 'fas fa-list',
					bullet: 'dot',
					root: true,
					page: '/pandemics/suspect/treatment',
					prod: true,
					// submenu: [],
				},
				{
					title: 'Tracing',
					module: 'pandemics',
					icon: 'fas fa-list',
					bullet: 'dot',
					root: true,
					page: '/pandemics/suspect/tracing',
					prod: true,
					// submenu: [],
				},
				// ------------------------------ end Pandemic ------------------------------
				// ----------------------------- Start CURAH HUJAN ---------------------------
				{
					title: 'Dashboard',
					module: 'curah-hujan',
					icon: 'fa fa-th-large',
					root: true,
					page: '/curah-hujan/dashboard',
					prod: true
				},
				{
					title: 'Data',
					module: 'curah-hujan',
					icon: 'fa fa-pencil-square-o',
					prod: true,
					submenu: [{
						title: "Hujan",
						page: "/curah-hujan/data/hujan",
					},
						{
							title: "Plan",
							page: "/curah-hujan/data/plan",
						},
						{
							title: "BMKG",
							page: "/curah-hujan/data/bmkg",
						}
					]
				},
				{
					title: 'Master',
					module: 'curah-hujan',
					icon: 'fa fa-database',
					prod: true,
					submenu: [{
						title: "Alat",
						page: "/curah-hujan/master/alat",
					},
						{
							title: "Area",
							page: "/curah-hujan/master/area",
						},
						{
							title: "Lokasi",
							page: "/curah-hujan/master/lokasi",
						},
						{
							title: "Shift",
							page: "/curah-hujan/master/shift",
						},
						{
							title: "Stasiun",
							page: "/curah-hujan/master/stasiun",
						}
					]
				},
				// --------------------------- START Ads ----------------------------
				{
					title: 'Banner',
					module: 'ads',
					icon: 'fas fa-image',
					root: true,
					page: '/ads/banners',
					prod: false,
				},
				{
					title: 'Role Access',
					module: 'ads',
					icon: 'fa fa-users',
					root: true,
					page: '/ads/role-access',
					prod: false,
				},
				// --------------------------- START SERVICE DESK ----------------------------
				{
					title: 'Dashboard',
					module: 'service-desk',
					icon: 'fa fa-th-large',
					root: true,
					page: '/service-desk/dashboard',
					prod: false,
				},
				{
					title: 'Dashboard',
					module: 'layanan-ti',
					icon: 'fa fa-th-large',
					root: true,
					page: '/layanan-ti/dashboard',
					prod: true
				},
				// {
				// 	title: 'Permintaan Akses TI',
				// 	module: 'layanan-ti',
				// 	icon: 'fa fa-pencil-square-o',
				// 	submenu: [
				// 		{
				// 			title: "To Do List",
				// 			page: "/layanan-ti/asset/todolist",
				// 		},
				// 		{
				// 			title: "Permintaan Asset",
				// 			page: "/layanan-ti/asset/requested",
				// 		},
				// 		{
				// 			title: "Permintaan Asset",
				// 			page: "/layanan-ti/asset/my-asset",
				// 		},
				// 	]
				// },
				{
					title: 'Permintaan Perangkat TI',
					module: 'layanan-ti',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/layanan-ti/asset/requested',
					prod: true,
				},
				{
					title: 'To Do List',
					module: 'layanan-ti',
					icon: 'fa fa-list',
					root: true,
					page: "/layanan-ti/asset/todolist",
					prod: true,
				},
				{
					title: 'Deploy',
					module: 'layanan-ti',
					icon: 'fa fa-arrow-circle-up',
					root: true,
					page: "/layanan-ti/deploy",
					prod: true,
				},
				{
					title: 'Data Master',
					module: 'layanan-ti',
					icon: 'fa fa-database',
					root: true,
					prod: true,
					submenu: [{
						title: 'Asset',
						page: "/layanan-ti/master/assets",
					},
						{
							title: 'Asset Brand',
							page: "/layanan-ti/master/asset_brands",
						},
						{
							title: 'Asset Tipe',
							page: "/layanan-ti/master/asset_types",
						},
						{
							title: 'Asset Jenis',
							page: "/layanan-ti/master/asset_categories",
						},
						{
							title: 'Asset Batch',
							page: "/layanan-ti/master/asset_batches",
						},
					],
				},
				// --------------------------- START CITRA ----------------------------
				{
					title: 'Contract Management',
					module: 'citra',
					icon: 'fas fa-file-contract',
					root: true,
					prod: false,
					submenu: [
						{
							icon: 'fa fa-list',
							title: 'Contract List',
							page: "/citra/contract-management/list",
						},
						{
							icon: 'fa fa-desktop',
							title: 'Contract Monitoring',
							page: "/citra/contract-management/monitoring",
						},
						{
							icon: 'fas fa-times-circle',
							title: 'Contract Closed',
							page: "/citra/contract-management/closed",
						},
					],
				},
				{
					title: 'Permit Management',
					module: 'citra',
					icon: 'fa fa-id-card',
					root: true,
					prod: false,
					submenu: [
						{
							icon: 'fa fa-list',
							title: 'Permit List',
							page: "/citra/permit-management/list",
						},
						{
							icon: 'fa fa-desktop',
							title: 'Permit Monitoring',
							page: "/citra/permit-management/monitoring",
						},
						{
							icon: 'fas fa-times-circle',
							title: 'Permit Closed',
							page: "/citra/permit-management/closed",
						},
					],
				},
				{
					title: 'Task Management',
					module: 'citra',
					icon: 'fa fa-plus-square',
					root: true,
					prod: false,
					submenu: [],
				},
				{
					title: 'TL & TCK Management',
					module: 'citra',
					icon: 'fa fa-plus-square',
					root: true,
					prod: false,
					submenu: [],
				},
				{
					title: 'MoM Management',
					module: 'citra',
					icon: 'fa fa-plus-square',
					root: true,
					prod: false,
					submenu: [],
				},
				{
					title: 'Administrator',
					module: 'citra',
					icon: 'fa fa-gears',
					root: true,
					prod: false,
					submenu: [
						{
							icon: 'fa fa-circle-o',
							title: 'Akses User',
							page: "/citra/administrator/user-accesses",
						},
						{
							icon: 'fa fa-circle-o',
							title: 'Master Klasifikasi',
							page: "/citra/administrator/classifications",
						},
						{
							icon: 'fa fa-circle-o',
							title: 'Master Grand Role',
							page: "/citra/administrator/grand-roles",
						},
					],
				},
				// ----------------------------  STOP CITRA ----------------------------
				// ----------------------------- START RMM -----------------------------
				{
					title: 'Dashboard',
					module: 'rmm',
					icon: 'fa fa-th-large',
					root: true,
					page: '/rmm/dashboard',
					prod: true,
				},
				{
					title: 'To Do List',
					module: 'rmm',
					icon: 'fa fa-tasks',
					root: true,
					page: '/rmm/todolist',
					prod: true,
				},
				{
					title: 'Progress Keputusan',
					module: 'rmm',
					icon: 'fa fa-list',
					root: true,
					page: '/rmm/minutes-of-meeting',
					prod: true,
				},
				{
					title: 'Dokumen',
					module: 'rmm',
					icon: 'fa fa-file-o',
					root: true,
					page: '/rmm/documents',
					prod: true,
				},
				{
					title: 'Role Access',
					module: 'rmm',
					icon: 'fa fa-users',
					root: true,
					page: '/rmm/role-access',
					prod: true,
				},
				// ----------------------------- END RAKER -----------------------------
				// ----------------------------- START ROYALTY -----------------------------
				{
					title: 'To Do List',
					module: 'royalty',
					icon: 'fa fa-list',
					root: true,
					page: '/royalty/todolist',
					prod: true,
				},
				{
					title: 'Dashboard',
					module: 'royalty',
					icon: 'fa fa-th-large',
					root: true,
					page: '/royalty/dashboard',
					prod: false,
				},
				{
					title: 'Entry Royalty',
					module: 'royalty',
					icon: 'fa fa-file',
					root: true,
					page: '/royalty/requests/entry',
					prod: true,
				},
				{
					title: 'Daftar Royalty',
					module: 'royalty',
					icon: 'fa fa-file-text-o',
					root: true,
					page: '/royalty/requests',
					prod: true,
				},
				{
					title: 'Master',
					module: 'royalty',
					icon: 'fa fa-database',
					root: true,
					prod: true,
					submenu: [{
						title: 'Buyer',
						page: "/royalty/master/buyer",
					},
						{
							title: 'Barge',
							page: "/royalty/master/barges",
						},
						{
							title: 'Vessel',
							page: "/royalty/master/vessels",
						},
						{
							title: 'Calorie',
							page: "/royalty/master/calories",
						},
						{
							title: 'Access',
							page: "/royalty/master/access",
						},
						{
							title: 'Document',
							page: "/royalty/master/documents",
						},
					],
				},
				{
					title: 'Role Access',
					module: 'royalty',
					icon: 'fa fa-users',
					root: true,
					page: '/royalty/role-access',
					prod: true,
				},
				// ----------------------------- END CURAH HUJAN -----------------------------
				// --------------------------- Start e-Investment  ---------------------------
				{
					title: 'Dashboard',
					module: 'e-investment',
					icon: 'fa fa-th-large',
					root: true,
					page: '/e-investment/dashboard',
					prod: true,
				},
				{
					title: 'Form Usulan',
					module: 'e-investment',
					icon: 'fa fa-th-large',
					root: false,
					page: '/e-investment/proposal',
					prod: true,
				},
				{
					title: 'Investasi Satker',
					module: 'e-investment',
					icon: 'fa fa-th-large',
					root: false,
					page: '/e-investment/satker-investation',
					prod: true,
				},
				{
					title: 'Investasi PTBA',
					module: 'e-investment',
					icon: 'fa fa-th-large',
					root: false,
					page: '/e-investment/company-investation',
					prod: true,
				},
				{
					title: 'Monitoring Investasi',
					module: 'e-investment',
					icon: 'fa fa-th-large',
					root: false,
					page: '/e-investment/monitoring',
					prod: true,
				},
				{
					title: 'Katalog Investasi',
					module: 'e-investment',
					icon: 'fa fa-th-large',
					root: false,
					page: '/e-investment/catalog',
					prod: true,
				},
				//{
				//	title: 'Master Data',
				//	module: 'e-investment',
				//	icon: 'fa fa-pencil-square-o',
				//	submenu: [
				//		{
				//			title: "Satuan",
				//			page: "/e-investment/master/unit",
				//		},
				//		{
				//			title: "Status",
				//			page: "/e-investment/master/status",
				//		},
				//		{
				//			title: "Satker",
				//			page: "/e-investment/master/satker",
				//		},
				//	],
				//},
				// ---------------------------- End e-Investment  ----------------------------
				// --------------------------- Start Geolog  ---------------------------
				{
					title: 'Dashboard',
					module: 'geolog',
					icon: 'fa fa-th-large',
					root: true,
					page: '/geolog/dashboard',
					prod: true,
				},
				{
					title: 'Todo List',
					module: 'geolog',
					icon: 'fa flaticon-interface-10',
					root: true,
					page: '/geolog/todo-list',
					prod: true,
				},
				{
					title: 'Data Geologi',
					module: 'geolog',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/geolog/reporting',
					prod: true,
				},
				{
					title: 'Sample Analysis',
					module: 'geolog',
					icon: 'fa flaticon-clipboard',
					root: true,
					page: '/geolog/usulan',
					prod: false,
				},
				{
					title: 'Report',
					module: 'geolog',
					icon: 'fa flaticon-clipboard',
					root: true,
					page: '/geolog/report',
					prod: false,
				},
				{
					title: 'Rule Access',
					module: 'geolog',
					icon: 'fa flaticon2-user-1',
					root: true,
					page: '/geolog/rules',
					prod: true,
				},
				{
					title: 'Master Data',
					module: 'geolog',
					icon: 'fa flaticon2-gear',
					root: true,
					page: '/geolog/master-data',
					prod: true,
				},
				// ---------------------------- End Geolog  ----------------------------
				// --------------------------- Start Geolog  ---------------------------
				{
					title: 'Dashboard',
					module: 'labsys',
					icon: 'fa fa-th-large',
					root: true,
					page: '/labsys/dashboard',
					prod: true,
				},
				{
					title: 'Pengujian PAB',
					module: 'labsys',
					icon: 'fa flaticon2-delivery-truck',
					root: true,
					page: '/labsys/pab',
					prod: true,
				},
				{
					title: 'Pengujian Briket',
					module: 'labsys',
					icon: 'fa flaticon-interface-10',
					root: true,
					page: '/labsys/briket',
					prod: true,
				},
				{
					title: 'Pengujian Sucofindo',
					module: 'labsys',
					icon: 'fa flaticon-interface-10',
					root: true,
					page: '/labsys/third_party',
					prod: true,
				},
				{
					title: 'Analisis',
					module: 'labsys',
					icon: 'fa flaticon-notes',
					root: true,
					page: '/labsys/analisis',
					prod: true,
				},
				{
					title: 'Report',
					module: 'labsys',
					icon: 'fa flaticon-folder-1',
					root: true,
					page: '/labsys/report',
					prod: true,
				},
				{
					title: "Role Access",
					module: "labsys",
					icon: "fas fa-list-ul",
					root: true,
					page: "/labsys/role_access",
					prod: true
				},

				// ---------------------------- End Geolog  ----------------------------
				// --------------------------- Start ppkp  ---------------------------
				{
					title: 'Dashboard',
					module: 'ppkp',
					icon: 'fa fa-th-large',
					root: true,
					page: '/ppkp/dashboard',
					prod: false,
				},
				{
					title: 'PPKP ANDA',
					module: 'ppkp',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/ppkp/data-kpi',
					prod: false,
				},
				{
					title: 'PPKP BAWAHAN ANDA',
					module: 'ppkp',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/ppkp/data-kpi-bawahan',
					prod: false,
				},
				{
					title: 'History Nilai',
					module: 'ppkp',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/ppkp/history-point',
					prod: false,
				},

				// ---------------------------- End ppkp  ----------------------------

				// --------------------------- Start MCT  ---------------------------
				{
					title: 'HSE',
					module: 'mct',
					icon: 'fa fa-th-large',
					root: true,
					prod: true,
					submenu: [{
						title: "Environment & Safety",
						page: "/mct/hse/environment/safety",
					},
						{
							title: "Safety Detail",
							page: "/mct/hse/safety/detail",
						},
						{
							title: "Safety Per Subsidiary",
							page: "/mct/hse/safety/subsidiary",
						},
						{
							title: "Safety Pyramid",
							page: "/mct/hse/safety/pyramid",
						},
					]
				},
				{
					title: 'Operation',
					module: 'mct',
					icon: 'fa fa-th-large',
					root: true,
					prod: true,
					submenu: [{
						title: "Production",
						page: "/mct/operation/production",
					},
						{
							title: "Production Key",
							page: "/mct/operation/production-key",
						},
						{
							title: "Equipment",
							page: "/mct/operation/equipment",
						},
					]
				},
				{
					title: 'Finance & Sales',
					module: 'mct',
					icon: 'fa fa-th-large',
					root: true,
					prod: true,
					submenu: [{
						title: "Executive Summary",
						page: "/mct/dashboard/ex-summary",
					},
						{
							title: "Profit and Loss",
							page: "/mct/dashboard/profit-loss",
						},
						{
							title: "Cash Flow Statement",
							page: "/mct/dashboard/cash-flow",
						},
						{
							title: "Balance Sheet",
							page: "/mct/dashboard/bal-sheet",
						},
						{
							title: "Data Sales",
							page: "/mct/dashboard/data-sales",
						},
					]
				},
				{
					title: 'Strategic Initiatives',
					module: 'mct',
					icon: 'fa fa-th-large',
					root: false,
					prod: true,
					submenu: [{
						title: "Executive Summary",
						page: "/mct/strategic/ex-summary",
					},
						{
							title: "Overview",
							page: "/mct/strategic/overview",
						},
						{
							title: "Risk Categories",
							page: "/mct/strategic/risk-categories",
						},
					]
				},
				{
					title: 'Human Capital',
					module: 'mct',
					icon: 'fa fa-th-large',
					root: false,
					page: '/mct/dashboard/human-capital',
					prod: true,
				},
				{
					title: 'Risk Management',
					module: 'mct',
					icon: 'fa fa-th-large',
					root: false,
					page: '/mct/risk-management',
					prod: true,
				},
				{
					title: 'KPI Monitoring',
					module: 'mct',
					icon: 'fa fa-th-large',
					root: true,
					prod: true,
					submenu: [{
						title: "Executive Summary",
						page: "/mct/kpi/exsum",
					},
						{
							title: "Current KPI",
							page: "/mct/kpi/current",
						},
						{
							title: "Previous KPI",
							page: "/mct/kpi/previous",
						},
					]
				},
				{
					title: 'Legal Monitoring',
					module: 'mct',
					icon: 'fa fa-th-large',
					root: false,
					//page: '/mct/legal-mointoring',
					prod: true,
				},
				{
					title: 'Audit Monitoring',
					module: 'mct',
					icon: 'fa fa-th-large',
					root: false,
					//page: '/mct/strategic',
					prod: true,
				},
				// ---------------------------- End MCT  ----------------------------
				// --------------------------- Start Alih Daya  ---------------------------
				// {
				// 	title: 'Dashboard',
				// 	module: 'alih-daya',
				// 	icon: 'fa fa-th-large',
				// 	root: true,
				// 	page: '/alih-daya/dashboard',
				// 	prod: false,
				// },
				{
					title: 'Dashboard',
					module: 'alihdaya',
					icon: 'fa fa-th-large',
					root: true,
					page: '/alihdaya/dashboard',
					prod: false,
				},
				{
					title: 'List Alih Daya',
					module: 'alihdaya',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/alihdaya/list-alihdaya',
					prod: false,
				},
				{
					title: 'Master',
					module: 'alihdaya',
					icon: 'fa fa-database',
					root: true,
					prod: false,
					submenu: [
						// {
						// 	title: 'Vendor',
						// 	page: "/alihdaya/master/vendor",
						// },
						{
							title: 'Kontrak',
							page: "/alihdaya/master/kontrak",
						},
						// {
						// 	title: 'PIC Vendor',
						// 	page: "/alihdaya/master/vendor_pic",
						// },
						{
							title: 'Data Pegawai Alih daya',
							page: "/alihdaya/master/pegawai-alihdaya",
						},
					],
				},
				{
					title: 'Role Access',
					module: 'alihdaya',
					icon: 'fa fa-users',
					root: true,
					page: '/alihdaya/role-access',
					prod: false,
				},


				// ---------------------------- End Alih daya  ----------------------------
				// ---------------------------- SIBAIA ------------------------------------
				{
					title: 'Dashboard',
					module: 'sibaia',
					icon: 'fa fa-th-large',
					root: true,
					page: '/sibaia/dashboard',
					prod: true,
				},
				{
					title: 'Todolist',
					module: 'sibaia',
					icon: 'fas fa-th-list',
					root: true,
					page: '/sibaia/todolist',
					prod: true,
				},
				{
					title: 'Pendaftaran',
					module: 'sibaia',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/sibaia/formsibaia',
					prod: true,
				},
				{
					title: 'Partisipan',
					module: 'sibaia',
					icon: 'fas fa-th-list',
					root: true,
					page: '/sibaia/daftarpartisipan',
					prod: true,
				},
				{
					title: 'Ranking',
					module: 'sibaia',
					icon: 'fas fa-th-list',
					root: true,
					page: '/sibaia/rangking',
					prod: true,
				},
				// {
				// 	title: 'Konfigurasi Juri',
				// 	module: 'sibaia',
				// 	icon: 'fas fa-th-list',
				// 	root: true,
				// 	page: '/sibaia/konfig-juri',
				// 	prod: true,
				// },
				{
					title: 'Konfigurasi Periode',
					module: 'sibaia',
					icon: 'fas fa-th-list',
					root: true,
					page: '/sibaia/konfig-periode',
					prod: false,
				},
				{
					title: 'Role Access',
					module: 'sibaia',
					icon: 'fa fa-users',
					root: true,
					page: '/sibaia/rule-access',
					prod: true,
				},

				// ---------------------------- SIBAIA END --------------------------------
				// ---------------------------- Layanan Keuangan Akuntansi --------------------------------
				{
					title: 'Todolist',
					module: 'layanan-akuntansi',
					icon: 'fas fa-th-list',
					root: true,
					page: '/layanan-akuntansi/todolist',
					prod: true,
				},
				{
					title: 'Data Pengajuan',
					module: 'layanan-akuntansi',
					icon: 'fas fa-th-list',
					root: true,
					page: '/layanan-akuntansi/data-pengajuan',
					prod: true,
				},
				{
					title: 'Uang Muka',
					module: 'layanan-akuntansi',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/layanan-akuntansi/pengajuan-uang-muka',
					prod: true,
				},
				{
					title: 'Pertanggung Jawaban <br> Uang Muka',
					module: 'layanan-akuntansi',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/layanan-akuntansi/pertanggung-jawaban-um',
					prod: true,
				},
				{
					title: 'Penggunaan Anggaran <br> Korporat',
					module: 'layanan-akuntansi',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/layanan-akuntansi/pengajuan-penggunaan-anggaran-korporat',
					prod: true
				},
				{
					title: 'Pengalihan Anggaran',
					module: 'layanan-akuntansi',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/layanan-akuntansi/pengajuan-pengalihan-anggaran',
					prod: true
				},
				{
					title: 'Report',
					module: 'layanan-akuntansi',
					icon: 'fas fa-folder-open',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Pengajuan Uang Muka',
							page: "/layanan-akuntansi/report/pengajuan-um",
							icon: 'fas fa-file-alt',
						},
						{
							title: 'Pertanggung Jawaban <br> Uang Muka',
							page: "/layanan-akuntansi/report/pertanggung-jawaban-um",
							icon: 'fas fa-file-alt',
						},
						{
							title: 'Penggunaan Anggaran <br> Korporat',
							page: "/layanan-akuntansi/report/penggunaan-anggaran",
							icon: 'fas fa-file-alt',
						},
						{
							title: 'Pengalihan Anggaran',
							page: "/layanan-akuntansi/report/pengalihan-anggaran",
							icon: 'fas fa-file-alt',
						},

					],
				},
				{
					title: 'History',
					module: 'layanan-akuntansi',
					icon: 'fas fa-clock',
					root: true,
					page: '/layanan-akuntansi/history',
					prod: true,
				},
				{
					title: 'Role Access',
					module: 'layanan-akuntansi',
					icon: 'fa fa-users',
					root: true,
					page: '/layanan-akuntansi/role-access',
					prod: true,
				},
				// ---------------------------- Layanan Keuangan / Akuntansi END --------------------------------
				// ------------------------------ PENSIUN ----------------------------------
				{
					title: 'Home',
					module: 'pensiun',
					icon: 'fas fa-home',
					root: true,
					page: '/pensiun/dashboardpensiun',
					prod: false,
				},
				{
					title: 'Pengajuan Pensiun',
					module: 'pensiun',
					icon: 'fas fa-toolbox',
					root: true,
					page: '/pensiun/administrasi',
					prod: false,
				},
				{
					title: 'Daftar Pengajuan Pensiun',
					module: 'pensiun',
					icon: 'fas fa-list',
					root: true,
					page: '/pensiun/list-pengajuan-pensiun',
					prod: false,
				},
				{
					title: 'Tanya Jawab',
					module: 'pensiun',
					icon: 'fas fa-question-circle',
					root: true,
					page: '/pensiun/tanya-jawab',
					prod: false,
				},
				{
					title: 'Master Data',
					module: 'pensiun',
					icon: 'fas fa-database',
					root: true,
					page: '/pensiun/master-data',
					prod: false,
				},
				{
					title: 'Konfigurasi Kepesertaan',
					module: 'pensiun',
					icon: 'fas fa-tools',
					root: true,
					page: '/pensiun/konfigurasi-kepesertaan',
					prod: false,
				},
				{
					title: 'Role Access',
					module: 'pensiun',
					icon: 'fas fa-users',
					root: true,
					page: '/pensiun/role-access',
					prod: false,
				},
				// ---------------------------- PENSIUN END --------------------------------
				// --------------------------- Start Renling  ---------------------------
				// {
				// 	title: 'Dashboard',
				// 	module: 'alih-daya',
				// 	icon: 'fa fa-th-large',
				// 	root: true,
				// 	page: '/alih-daya/dashboard',
				// 	prod: false,
				// },
				// {
				// 	title: 'Dashboard',
				// 	module: 'renling',
				// 	icon: 'fa fa-th-large',
				// 	root: true,
				// 	page: '/renling/dashboard',
				// 	prod: true,
				// },
				// {
				// 	title: 'Konfigurasi IPLC',
				// 	module: 'renling',
				// 	icon: 'fa fa-pencil-square-o',
				// 	root: true,
				// 	page: '/renling/iplc',
				// 	prod: true,
				// },
				// {
				// 	title: 'SK',
				// 	module: 'renling',
				// 	icon: 'fa fa-list',
				// 	root: true,
				// 	page: '/renling/sk',
				// 	prod: true,
				// },
				// {
				// 	title: 'KPL',
				// 	module: 'renling',
				// 	icon: 'fa flaticon-clipboard',
				// 	root: true,
				// 	page: '/renling/kpl',
				// 	prod: true,
				// },
				// {
				// 	title: 'Master',
				// 	module: 'renling',
				// 	icon: 'fa fa-database',
				// 	root: true,
				// 	prod: true,
				// 	submenu: [{
				// 		title: 'Sungai',
				// 		page: "/renling/master/sungai",
				// 	},
				// 		{
				// 			title: 'IUP',
				// 			page: "/renling/master/iup",
				// 		},
				// 		{
				// 			title: 'Kabupaten',
				// 			page: "/renling/master/kabupaten",
				// 		},
				// 		{
				// 			title: 'Sumber Air',
				// 			page: "/renling/master/sumber-air",
				// 		},
				// 	],
				// },


				// ---------------------------- End Renling ----------------------------
				// --------------------------- Start UMKM  ---------------------------
				// {
				// 	title: 'Report',
				// 	module: 'umkm',
				// 	icon: 'fa fa-th-large',
				// 	root: true,
				// 	page: '/umkm/report',
				// 	prod: true,
				// },
				{
					title: 'Dashboard',
					module: 'umkm',
					icon: 'fa fa-th-large',
					root: true,
					page: '/umkm/dashboard',
					prod: true,
				},
				{
					title: 'Transaksi',
					module: 'umkm',
					icon: 'fa flaticon2-supermarket',
					root: true,
					page: '/umkm/data/transaksi',
					prod: true,
				},
				{
					title: 'UMKM',
					module: 'umkm',
					icon: 'fa flaticon-map',
					root: true,
					page: '/umkm/data/umkm',
					prod: true,
				},
				{
					title: 'Rule Access',
					module: 'umkm',
					icon: 'fa flaticon2-user-1',
					root: true,
					page: '/umkm/access',
					prod: true,
				},
				// ---------------------------- End UMKM ----------------------------

				// --------------------- Start Daily Activities ---------------------
				{
					title: 'Dashboard',
					module: 'daily-activities',
					icon: 'fas fa-calendar-check',
					root: true,
					page: '/daily-activities/dashboard',
					prod: true,
				},
				{
					title: 'My Calendar',
					module: 'daily-activities',
					icon: 'fas fa-calendar',
					root: true,
					page: '/daily-activities/my-calendar',
					prod: true,
				},
				{
					title: 'My Activity',
					module: 'daily-activities',
					icon: 'fas fa-calendar-plus',
					root: true,
					page: '/daily-activities/my-activity',
					prod: true,
				},
				{
					title: 'My Satker',
					module: 'daily-activities',
					icon: 'fas fa-calendar-plus',
					root: true,
					page: '/daily-activities/my-satker',
					prod: true,
				},
				// ---------------------- End Daily Activities ----------------------


				//------------------------------- CMW START ----------------------------------
				{
					title: 'Dashboard',
					module: 'cmw',
					icon: 'fas fa-home',
					root: true,
					page: '/cmw/dashboard',
					prod: true,
				},
				{
					title: 'Job Order',
					module: 'cmw',
					page: '/cmw/joborder',
					icon: 'fas fa-toolbox',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Data Job Order',
							page: '/cmw/joborder/data',
						},
						{
							title: 'History Job Order',
							page: '/cmw/joborder/history',
						},

					],
				},
				{
					title: 'Activity',
					module: 'cmw',
					icon: 'fas fa-truck-pickup',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'APT',
							submenu: [
								{
									title:'Data APT',
									page: "/cmw/activity/apt/data",
								},
								{
									title:'HIstory APT',
									page: "/cmw/activity/apt/history",
								}
							]

						},
						{
							title: 'DT',
							submenu: [
								{
									title:'Data APT',
									page: "/cmw/activity/dt/data",
								},
								{
									title:'HIstory APT',
									page: "/cmw/activity/dt/history",
								}
							]
						},
						{
							title: 'Laporan',
							submenu: [
								{
									title: 'Create Laporan',
									page: "/cmw/activity/laporan/create"
								},
								{
									title: 'History Laporan',
									page: "/cmw/activity/laporan/history"
								}
							]
						},

					],
				},
				{
					title: 'Data Master',
					module: 'cmw',
					icon: 'fas fa-folder-open',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Equipment',
							page: "/cmw/master/equipment",
						},
						{
							title: 'Unit Kontrak',
							page: "/cmw/master/unit-kontrak"
						},
						{
							title: 'Unit Assignment',
							page: "/cmw/master/unit-assignment"
						},
						{
							title: 'SPPH',
							page: "/cmw/master/spph"
						},
						{
							title: 'Kategori Pekerjaan',
							page: "/cmw/master/kategori-pekerjaan"
						},
						{
							title: 'Lokasi',
							page: "/cmw/master/lokasi"
						},
						{
							title: 'Material',
							page: "/cmw/master/material"
						},
						{
							title: 'Cost Center',
							page: "/cmw/master/cost-center"
						},
					],
				},
				//-------------------------------CMW END ------------------------------------

				//------------------------------- KEloling START ----------------------------------
				{
					title: 'Dashboard',
					module: 'keloling',
					icon: 'fas fa-home',
					root: true,
					page: '/keloling/dashboard',
					prod: true,
				},
				{
					title: 'Job Order',
					module: 'keloling',
					page: '/keloling/joborder',
					icon: 'fas fa-toolbox',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Data Job Order',
							page: '/keloling/joborder/data',
						},
						{
							title: 'History Job Order',
							page: '/keloling/joborder/history',
						},

					],
				},
				{
					title: 'Activity',
					module: 'keloling',
					icon: 'fas fa-truck-pickup',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'APT',
							submenu: [
								{
									title:'Data APT',
									page: "/keloling/activity/apt/data",
								},
								{
									title:'HIstory APT',
									page: "/keloling/activity/apt/history",
								}
							]

						},
						{
							title: 'DT',
							submenu: [
								{
									title:'Data APT',
									page: "/keloling/activity/dt/data",
								},
								{
									title:'HIstory APT',
									page: "/keloling/activity/dt/history",
								}
							]
						},
						{
							title: 'Laporan',
							submenu: [
								{
									title: 'Create Laporan',
									page: "/keloling/activity/laporan/create"
								},
								{
									title: 'History Laporan',
									page: "/keloling/activity/laporan/history"
								}
							]
						},

					],
				},
				{
					title: 'Data Master',
					module: 'keloling',
					icon: 'fas fa-folder-open',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Equipment',
							page: "/keloling/master/equipment",
						},
						{
							title: 'Unit Kontrak',
							page: "/keloling/master/unit-kontrak"
						},
						{
							title: 'Unit Assignment',
							page: "/keloling/master/unit-assignment"
						},
						{
							title: 'SPPH',
							page: "/keloling/master/spph"
						},
						{
							title: 'Kategori Pekerjaan',
							page: "/keloling/master/kategori-pekerjaan"
						},
						{
							title: 'Lokasi',
							page: "/keloling/master/lokasi"
						},
						{
							title: 'Material',
							page: "/keloling/master/material"
						},
						{
							title: 'Cost Center',
							page: "/keloling/master/cost-center"
						},
					],
				},
				//-------------------------------KEloling END ------------------------------------


				{
					title: 'Dashboard',
					module: 'marketing',
					icon: 'fas fa-list',
					root: true,
					page: '/marketing/dashboard',
					prod: true,
				},
				{
					title: "Monitoring Kontrak",
					bullet: "dot",
					module: "marketing",
					prod: true,
					submenu: [{
						title: "Dashboard Monitoring",
						page: "/marketing/monitoring/dashboard",
					},
						{
							title: "Monitoring Kontrak",
							page: "/marketing/monitoring/monitoring"
						}
					]
				},
				{
					title: 'Laporan',
					module: 'marketing',
					icon: 'fas fa-list',
					root: true,
					page: '/marketing/reports',
					prod: false,
				},
				{
					title: 'Dokumen',
					module: 'marketing',
					icon: 'fas fa-list',
					root: true,
					prod: false,
					submenu: [
						{ title: 'Full Company Offers', page: '/marketing/fco' },
					],
				},
				// --------------------------- Start ERM  ---------------------------

				{
					title: 'Dashboard',
					module: 'erm',
					icon: 'fa fa-th-large',
					root: true,
					page: '/erm/dashboard',
					prod: true,
				},
				{
					title: 'To Do List',
					module: 'erm',
					icon: 'fas fa-tasks	',
					root: true,
					prod: true,
					submenu: [{
						title: 'Risk Library',
						page: "/erm/todolist/risklibrary",
						}, {
							title: 'Business Process Model',
							page: "/erm/todolist/bpm",
						}
					],
				},
				{
					title: 'Business Process Model',
					module: 'erm',
					icon: 'fas fa-tools',
					root: true,
					page: '/erm/bpm',
					prod: true,
				},
				{
					title: 'Risk Matrix',
					module: 'erm',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/erm/riskmatrix',
					prod: true,
				},
				{
					title: 'Risk Library',
					module: 'erm',
					icon: 'fa fa-list',
					root: true,
					page: '/erm/risklibrary',
					prod: true,
				},
				{
					title: 'Risk Assessment',
					module: 'erm',
					icon: 'fa flaticon-clipboard',
					root: true,
					page: '/erm/riskassessment',
					prod: true,
				},
				{
					title: 'Risk Treatment',
					module: 'erm',
					icon: 'fa flaticon-interface-10',
					root: true,
					page: '/erm/risktreatment',
					prod: true,
				},
				{
					title: 'Risk Profile Compilation',
					module: 'erm',
					icon: 'fas fa-toolbox',
					root: true,
					page: '/erm/rpc',
					prod: true,
				},
				{
					title: 'Key Risk Indicator',
					module: 'erm',
					icon: 'fa fa-pencil-square-o',
					root: true,
					page: '/erm/kri',
					prod: true,
				},
				{
					title: 'Master',
					module: 'erm',
					icon: 'fa fa-database',
					root: true,
					prod: true,
					submenu: [{
						title: 'Role',
						page: "/erm/master/role",
					},
						{
							title: 'User Role',
							page: "/erm/master/user",
						},
					],
				},


				// ---------------------------- End ERM ----------------------------
				// --------------------------- Start Mercusuar  ---------------------------
				{
					title: 'Dashboard',
					module: 'mercusuar',
					icon: 'fa fa-th-large',
					root: true,
					page: '/mercusuar/dashboard',
					prod: true,
				},
				{
					title: 'Monitor Jaringan',
					module: 'mercusuar',
					icon: 'fa fa-th-large',
					root: true,
					page: '/mercusuar/monitor',
					prod: true,
				},
				{
					title: 'Master',
					module: 'mercusuar',
					icon: 'fa fa-database',
					root: true,
					prod: true,
					submenu: [{
						title: 'Network',
						page: "/mercusuar/master/network",
					},
					],
				},

				// ---------------------------- End Mercusuar ----------------------------

				// --------------------------- Start ECMS  ---------------------------
				{
					title: 'Document',
					module: 'ecms',
					icon: 'fa fa-file-alt',
					root: true,
					page: '/ecms/document/2',
					prod: true,
				},
                {
					title: 'Trash',
					module: 'ecms',
					icon: 'fa fa-trash',
					root: true,
					page: '/ecms/document/1',
					prod: true,
				},
				{
					title: 'Configuration',
					module: 'ecms',
					icon: 'fa fa-cogs',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Series',
							page: "/ecms/series",
						},
						{
							title: 'Categories',
							page: "/ecms/categories",
						},
						{
							title: 'Document Types',
							page: "/ecms/doctypes",
						},
                        {
							title: 'Permission by Group',
							page: "/ecms/permission-group",
						},
                        {
							title: 'Permission by Role',
							page: "/ecms/permission-role",
						},
                        {
							title: 'Predefined Account',
							page: "/ecms/docaccounts",
						},
                        {
							title: 'User Access',
							page: "/ecms/user-access",
						},
					],
				},
				// ---------------------------- End ECMS ----------------------------

				// --------------------------- Start MOST  ---------------------------
				{
					title: 'Master',
					module: 'most',
					icon: 'fa fa-file-alt',
					root: true,
					page: '/most/master',
					prod: true,
				},
				// ---------------------------- End MOST ----------------------------
				// ---------------------------- GADIS START -------------------------------
				{
					title: 'Todolist',
					module: 'gadis',
					icon: 'fas fa-th-list',
					root: true,
					page: '/gadis/todolist',
					prod: true,
				},
				{
					title: 'Data Pengajuan',
					module: 'gadis',
					icon: 'fas fa-th-list',
					root: true,
					page: '/gadis/datapengajuan',
					prod: true,
				},
				{
					title: 'Input Permintaan',
					module: 'gadis',
					icon: 'fas fa-file-alt',
					root: true,
					page: '/gadis/form-bon-konsumsi',
					prod: true,
				},
				{
					title: 'Report',
					module: 'gadis',
					icon: 'fas fa-folder-open',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Bon Layum',
							page: "/gadis/report/bon-konsumsi",
						},
					],
				},
				{
					title: 'Master Data',
					module: 'gadis',
					icon: 'fa fa-database',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Konsumsi / Galon',
							page: "/gadis/master/konsumsi",
						},
						{
							title: 'Vendor',
							page: "/gadis/master/vendor-gadis",
						},
						{
							title: 'Galon',
							page: "/gadis/master/galon",
						},
					],
				},
				{
					title: 'History',
					module: 'gadis',
					icon: 'fas fa-clock',
					root: true,
					page: '/gadis/history',
					prod: true,
				},
				{
					title: 'Role Access',
					module: 'gadis',
					icon: 'fas fa-users',
					root: true,
					page: '/gadis/roleaccess',
					prod: true,
				},
				// ---------------------------- GADIS END --------------------------------
				// ---------------------------- SURVEY START -------------------------------
				{
					title: 'Kuisioner',
					module: 'survey',
					icon: 'fas fa-th-list',
					root: true,
					page: '/survey/kuisioner',
					prod: false,
				},
				{
					title: 'Kuisioner Input Pertanyaan',
					module: 'survey',
					icon: 'fas fa-th-list',
					root: true,
					page: '/survey/input-pertanyaan',
					prod: false,
				},
				// ---------------------------- SURVEY END --------------------------------
				// ---------------------------- GCG START -------------------------------
				{
					title: 'To Do List',
					module: 'gcg',
					icon: 'fas fa-th-list',
					badge: { type: 'kt-badge--primary', value: localStorage.getItem("gcg_badge_todolist") },
					root: true,
					page: '/gcg/todo-list',
					prod: false,
				},
				{
					title: 'Informasi',
					module: 'gcg',
					icon: 'fas fa-info-circle',
					root: true,
					page: '/gcg/dokumen/17910',
					prod: false,
				},
				{
					title: 'News',
					module: 'gcg',
					icon: 'fas fa-newspaper',
					root: true,
					bullet: "dot",
					prod: false,
					submenu: [
						{
							title: 'Sosialisasi',
							page: "/gcg/news/sosialisasi",
						},
						{
							title: 'Capaian',
							page: "/gcg/news/capaian",
						},
					],
				},
				{
					title: 'Quiz Pemahaman GCG',
					module: 'gcg',
					icon: 'fas fa-clipboard-list',
					root: true,
					page: '/gcg/quiz-pemahaman',
					prod: false,
				},
				{
					title: 'Code of Conduct',
					module: 'gcg',
					icon: 'fas fa-file-signature',
					root: true,
					bullet: "dot",
					prod: false,
					submenu: [
						{
							title: 'Pribadi',
							page: "/gcg/coc",
						},
						{
							title: 'Bawahan',
							page: "/gcg/coc/bawahan",
						},
					],
				},
				{
					title: 'Q&A',
					module: 'gcg',
					icon: 'fas fa-question-circle',
					root: true,
					page: '/gcg/qna',
					prod: false,
				},
				{
					title: 'Configuration',
					module: 'gcg',
					icon: 'fas fa-cogs',
					root: true,
					bullet: "dot",
					prod: false,
					submenu: [
						{
							title: 'Administrator',
							page: "/gcg/administrator",
						},
						{
							title: 'Quiz',
							page: "/gcg/setting-quiz",
						},
						{
							title: 'Quiz Pemahaman GCG',
							page: "/gcg/setting-quiz-pemahaman",
						},
						{
							title: 'Coc',
							page: "/gcg/setting-coc",
						},
					],
				},
				// ---------------------------- GCG END --------------------------------
				// ---------------------------- SATKER START -------------------------------
				// {
				// 	title: 'Dashboard',
				// 	module: 'dashdiv',
				// 	icon: "fa fa-th-large",
				// 	root: true,
				// 	page: '/dashdiv/dashboard',
				// 	prod: false,
				// },
				// ---------------------------- SATKER END --------------------------------

                // --------------------------- Start SCT  ---------------------------
				{
					title: 'Dashboard',
					module: 'sct',
					icon: 'fa fa-chart-pie',
					root: true,
					page: '/sct/dashboard',
					prod: true,
				},
                {
					title: 'Finance',
					module: 'sct',
					icon: 'fa fa-dollar-sign',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Balance Sheet',
							page: "/sct/balance-sheet",
						},
						{
							title: 'Profit Loss',
							page: "/sct/profit-loss",
						},
                        {
							title: 'Cash Flow',
							page: "/sct/cash-flow",
						},
                        {
							title: 'Financial Ratio',
							page: "/sct/finance-ratio",
						},
                        {
							title: 'Account Receivables',
							page: "/sct/account-receivables",
						},
                        {
							title: 'Account Payables',
							page: "/sct/account-payables",
						},
                        {
							title: 'Investment',
							page: "/sct/investment",
						},
					],
				},
                {
					title: 'Operation',
					module: 'sct',
					icon: 'fa fa-th-large',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Production',
							page: "/sct/production",
						},
						{
							title: 'Stockpile',
							page: "/sct/stockpile",
						},
					],
				},
                {
					title: 'Report',
					module: 'sct',
					icon: 'fa fa-file-alt',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Balance Sheet',
							page: "/sct/report/balance-sheet",
						},
						{
							title: 'Profit Loss',
							page: "/sct/report/profit-loss",
						},
                        {
							title: 'Cash Flow',
							page: "/sct/report/cash-flow",
						},
                        {
							title: 'Financial Ratio',
							page: "/sct/report/finance-ratio",
						},
                        {
							title: 'Account Receivables',
							page: "/sct/report/account-receivables",
						},
                        {
							title: 'Account Payables',
							page: "/sct/report/account-payables",
						},
                        {
							title: 'Investment',
							page: "/sct/report/investment",
						},
					],
				},
                {
					title: 'Configuration',
					module: 'sct',
					icon: 'fa fa-cogs',
					root: true,
					prod: true,
					submenu: [
                        {
							title: 'Subsidiaries',
							page: "/sct/subsidiary",
						},
                        /*
                        {
							title: 'Vendor',
							page: "/sct/vendor",
						},
						{
							title: 'User Roles',
							page: "/sct/user-role",
						},
                        */
                        {
							title: 'User Access',
							page: "/sct/user-access",
						},
					],
				},

				// ---------------------------- End SCT ----------------------------

				//-----------------------------------START BLASTING ----------------------------
				{
					title: 'Dashboard',
					module: 'blasting',
					icon: 'fas fa-bomb',
					root: true,
					prod: true,
					page: '/blasting/dashboard',
				},
				{
					title: 'Blasting Activity',
					module: 'blasting',
					icon: 'fas fa-bomb',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Data Blasting',
							page: "/blasting/blast/activity",
						},
						{
							title: 'History Blasting',
							page: "/blasting/blast/history",
						},
					],
				},
				{
					title: 'Blasting Vibration <br> Monitoring',
					module: 'blasting',
					icon: 'fas fa-wave-square',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Data Blasting Vibration ',
							page: "/blasting/vibration/activity",
						},
						{
							title: 'History Blasting Vibration',
							page: "/blasting/vibration/history",
						},

					],
				},
				{
					title: 'Drilling Activity',
					module: 'blasting',
					icon: 'fas fa-bomb',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Drill Activity',
							page: "/blasting/drill/plan",
						},
						{
							title: 'Drill Machinery Performance',
							page: "/blasting/drill/machinery-performance",
						},
						{
							title: 'Drill Report',
							page: "/blasting/drill/report",
						},
					],
				},
				{
					title: 'Exsplosive Stock  <br>& License Monitoring',
					module: 'blasting',
					icon: 'fas fa-file-alt',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Exsplosive Stock',
							page: "/blasting/stock-license/stock",
						},
						{
							title: 'License Monitoring',
							page: "/blasting/license",
						},
					],
				},
				{
					title: 'Data Master',
					module: 'blasting',
					icon: 'fas fa-file-alt',
					root: true,
					prod: true,
					submenu: [
						{
							title: 'Front',
							page: "/blasting/data-master/front",
						},
						{
							title: 'Seam',
							page: "/blasting/data-master/seam",
						},
						{
							title: 'Blaster',
							page: "/blasting/data-master/blaster",
						},
						{
							title: 'Equipment',
							page: "/blasting/data-master/equipment",
						},
						{
							title: 'Model',
							page: "/blasting/data-master/model",
						},
						{
							title: 'Tipe',
							page: "/blasting/data-master/tipe",
						},
						{
							title: 'Micromate Location',
							page: "/blasting/data-master/micromate",
						},
						{
							title: 'Cost Center',
							page: "/blasting/data-master/cost-center",
						},
						{
							title: 'Tie-Up',
							page: "/blasting/data-master/tie-up",
						},
						{
							title: 'Status Peledakan',
							page: "/blasting/data-master/status-peledakan",
						},
					],
				},

				//-----------------------------------END BLASTIN -------------------------------

				//#region Whistleblower
				{
					title: 'Dashboard',
					module: 'wbs',
					icon: 'fas fa-file-alt',
					root: true,
					page: '/wbs/report',
					prod: false,
				},
				{
					title: 'Broadcast List',
					module: 'wbs',
					icon: 'fas fa-file-alt',
					root: true,
					page: '/wbs/broadcast',
					prod: false,
				},
				{
					title: 'User List',
					module: 'wbs',
					icon: 'fas fa-file-alt',
					root: true,
					prod: false,
					submenu: [
						{ title: 'Telegram', page: '/wbs/log/telegram' },
					],
				},
				//#endregion
			],
		},
	};

	public get configs(): MenuConfiguration {
		return this.defaults;
	}
}
