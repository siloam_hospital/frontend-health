// USA
export const locale = {
	lang: 'id',
	data: {
		TRANSLATOR: {
			SELECT: 'Select your language',
		},
		MENU: {
			NEW: 'new',
			ACTIONS: 'Actions',
			CREATE_POST: 'Create New Post',
			PAGES: 'Pages',
			FEATURES: 'Features',
			APPS: 'Apps',
			DASHBOARD: 'Dashboard',
			NO: 'Tidak',
			YES: 'Ya',
			PREVIOUS: 'Sebelumnya',
			SUBMIT: 'Kirim',
			NEXT_STEP: 'Selanjutnya',
			SUBMITTED_DATE:'Tanggal Pengisian',
			EXP_DATE:'Tanggal Berakhir'
		},
		AUTH: {
			GENERAL: {
				OR: 'Or',
				SUBMIT_BUTTON: 'Submit',
				NO_ACCOUNT: 'Don\'t have an account?',
				SIGNUP_BUTTON: 'Sign Up',
				FORGOT_BUTTON: 'Forgot Password',
				BACK_BUTTON: 'Back',
				PRIVACY: 'Privacy',
				LEGAL: 'Legal',
				CONTACT: 'Contact',
			},
			LOGIN: {
				TITLE: 'Login Account',
				BUTTON: 'Sign In',
			},
			FORGOT: {
				TITLE: 'Forgotten Password?',
				DESC: 'Enter your email to reset your password',
				SUCCESS: 'Your account has been successfully reset.'
			},
			REGISTER: {
				TITLE: 'Sign Up',
				DESC: 'Enter your details to create your account',
				SUCCESS: 'Your account has been successfuly registered.'
			},
			INPUT: {
				EMAIL: 'Email',
				FULLNAME: 'Fullname',
				PASSWORD: 'Password',
				CONFIRM_PASSWORD: 'Confirm Password',
				USERNAME: 'Username'
			},
			VALIDATION: {
				INVALID: '{{name}} is not valid',
				REQUIRED: '{{name}} is required',
				MIN_LENGTH: '{{name}} minimum length is {{min}}',
				AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
				NOT_FOUND: 'The requested {{name}} is not found',
				INVALID_LOGIN: 'The login detail is incorrect',
				REQUIRED_FIELD: 'Required field',
				MIN_LENGTH_FIELD: 'Minimum field length:',
				MAX_LENGTH_FIELD: 'Maximum field length:',
				INVALID_FIELD: 'Field is not valid',
			}
		},
		GENDER: {
			MALE: 'Pria',
			FEMALE: 'Wanita'
		},
		HOME: {
			TITTLE: 'Form Deklarasi Kesehatan',
			P1: 'Rumah sakit memberlakukan proses skrining untuk mencegah penyebaran COVID-19 dan menurunkan risiko paparan terhadap pengunjung dan staf rumah sakit. Untuk mempercepat proses tersebut, Anda dapat mengisi form digital di bawah ini.',
			P1_CHILD: [
				'Mohon isi kuesioner ini pada hari yang sama dengan hari kunjungan ke rumah sakit.',
				'Kuesioner diisi oleh setiap pengunjung rumah sakit.',
				'Setiap pasien hanya dapat didampingi oleh 1 orang.',
				'Mohon tunjukkan hasil kuesioner di ponsel Anda kepada staf di pintu masuk.'
			],
			P2: 'Pengisian formulir elektronik di Siloam Hospitals akan dilakukan secara bertahap dengan urutan sebagai berikut:',
			P2_CHILD: [
				'Siloam Hospitals & RSUS Lippo Village',
				'Siloam Hospitals di area DKI Jakarta',
				'Siloam Hospitals seluruh Indonesia'
			],
			P3: 'Mohon ikuti prosedur yang berlaku di masing-masing Siloam Hospitals yg Anda kunjungi, sesuai dengan arahan petugas.',
			REFERENCES: 'Referensi',
			REFERENCES_DETAIL: 'Ministry of Health Republic of Indonesia CDC WHO',
			START: 'Memulai'
		},
		STEP1: {
			TITTLE: 'Mohon lengkapi informasi diri Anda.',
			LABEL: 'Informasi Pribadi',
			NAME: 'Nama Lengkap',
			GENDER: 'Jenis Kelamin',
			DATE_BIRTH: 'Tanggal Lahir',
			PHONE: 'Nomor Handphone',
			EMAIL: 'Email',
			VISIT_SILOAM: 'Rumah Sakit Siloam yang dikunjungi'
		},
		STEP2: {
			TITTLE: 'Apakah Anda memiliki hasil PCR atau antigen dalam kurun waktu <10 hari dengan hasil non-reaktif ?',
			LABEL: 'Hasil Reaktif'
		},
		STEP3: {
			TITTLE: 'Apakah Anda memiliki gejala berikut dalam 14 hari?',
			LABEL: 'Gejala',
			FEVER: 'Demam atau riwayat demam',
			BREATHING: 'Sesak / kesulitan bernapas',
			COUGHT: 'Batuk atau riwayat batuk',
			RUNNY_NOSE: 'Pilek',
			SMELL: 'Kehilangan daya penciuman',
			TASTE: 'Kehilangan daya pengecap',
			ACHES: 'Nyeri otot',
			VOMITING: 'Muntah atau diare',
			AGREEMENT: 'Dengan ini saya menyatakan bahwa informasi yang diberikan diatas adalah sesuai dengan yang Saya ketahui, lengkap dan benar.'
		},
		STEP4: {
			TITTLE: '',
			LABEL: 'Pratinjau',
		},
		STEP5: {
			TITTLE: 'Apakah Anda terpapar dengan seseorang yang terdiagnosis COVID-19 dalam 14 hari terakhir?',
			LABEL:'Terpapar COVID-19',
			LIVE_SOMEONE: 'Saya tinggal bersama seseorang yang terdiagnosis COVID-19',
			CLOSE_CONTACT: 'Saya pernah kontak erat (berada dalam jarak 2 meter) dengan seseorang yang terdiagnosis COVID-19',
			WITH_SOMEONE: 'Saya tinggal bersama seseorang yang terdiagnosis COVID-19, namun saat ini sudah sembuh',
			CLOSE_CONTACT_RECOVERED: 'Saya pernah kontak erat (berada dalam jarak 2 meter) dengan seseorang yang terdiagnosis COVID-19, namun saat ini sudah sembuh',
			TREATING_PATIENTS:'Apakah Anda bekerja di fasilitas kesehatan yang merawatpasien COVID-19 dalam 14 hari terakhir?'
		},
		COMPLETE: {
			THANKS: "Terima Kasih Telah Mengisi Form",
			THANKS1: 'Mohon tunjukkan hasil kuesioner ini kepada staf di pintu masuk',
			THANKS2: 'Terima Kasih atas kerjasamanya dalam mengisi form deklarasi kesehatan dalam rangka menjaga penyebaran dan mengurangi resiko terpapar COVID-19. Berdasarkan jawaban Anda, hasil Anda',
			EVIDENCE: 'Bukti Rapid Diagnostic Test dalam kurun waktu 10 hari terakhir dengan hasil non-reaktif :',
			SYMPTOMS: 'Gejala dalam 14 hari terakhir :',
			EXPOSURE: 'Terpapar dengan seseorang yang terdiagnosis COVID-19 dalam 14 hari terakhir :',
			WORKED: 'Bekerja di fasilitas kesehatan yang merawatpasien COVID-19 dalam 14 hari terakhir :'
		}
	}
};
