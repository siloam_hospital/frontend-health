// USA
export const locale = {
	lang: 'en',
	data: {
		TRANSLATOR: {
			SELECT: 'Select your language',
		},
		MENU: {
			NEW: 'new',
			ACTIONS: 'Actions',
			CREATE_POST: 'Create New Post',
			PAGES: 'Pages',
			FEATURES: 'Features',
			APPS: 'Apps',
			DASHBOARD: 'Dashboard',
			NO: 'No',
			YES: 'Yes',
			PREVIOUS: 'Previous',
			SUBMIT: 'Submit',
			NEXT_STEP: 'Next Step',
			SUBMITTED_DATE:'Submitted Date',
			EXP_DATE:'Exp Date'
		},
		AUTH: {
			GENERAL: {
				OR: 'Or',
				SUBMIT_BUTTON: 'Submit',
				NO_ACCOUNT: 'Don\'t have an account?',
				SIGNUP_BUTTON: 'Sign Up',
				FORGOT_BUTTON: 'Forgot Password',
				BACK_BUTTON: 'Back',
				PRIVACY: 'Privacy',
				LEGAL: 'Legal',
				CONTACT: 'Contact',
			},
			LOGIN: {
				TITLE: 'Login Account',
				BUTTON: 'Sign In',
			},
			FORGOT: {
				TITLE: 'Forgotten Password?',
				DESC: 'Enter your email to reset your password',
				SUCCESS: 'Your account has been successfully reset.'
			},
			REGISTER: {
				TITLE: 'Sign Up',
				DESC: 'Enter your details to create your account',
				SUCCESS: 'Your account has been successfuly registered.'
			},
			INPUT: {
				EMAIL: 'Email',
				FULLNAME: 'Fullname',
				PASSWORD: 'Password',
				CONFIRM_PASSWORD: 'Confirm Password',
				USERNAME: 'Username'
			},
			VALIDATION: {
				INVALID: '{{name}} is not valid',
				REQUIRED: '{{name}} is required',
				MIN_LENGTH: '{{name}} minimum length is {{min}}',
				AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
				NOT_FOUND: 'The requested {{name}} is not found',
				INVALID_LOGIN: 'The login detail is incorrect',
				REQUIRED_FIELD: 'Required field',
				MIN_LENGTH_FIELD: 'Minimum field length:',
				MAX_LENGTH_FIELD: 'Maximum field length:',
				INVALID_FIELD: 'Field is not valid',
			}
		},
		GENDER: {
			MALE: 'Male',
			FEMALE: 'Female'
		},
		HOME: {
			TITTLE: 'Health Declaration Form',
			P1: 'Our hospitals are currently operating a screening process to prevent the spread of COVID-19 and reduce the risk of exposure to visitors and staff. To speed up the process, please fill in this digital form.',
			P1_CHILD: [
				'Please fill in this questionnaire on the same day as the hospital visit.',
				'This questionnaire should be completed by every visitor to the hospital.',
				'Each patient can only be accompanied by 1 person.',
				'Please show the results of this questionnaire to staff at the entrance.'
			],
			P2: 'The usage of electronic health declaration forms at Siloam Hospitals will be implemented in a few stages, according to this order:',
			P2_CHILD: [
				'Siloam Hospitals & RSUS Lippo Village',
				'Siloam Hospitals in DKI Jakarta area',
				'Siloam Hospitals in Indonesia'
			],
			P3: 'Please follow the procedure within each Siloam Hospitals you visit, according to our officers\' guidance.',
			REFERENCES: 'References',
			REFERENCES_DETAIL: 'Ministry of Health Republic of Indonesia CDC WHO',
			START: 'Start'
		},
		STEP1: {
			TITTLE: 'Please complete your personal information.',
			LABEL: 'Personal Information',
			NAME: 'Full Name',
			GENDER: 'Gender',
			DATE_BIRTH: 'Date of Birth',
			PHONE: 'Handphone Number',
			EMAIL: 'Email',
			VISIT_SILOAM: 'Visit Siloam Hospital'
		},
		STEP2: {
			TITTLE: 'Do you have PCR or antigen test result within the last 10 days with a non-reactive result?',
			LABEL: 'Reactive Result'
		},
		STEP3: {
			TITTLE: 'Do you have any of these following symptoms within the last 14 days?',
			LABEL: 'Symptoms',
			FEVER: 'Fever or history of fever',
			BREATHING: 'Breathing difficulty',
			COUGHT: 'Cough or history of cough',
			RUNNY_NOSE: 'Runny nose',
			SMELL: 'Losing sense of smell',
			TASTE: 'Losing sense of taste',
			ACHES: 'Body aches',
			VOMITING: 'Vomiting or diarrhea',
			AGREEMENT: 'I hereby declare that the above information provided is to the best of my knowledge, complete and true.'
		},
		STEP4: {
			TITTLE: '',
			LABEL: 'Preview',
		},
		STEP5: {
			TITTLE: 'What is your exposure to others who are known to have COVID-19 within the last 14 days?',
			LABEL:'Exposure COVID-19',
			LIVE_SOMEONE: 'I live with someone who has COVID-19',
			CLOSE_CONTACT: 'I\'ve had close contact (within 2 meters) with someone who has COVID-19',
			WITH_SOMEONE: 'I live with someone who has recoverd from COVID-19',
			CLOSE_CONTACT_RECOVERED: 'I\'ve had close contact (within 2 meters) with someone who has recovered from COVID-19',
			TREATING_PATIENTS:'Have you worked in any health care facility which care for COVID-19 patients within the last 14 days'
		},
		COMPLETE: {
			THANKS: "Thank You for Completing the Form",
			THANKS1: 'Please show the results of this questionnaire to staff at the entrance',
			THANKS2: 'Thank you for your cooperation in filling Health Declaration Form in order to prevent the spread of COVID-19 and reduce the risk of exposure to visitors and staff. Based on your answers, your result is',
			EVIDENCE: 'Evidence of a rapid diagnostic test within the last 10 days with a non-reactive result :',
			SYMPTOMS: "Symptoms within the last 14 days :",
			EXPOSURE: "Exposure to others who are known to have COVID-19 within the last 14 days :",
			WORKED: 'Worked in any health care facility which care for COVID-19 patients within the last 14 days :'
		}
	}
};
