import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {  environment} from '../../../environments/environment';
import { EncrDecrService } from "../../core/EncrDecr/encr-decr.service";
//Di Pakai Hanya untuk CRUD Ringan/Simple

@Injectable({
  providedIn: 'root'
})

export class Database {
  httpOptions: any = [];

  constructor(private http: HttpClient,private EncrDecr: EncrDecrService,) {
    let type;
		environment.APIUrl.split("/").findIndex(item => {
			if (item === "v2") { //berlaku res data encrypt
				type='text';
				return true;
			}else{
				type='json';
			}
		})
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/'+type+';charset=utf-8',
       // 'Authorization': 'Bearer '+environment.DBToken
      })
    };
  }

  select(query){    
    return new Promise<any>((resolve, reject) => {
      this.http.get(environment.APIUrl + "auth/accessapi/?query="+query,this.httpOptions)
      .subscribe(res=>{
        res = this.EncrDecr.response(res);
        if(res['status']){
          resolve(res['data'])
        }else{
          reject(res)
        }
      },err=>reject(err));
    })
  }

  insert(query) {
    return new Promise<any>((resolve, reject) => {
      this.http.post(environment.APIUrl + "auth/accessapi/?query=" + query, this.httpOptions)
        .subscribe(res => {
          res = this.EncrDecr.response(res);
          if (res['status']) {
            resolve(res['data'])
          } else {
            reject(res)
          }
        }, err => reject(err));
    })
  }

}
