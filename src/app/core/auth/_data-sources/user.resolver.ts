import { Injectable } from "@angular/core";
import {
	Resolve,
	ActivatedRouteSnapshot,
	ActivatedRoute,
	Router,
	RouterStateSnapshot
} from "@angular/router";
import { User } from "../../../core/auth/_models/user.model";
import { EncrDecrService } from "../../../core/EncrDecr/encr-decr.service";

import { environment } from "../../../../environments/environment";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { AppService } from "../../../app.service";
import { DeviceDetectorService } from "ngx-device-detector";
import { PermissionService } from "../../../core/auth/_permissions/permission.service";
import { EventEmitterService } from "../../../event-emitter.service";
@Injectable()
export class UserResolver implements Resolve<User> {
	constructor(
		private appService: AppService,
		private router: Router,
		private route: ActivatedRoute,
		private http: HttpClient,
		private EncrDecr: EncrDecrService,
		private deviceService: DeviceDetectorService,
		private permission: PermissionService,
		private activatedRoute: ActivatedRoute,
		private eventEmitterService: EventEmitterService
	) {}

	resolve(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Promise<User> {
		let user = new User();
		return new Promise((resolve, reject) => {
			let apiToken = this.EncrDecr.get(
				localStorage.getItem(environment.authTokenKey)
			);
			let path;
			if (apiToken["status"]) {
				let dataAPI = JSON.parse(apiToken["data"]);
				user["refreshToken"] = dataAPI["refreshToken"];
				user["username"] = dataAPI["username"];
				user["accessToken"] = dataAPI["token"];
				user["id"] = dataAPI["id"];
				user["employeeID"] = dataAPI["employeeID"];
				user["position"] = dataAPI["position"];
				user["user_level"] = dataAPI["user_level"];
				user["id_perusahaan"] = dataAPI["id_perusahaan"];
				user["pic"] =
					environment.APIUrl +
					"/images/photo/E" +
					dataAPI["employeeID"] +
					".gif";
				user["module"] = dataAPI["module"];
				let tree = this.router.parseUrl(state.url);
				let children = tree.root.children["primary"];
				let segments = children.segments;
				this.eventEmitterService.onFirstComponentLoad({
					name: segments[0]["path"],
					link: segments[0]["path"]
				});
				setTimeout(() => {
					this.eventEmitterService.onFirstComponentLoad({
						name: segments[0]["path"],
						link: segments[0]["path"]
					});
				}, 500);
				path = segments[0]["path"];
			} else {
				path = false;
			}

			this.permission.access(path).then( 
				res => {
					let device = null;
					if (this.deviceService.isMobile()) {
						device = "mobile";
					} else if (this.deviceService.isTablet()) {
						device = "tablet";
					} else if (this.deviceService.isDesktop()) {
						device = "desktop";
					}
					user["responsive"] = {
						info: this.deviceService.getDeviceInfo(),
						device: device
					};
					user["roles"] = res;
					return resolve(user);
				},
				err => {
					return reject(err);
				}
			);
		});
	}
}
