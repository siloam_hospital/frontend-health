// Angular
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
// NGRX
import { select, Store } from '@ngrx/store';
// Auth reducers and selectors
import { AppState} from '../../../core/reducers/';
import { isLoggedIn } from '../_selectors/auth.selectors';
import { EncrDecrService} from '../../../core/EncrDecr/encr-decr.service';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
    Login,
    Logout,
    Register,
    UserRequested,
    UserLoaded,
    AuthActionTypes,
    AuthActions
} from '../../../core/auth/_actions/auth.actions';

@Injectable({
	providedIn: "root"
})
export class AuthGuard implements CanActivate {
    constructor(private http: HttpClient,private store: Store<AppState>, private router: Router,private EncrDecr: EncrDecrService,) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>  {
        return this.store
            .pipe(
                select(isLoggedIn),
                tap(loggedIn => {
                    console.log('Data Login',loggedIn)
                    if (!loggedIn) {
                        localStorage.clear(); 
                        let tree = this.router.parseUrl(state.url);
                        let children = tree.root.children["primary"];
                        let segments = children.segments;
                        let url="/auth/login?returnUrl=";
                        Object.keys(segments).forEach(key => {
                            url+="/"+segments[key]['path'];
                        })
                        this.router.navigateByUrl(url);
                        //this.router.navigate(["/auth/login"])
                    }else{
                        let timestamp = Math.floor(Date.now() / 1000)
                        let apiToken = this.EncrDecr.get(localStorage.getItem(environment.authTokenKey));
                        
                        let parsedUserData = JSON.parse(apiToken.data)
                        this.isExternalUser(parsedUserData.id_perusahaan)
                        this.isRedirectByModule(parsedUserData.module,parsedUserData.id_perusahaan)
                        if(apiToken['status']){
                            //
                            let dataAPI = JSON.parse(apiToken['data']);
                            if(new Date(dataAPI['exp']) < new Date()){

                                let httpOptions = {
                                    headers: new HttpHeaders({
                                      'Content-Type': 'application/json;charset=utf-8',
                                    })
                                  };
                                  
                                this.http.post(environment.APIUrl + "auth/token/"+timestamp,{
                                    'token':dataAPI['refreshToken'],
                                  },httpOptions)
                                  .subscribe(response => {
                                    if (response['status']) { 
                                        let token = this.EncrDecr.set(JSON.stringify({
                                            employeeID:response['data']['employeeID'],
                                            username:response['data']['username'],
                                            id:response['data']['id'],
                                            user_level:response['data']['user_level'],
                                            position:response['data']['position'],
                                            token:response['data']['token'],
                                            id_perusahaan:response["data"]["id_perusahaan"],
                                            refreshToken:response['data']['refreshToken'],
                                            exp:new Date(response['data']['exp']*1000)
                                        }));
                                        this.store.dispatch(new Login({authToken: token}));
                                    }else{
                                        this.store.dispatch(new Logout());
                                        //this.router.navigateByUrl("/");
                                        //this.router.navigate(["/auth/login"])
                                        //this.router.navigate(["/auth/login"]);
                                        localStorage.clear(); 
                                        let tree = this.router.parseUrl(state.url);
                                        let children = tree.root.children["primary"];
                                        let segments = children.segments;
                                        let url="/auth/login?returnUrl=";
                                        Object.keys(segments).forEach(key => {
                                            url+="/"+segments[key]['path'];
                                        })
                                        this.router.navigate(["'"+url+"'"]);
                                    }
                                  })
                            }else{

                            }
                        }else{
                            this.store.dispatch(new Logout());
                            localStorage.clear(); 
                            let tree = this.router.parseUrl(state.url);
                            let children = tree.root.children["primary"];
                            let segments = children.segments;
                            let url="/auth/login?returnUrl=";
                            Object.keys(segments).forEach(key => {
                                url+="/"+segments[key]['path'];
                            })
                            this.router.navigate(["'"+url+"'"]);
                            //this.router.navigate(["/auth/login"]);
                            //this.router.navigate(["/auth/login"])
                        }
                    }
                })
            );
    }

    isExternalUser(id){
        console.log(id)
        switch(id){
            case 6 : {
                console.log('here execute 6')
                this.router.navigateByUrl("/epa-kontraktor");
                break;
            }
            case 7: {
                console.log('here execute 7')
                this.router.navigateByUrl("/epa-kontraktor");
                break;
            }
            case 8: {
                console.log('here execute 8')
                this.router.navigateByUrl("sibaia-juri");
                break;
            }
            //satker
        }
    }

    isRedirectByModule(module : string='-',id_perusahaan : number = 1){
        if(typeof module !='undefined'){
            if(module.length > 1){
                let tmpModule = module.split(",");
                if(tmpModule.includes('69') && id_perusahaan == 10){
                    this.router.navigateByUrl("/dashdiv");
                }
            }
        }

    }
}


