import {
	BaseModel
} from '../../_base/crud';
import {
	Address
} from './address.model';
import {
	SocialNetworks
} from './social-networks.model';
import {
	environment
} from '../../../../environments/environment';
export class User extends BaseModel {
	id: number;
	employeeID: string;
	username: string;
	password: string;
	email: string;
	accessToken: string;
	refreshToken: string;
	roles: any;
	pic: string;
	fullname: string;
	occupation: string;
	companyName: string;
	phone: string;
	address: Address;
	socialNetworks: SocialNetworks;
	position: any;
	responsive: any;
	production: boolean = this.getProd();
	clear(): void {
		this.id = undefined;
		this.employeeID = undefined;
		this.username = '';
		this.password = '';
		this.email = '';
		this.roles = [];
		this.fullname = '';
		this.accessToken = undefined;
		this.refreshToken = undefined;
		this.pic = './assets/media/users/default.jpg';
		this.occupation = '';
		this.companyName = '';
		this.phone = '';
		this.address = new Address();
		this.address.clear();
		this.socialNetworks = new SocialNetworks();
		this.socialNetworks.clear();
		this.position = [];
		this.responsive = [];
	}
	getProd() {
        if (location.hostname == "36.92.52.245"){
            return true;
        }else if (location.hostname == 'localhost') {
			return false;
		} else if (location.hostname == "10.3.4.181") {
			return false;
		}
	}
}
