import {
	Injectable
} from '@angular/core';
import {
	AppService
} from '../../../app.service';
import {
	environment
} from '../../../../environments/environment';
import {
	EncrDecrService
} from '../../../core/EncrDecr/encr-decr.service';
import { select, Store } from '@ngrx/store';
import { AppState} from '../../../core/reducers/';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
    Login,
    Logout,
    Register,
    UserRequested,
    UserLoaded,
    AuthActionTypes,
    AuthActions
} from '../../../core/auth/_actions/auth.actions';
@Injectable({
	providedIn: 'root'
})
export class PermissionService {

	constructor(private http: HttpClient,private appService: AppService, private store: Store<AppState>,private EncrDecr: EncrDecrService) {}

	access(modul) {
		return new Promise < any > ((resolve, reject) => {
			if (modul) {
				let apiToken = this.EncrDecr.get(localStorage.getItem(environment.authTokenKey));
				if (apiToken['status']) {
					let dataAPI = JSON.parse(apiToken['data']);
					let body = {
						employeeID: dataAPI['employeeID'],
						modul: modul,
					}
					if(new Date(dataAPI['exp']) < new Date()){
						let httpOptions = {
							headers: new HttpHeaders({
							  'Content-Type': 'application/json;charset=utf-8',
							})
						  };
						  let timestamp = Math.floor(Date.now() / 1000)
						  this.http.post(environment.APIUrl + "auth/token/"+timestamp,{
							'token':dataAPI['refreshToken'],
						  },httpOptions)
						  .subscribe(response => {
							if (response['status']) { 
								let token = this.EncrDecr.set(JSON.stringify({
									employeeID:response['data']['employeeID'],
									id:response['data']['id'],
									user_level:response['data']['user_level'],
									position:response['data']['position'],
									token:response['data']['token'],
									refreshToken:response['data']['refreshToken'],
									exp:new Date(response['data']['exp']*1000)
								}));
								this.store.dispatch(new Login({authToken: token}));
								this.appService.post("user/access", body).toPromise().then(res => {
									return resolve(res['data']);
								}).catch(err => {
									return reject({
										error: false,
										message: err
									});
								});
							}else{
								this.store.dispatch(new Logout());
							}
						  })
					}else{
						this.appService.post("user/access", body).toPromise().then(res => {
							return resolve(res['data']);
						}).catch(err => {
							return reject({
								error: false,
								message: err
							});
						});
					}
					
				} else {
					return reject({
						error: false,
						message: "EncrDecr failed"
					});
				}
			} else {
				return resolve([]);
			}
		})
	}
}
