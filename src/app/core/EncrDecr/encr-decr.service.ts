import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {  environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class EncrDecrService {
  cryptoKey:string=CryptoJS.enc.Utf8.parse(environment.cryptoKey);
  cryptoiv:string=CryptoJS.enc.Utf8.parse(environment.cryptoiv);

  constructor() {
  }

  //The set method is use for encrypt the value.
  set(value,expire=null){
    if(expire!=null){
      expire = new Date(expire);
    }
    let data = {
      value:value,
      create:new Date(),
      expire:expire
    }
    return this.coreEncrypt(data);
  }

  //The get method is use for decrypt the value.
  get(value){
    return this.coreDecrypt(value,'json');
  }
  response(value){
    return this.coreDecrypt(value,'response');
  }

  coreDecrypt(value,type){
    let dataR = {
      data:null,
      status:false,
      create:null
    };
  try {
      var decrypted = CryptoJS.AES.decrypt(value, this.cryptoKey, {
          keySize: 128,
          iv: this.cryptoiv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
      });
      let datadec;
     
      if(type==='json'){
        datadec =JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
        dataR['data']=datadec['value'];
        dataR['create']=datadec['create'];
        dataR['status']=true;
        if (typeof datadec['expire'] !== 'undefined') {
          if(datadec['expire']!=null){
            let now =new Date();
            if(new Date(datadec['expire']) < now){
              dataR['status']=false;  
            }
          }
        }
        return dataR;
      }else if(type==='response'){
        return datadec =JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
      }else{
        datadec =decrypted.toString(CryptoJS.enc.Utf8);
        return datadec;
      }
  } catch (e) {
    if(type==='response'){
      return value;
    }else{
      dataR['data']=e;
      dataR['status']=false;
      return dataR;
    }
  }
  }
  coreEncrypt(value){
    let datastring = JSON.stringify(value);
    var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(datastring.toString()), this.cryptoKey,
    {
        keySize: 128,
        iv: this.cryptoiv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.toString();
  }
  bodyEncrypt(value,type){
    if(type){
      return this.coreEncrypt(value);
    }else{
      return value;
    }
    
  }
}
