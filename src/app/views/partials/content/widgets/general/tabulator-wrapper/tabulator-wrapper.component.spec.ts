import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabulatorWrapperComponent } from './tabulator-wrapper.component';

describe('TabulatorWrapperComponent', () => {
  let component: TabulatorWrapperComponent;
  let fixture: ComponentFixture<TabulatorWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabulatorWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabulatorWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
