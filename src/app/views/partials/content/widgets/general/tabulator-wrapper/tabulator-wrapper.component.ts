import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
} from '@angular/core';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import Tabulator from 'tabulator-tables';
import Swal from 'sweetalert2';
export type TabulatorColumnConfiguration = Tabulator.ColumnDefinition;
@Component({
	selector: 'kt-tabulator-wrapper',
	templateUrl: './tabulator-wrapper.component.html',
	styleUrls: ['./tabulator-wrapper.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class TabulatorWrapperComponent < T = any > implements OnInit, AfterViewInit, OnChanges, OnDestroy {
	@ViewChild('table', {
		static: true
	}) tableContainerRef: ElementRef < HTMLDivElement > ;
	tableRef: Tabulator;
	@Input('data') datasource: T[] | Observable < T[] > ;
	@Input('columns') columnConfig: TabulatorColumnConfiguration[];
	@Input('setColumns') setColumns: TabulatorColumnConfiguration[];
	@Input('enableAddRow') enableRowAdd: boolean;
	@Input('enableDownload') enableRowDownload: any={};
	@Input('enableSelection') enableSelection: boolean = true;
	@Input('enableNumberRow') enableNumberRow: boolean = true;
	@Input('headerVisible') headerVisible: boolean = true;
	@Input('addColumns') addColumns = [];

	@Input('rowAddText') rowAddText: string;
	@Input('reload') text: string;
	@Input('paginationSize') paginationSize: number;
	deleteSelectedText: string;
	@Output('rowAdded') rowAddedEvent: EventEmitter < T > = new EventEmitter(true);
	@Output('rowUpdated') rowUpdatedEvent: EventEmitter < T > = new EventEmitter(true);
	@Output('rowDeleted') rowDeletedEvent: EventEmitter < T > = new EventEmitter(true);
	@Output('dataChanged') dataChangedEvent: EventEmitter < T[] > = new EventEmitter(true);

	private _ds: Observable < T[] > ;
	dummy: boolean = true;
	subscriptions: Subscription[];
	selectedRowExist = new BehaviorSubject < boolean > (false)

	constructor(private cdr: ChangeDetectorRef) {
		this.subscriptions = new Array < Subscription > ();
	}
	ngOnChanges(changes: SimpleChanges): void {
		if (typeof changes.datasource !== 'undefined') {
			try {
				// console.log("---------------");
				// console.log(changes);
				// console.log(changes.datasource.currentValue);
				this.datasource = changes.datasource.currentValue;
				this._ds = this.datasource instanceof Observable ? this.datasource : of (this.datasource)
				//console.log("---------------");
				this.subscriptions.push(this._ds.subscribe(frame => {
					try {
						this.tableRef.replaceData(frame);
					} catch (error) {

					}
				}));
			} catch (error) {

			}
		}
		if (typeof changes.setColumns !== 'undefined') {
			try {
				let tempSetColumns = changes.setColumns.currentValue;
				if (this.enableNumberRow && this.check_duplicate('formatter', 'rownum')) {
					tempSetColumns.unshift({
						title: 'No',
						formatter: 'rownum',
						width: 60,
					});
				}
				if (this.enableSelection && this.check_duplicate('formatter', 'rowSelection')) {
					tempSetColumns.unshift({
						title: '',
						formatter: 'rowSelection',
						titleFormatter: 'rowSelection',
						hozAlign: 'center',
						headerSort: false,
						width: 50,
					});
				}
				this.tableRef.setColumns(tempSetColumns)
			} catch (error) {
				
			}
		}

		if (typeof changes.addColumns !== 'undefined') {
			this.addColumn(changes.addColumns);
		}
	}

	addRow() {
		if (this.tableRef) {
			this.tableRef.addRow();
		}
	}


	addColumn(data) {		
		Object.keys(data.currentValue).forEach(key => {
			this.tableRef.addColumn(data.currentValue[key]);
		})
		this.cdr.detectChanges();
	}

	deleteSelected() {
		Swal.fire({
				title: `Hapus data`,
				text: `Anda akan menghapus data ?`,
				icon: 'question',
				showCancelButton: true,
				focusCancel: true,
			})
			.then((res) => {
				if (res.value) {
					if (this.tableRef) {
						this.tableRef.getSelectedRows().forEach(r => {
							r.delete();
						});
					}
				}
			})
	}

	ngOnInit() {
		this._ds = this.datasource instanceof Observable ? this.datasource : of (this.datasource);
		// console.log(this.setColumns);
		
		// try {
		// 	this.setColumns.values
		// } catch (error) {
			
		// }
	}

	ngAfterViewInit() {
		if (this.enableNumberRow && this.check_duplicate('formatter', 'rownum')) {
			this.columnConfig.unshift({
				title: 'No',
				formatter: 'rownum',
				width: 60,
			});
		}

		if (this.enableSelection && this.check_duplicate('formatter', 'rowSelection')) {
			this.columnConfig.unshift({
				title: '',
				formatter: 'rowSelection',
				titleFormatter: 'rowSelection',
				hozAlign: 'center',
				headerSort: false,
				width: 50,
			});
		}

		this.tableRef = new Tabulator(this.tableContainerRef.nativeElement, {
			data: [],
			reactiveData: true,
			layout: 'fitDataFill',
			virtualDom: true,
			headerVisible: this.headerVisible,
			columns: this.columnConfig,
			rowAdded: (row => this.rowAddedEvent.emit(row.getData())),
			rowUpdated: (row => this.rowUpdatedEvent.emit(row.getData())),
			rowDeleted: (row => this.rowDeletedEvent.emit(row.getData())),
			dataChanged: (data => this.dataChangedEvent.emit(data)),
			pagination: this.paginationSize ? 'local' : null,
			paginationSize: this.paginationSize ? this.paginationSize : 10,
			rowSelectionChanged: (data => {
				this.selectedRowExist.next(data.length > 0);
			}),
			paginationAddRow: 'table',
			paginationSizeSelector: true,
			virtualDomBuffer:true,
			placeholder:"No Data Available",
			// ajaxProgressiveLoadDelay:200, //wait 200 milliseconds between each request
			// ajaxProgressiveLoadScrollMargin:300,
		});
		this.load();
	}
	load() {
		this.tableRef.columnManager.columnsByIndex.forEach(function (col) {
			if (col.visible) {
			  var width = col.definition.width;
			  if (width && typeof width === 'string' && width.indexOf('%') > -1) {
				col.setWidth(this.tableRef.element.clientWidth / 100 * parseInt(width));
			  }
			}
		  });
		this.subscriptions.push(this._ds.subscribe(frame => {
			try {
				this.tableRef.setData(frame);
				this.tableRef.redraw(true);
			} catch (error) {}
		}));

	}
	ngOnDestroy() {
		this.tableRef.destroy();
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}
	downloadXlsx(){
		console.log(this.enableRowDownload.type);
		this.tableRef.download(this.enableRowDownload.type, this.enableRowDownload.name+".xlsx", {sheetName:this.enableRowDownload.sheetName});	
	}
	check_duplicate(format, row) {
		let result = this.columnConfig.findIndex(item => {
			if (typeof item[format] != 'undefined') {
        if (item[format] == row) {
        return true;
        }
			}
		})
		if (result < 0) {
			return true;
		} else {
			return false;
		}
	}
}
