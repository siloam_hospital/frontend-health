import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnDestroy, SimpleChanges, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxDropzonePreviewComponent } from 'ngx-dropzone';
import { getDocument } from 'pdfjs-dist';
import { environment as env } from '../../../../../../../environments/environment';

type RemoteThumbnailState = 'idle' | 'loading' | 'loaded' | 'failed';

let PDFJS: typeof import('pdfjs-dist');

@Component({
  selector: 'kt-dropzone-remote-preview',
  templateUrl: './dropzone-remote-preview.component.html',
  styleUrls: ['./dropzone-remote-preview.component.scss'],
  providers: [
    {
      provide: NgxDropzonePreviewComponent,
      useExisting: DropzoneRemotePreviewComponent,
    },
  ],
})
export class DropzoneRemotePreviewComponent extends NgxDropzonePreviewComponent implements AfterViewInit, OnChanges, OnDestroy {
  @ViewChild('canvasRef', { static: true }) canvasRef: ElementRef<HTMLCanvasElement>;
  @Input('thumbnail') thumbnailRef: string = null;
  @Input('forceThumb') thumbOnly: boolean = false;
  _ref: HTMLImageElement = null;
  _flag: RemoteThumbnailState = 'idle';
  private _cmaps = typeof PDFJS !== 'undefined' ? `https://unpkg.com/pdfjs-dist@${PDFJS.version}/cmaps/` : null;

  constructor(
    sanitizer: DomSanitizer,
  ) {
    super(sanitizer);
  }

  cacheImage(data: string | ArrayBuffer) {
    return new Promise<void>((resolve, reject) => {
      this._flag = 'loading';
      this._ref = new Image();
      this._ref.src = data as string;
      this._ref.onload = () => {
        this._flag = 'loaded';
        resolve();
      };
      this._ref.onerror = (e) => {
        this._flag = 'failed';
        reject(e);
      };
    })
  }

  parseIntoBase64(target: File) {
    return new Promise<string | ArrayBuffer>((resolve, reject) => {
      const reader = new FileReader();
      if (target.type === 'application/pdf') {
        reader.readAsArrayBuffer(target);
      } else {
        reader.readAsDataURL(target);
      }
      reader.onload = () => resolve(reader.result);
      reader.onerror = (e) => {
        console.error('Reader err:', e);
        reject(e);
      };
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.file) {
      this.draw();
    }
  }

  async draw() {
    if (this.canvasRef && this.canvasRef.nativeElement) {
      try {
        const canvasPointer = this.canvasRef.nativeElement;
        const contextPointer = canvasPointer.getContext('2d');
        contextPointer.clearRect(0, 0, canvasPointer.width, canvasPointer.height);
  
        let src: string | ArrayBuffer = null;
        if (this.thumbnailRef) {
          src = this.thumbnailRef;
        }
        if (this.file && /image\/.*/.test(this.file.type) && !this.thumbOnly) {
          src = await this.parseIntoBase64(this.file);
        }
  
        if (this.file && this.file.type === 'application/pdf' && !this.thumbOnly) {
          const data = await getDocument({
            data: await this.parseIntoBase64(this.file),
            cMapUrl: this._cmaps,
            cMapPacked: true,
          }).promise;
          const targetPage = await data.getPage(1);
          const viewport = targetPage.getViewport({ scale: 1.0 });
          await targetPage.render({
            canvasContext: contextPointer,
            viewport: targetPage.getViewport({
              scale: Math.max(canvasPointer.height / viewport.height, canvasPointer.width / viewport.width),
            }),
          }).promise;
        } else if (src) {
          await this.cacheImage(src);
          
          const { dh, dw } = {
            dh: this._ref.width > this._ref.height ? (this._ref.height * canvasPointer.width / this._ref.width) : canvasPointer.height,
            dw: this._ref.width > this._ref.height ? canvasPointer.width : (this._ref.width * canvasPointer.height / this._ref.height),
          };
  
          contextPointer.drawImage(this._ref, (canvasPointer.width - dw) / 2, (canvasPointer.height - dh) / 2, dw, dh);
        }
      } catch (e) {
        if (!env.production) {
          console.error('View error:', e);
        }
      }
    }
  }

  ngAfterViewInit() {
    this.draw();
  }

  ngOnDestroy() {
    if (this._ref) {
      this._ref.remove();
    }
  }

}
