import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropzoneRemotePreviewComponent } from './dropzone-remote-preview.component';

describe('DropzoneRemotePreviewComponent', () => {
  let component: DropzoneRemotePreviewComponent;
  let fixture: ComponentFixture<DropzoneRemotePreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropzoneRemotePreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropzoneRemotePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
