import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReXyGraphComponent } from './re-xy-graph.component';

describe('ReXyGraphComponent', () => {
  let component: ReXyGraphComponent;
  let fixture: ComponentFixture<ReXyGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReXyGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReXyGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
