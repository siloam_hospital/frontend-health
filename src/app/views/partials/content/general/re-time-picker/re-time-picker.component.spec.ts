import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReTimePickerComponent } from './re-time-picker.component';

describe('ReTimePickerComponent', () => {
  let component: ReTimePickerComponent;
  let fixture: ComponentFixture<ReTimePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReTimePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReTimePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
