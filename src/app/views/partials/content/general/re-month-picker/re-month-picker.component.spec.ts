import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReMonthPickerComponent } from './re-month-picker.component';

describe('ReMonthPickerComponent', () => {
  let component: ReMonthPickerComponent;
  let fixture: ComponentFixture<ReMonthPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReMonthPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReMonthPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
