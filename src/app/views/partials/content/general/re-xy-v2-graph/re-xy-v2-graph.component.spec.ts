import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReXyV2GraphComponent } from './re-xy-v2-graph.component';

describe('ReXyV2GraphComponent', () => {
  let component: ReXyV2GraphComponent;
  let fixture: ComponentFixture<ReXyV2GraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReXyV2GraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReXyV2GraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
