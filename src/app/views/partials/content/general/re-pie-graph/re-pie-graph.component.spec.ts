import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RePieGraphComponent } from './re-pie-graph.component';

describe('RePieGraphComponent', () => {
  let component: RePieGraphComponent;
  let fixture: ComponentFixture<RePieGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RePieGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RePieGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
