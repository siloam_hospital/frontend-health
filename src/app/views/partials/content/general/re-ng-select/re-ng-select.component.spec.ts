import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReNgSelectComponent } from './re-ng-select.component';

describe('TabulatorWrapperComponent', () => {
  let component: ReNgSelectComponent;
  let fixture: ComponentFixture<ReNgSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReNgSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReNgSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
