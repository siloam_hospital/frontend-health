import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReYearPickerComponent } from './re-year-picker.component';

describe('ReYearPickerComponent', () => {
  let component: ReYearPickerComponent;
  let fixture: ComponentFixture<ReYearPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReYearPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReYearPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
