import {Component, OnInit} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ServiceDeskFrontModalComponent} from "./front-modal/front-modal.component";



@Component({
	selector: 'kt-service-desk-header',
	templateUrl: './service-desk.component.html',
	styleUrls: ['./service-desk.component.scss']
})
export class ServiceDeskComponent implements OnInit {

	constructor(
		private modalService: NgbModal
	) { }

	ngOnInit(): void {
	}

	open() {
		const modalRef = this.modalService.open(ServiceDeskFrontModalComponent, {
			size: 'xl',
			centered: true,
		});
		modalRef.componentInstance.name = 'World';
	}
}
