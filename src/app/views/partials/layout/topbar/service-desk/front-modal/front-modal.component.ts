import {Component, Input} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: 'kt-service-desk-front-modal',
	templateUrl: './front-modal.component.html',
	styleUrls: ['./front-modal.component.scss']
})
export class ServiceDeskFrontModalComponent {
	@Input() name;

	constructor(
		public activeModal: NgbActiveModal
	) {}
}
