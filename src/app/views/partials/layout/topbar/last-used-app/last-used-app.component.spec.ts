import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastUsedAppComponent } from './last-used-app.component';

describe('LastUsedAppComponent', () => {
  let component: LastUsedAppComponent;
  let fixture: ComponentFixture<LastUsedAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastUsedAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastUsedAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
