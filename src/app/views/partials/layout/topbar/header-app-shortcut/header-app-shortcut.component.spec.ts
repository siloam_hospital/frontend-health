import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderAppShortcutComponent } from './header-app-shortcut.component';

describe('HeaderAppShortcutComponent', () => {
  let component: HeaderAppShortcutComponent;
  let fixture: ComponentFixture<HeaderAppShortcutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderAppShortcutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderAppShortcutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
