import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { EventEmitterService } from '../../../../../event-emitter.service';
import { Router } from "@angular/router";
@Component({
  selector: 'kt-header-app-shortcut',
  templateUrl: './header-app-shortcut.component.html',
  styleUrls: ['./header-app-shortcut.component.scss']
})
export class HeaderAppShortcutComponent implements OnInit {
  menuList: any = [];
  module = [];

  constructor(
    private eventEmitterService: EventEmitterService,
    private cdr: ChangeDetectorRef,
    private router: Router
  ) {
    if(localStorage.getItem('recentMenu')!==null){
			this.menuList=JSON.parse(localStorage.getItem('recentMenu'));
    }
  }

  ngOnInit() {
    this.eventEmitterService.subsVar = this.eventEmitterService.
		recentModulComponentFunction.subscribe((dataUrl: any) => {
      let findIndexOf: number = this.module.findIndex(item => { 
        return '/'+item.slug == dataUrl['page'];
      })
      console.log(findIndexOf);
      console.log(this.module[findIndexOf]);
      if (findIndexOf !== -1) {
          let findIndexOf2: number = this.menuList.findIndex(item => { 
            return '/'+item.slug == dataUrl['page'];
          })
          console.log(findIndexOf2);
          if (findIndexOf2 == -1) {
            console.log(this.module[findIndexOf]);
            this.menuList.push(this.module[findIndexOf]);
            localStorage.setItem('recentMenu', JSON.stringify(this.menuList));
            this.cdr.detectChanges();
          }
      }
    });
    
  }
  gotoTab(link){
    this.router.navigate(['/'+link]);
	}

}
