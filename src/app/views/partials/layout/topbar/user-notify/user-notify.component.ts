// Angular
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
// State
import { AppState } from '../../../../../core/reducers';
import { currentUser, Logout, User } from '../../../../../core/auth';

import { AppService } from '../../../../../app.service'
import { ActivatedRoute, Router } from '@angular/router';
import { UserNotifSocket } from './user-notif-socket';
@Component({
	selector: 'kt-user-notify',
	templateUrl: './user-notify.component.html',
})
export class UserNotifyComponent implements OnInit {
	// Public properties
	user$: Observable<User>;
	userData : any = []
	dataNotif : any = []
	
	@Input() avatar: boolean = true;
	@Input() greeting: boolean = true;
	@Input() badge: boolean;
	@Input() icon: boolean;

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 */
	constructor(
		private store	: Store<AppState>,
		private AppServices : AppService,
		private route	: ActivatedRoute,
		private cdr		: ChangeDetectorRef,
		private router	: Router,
		//private socket	: UserNotifSocket,
		) {
		this.route.data.subscribe(res => {
			this.userData["employeeID"] = res["data"]["employeeID"];
		});
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.user$ = this.store.pipe(select(currentUser));
	}


	/**
	 * Log out
	 */
	logout() {
		this.store.dispatch(new Logout());
	}
}
