import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { AuthService } from '../../../../../core/auth';

@Injectable()
export class UserNotifSocket extends Socket {

    constructor(public auth: AuthService) {
        super({
            url: 'https://domain.co.id',
            options: {
                path: '/api-notif/ws',
                transports: ['websocket'],
                // upgrade: false,
                query: {
                    auth_token: auth.getToken(),
                },
            },
        });
    }
}