// Angular
import { Component, Input, OnInit } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
// State
import { AppState } from '../../../../../core/reducers';
import { currentUser, Logout, User } from '../../../../../core/auth';
import { AppService } from '../../../../../app.service';
import { ActivatedRoute,Router} from '@angular/router';
import { EventEmitterService } from '../../../../../event-emitter.service';

@Component({
	selector: 'kt-user-profile2',
	templateUrl: './user-profile2.component.html',
})
export class UserProfile2Component implements OnInit {
	// Public properties
	user$: Observable<User>;
	photo: string;
	@Input() avatar: boolean = true;
	@Input() greeting: boolean = true;
	@Input() badge: boolean;
	@Input() icon: boolean;

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 */
	constructor(private router: Router,private eventEmitterService: EventEmitterService,private store: Store<AppState>,private appService: AppService,private route: ActivatedRoute) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.user$ = this.store.pipe(select(currentUser));
		this.route.data.subscribe(routeData => {
			let data = routeData['data'];
			this.photo=data['pic']
		});
	}
	setDefaultPic() {
		this.photo = "assets/media/other/user.png";
	}
	gotoTab(name,link){
		let urldata = {
			name:name,link:link
		} 
		//this.eventEmitterService.onFirstComponentButtonClick(urldata);
		this.router.navigate(['/'+link]);
	  }
	/**
	 * Log out
	 */
	logout() {
		this.store.dispatch(new Logout());
	}
}
