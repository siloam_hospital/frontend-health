// Angular
import {
	ChangeDetectorRef,
	Component,
	Input,
	OnInit,
	ViewChild,
	ElementRef
} from '@angular/core';
// RxJS
import {
	Observable
} from 'rxjs';
// NGRX
import {
	select,
	Store
} from '@ngrx/store';
// State
import {
	AppState
} from '../../../../../core/reducers';
import {
	currentUser,
	Logout,
	User
} from '../../../../../core/auth';
import {
	NgbModule,
	NgbModal
} from "@ng-bootstrap/ng-bootstrap";


@Component({
	selector: 'kt-user-logout',
	templateUrl: './user-logout.component.html',
})
export class UserLogoutComponent implements OnInit {
	// Public properties
	timer: number = 0;
	bar: number;
	barString: string;
	user$: Observable < User > ;
	showModal: boolean = true;
	@Input() avatar: boolean = false;
	@Input() greeting: boolean = true;
	@Input() badge: boolean;
	@Input() icon: boolean;
	@ViewChild('TimeOut', {
		static: true
	}) templateRef: ElementRef;

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 */
	constructor(
		private cdr: ChangeDetectorRef,
		private modal: NgbModal,
		private modalService: NgbModal,private store: Store < AppState > ) {}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
	
	}

	/**
	 * Log out
	 */
	logout() {
		this.modal.dismissAll();
		localStorage.clear();
		this.store.dispatch(new Logout());
	}
	
}
