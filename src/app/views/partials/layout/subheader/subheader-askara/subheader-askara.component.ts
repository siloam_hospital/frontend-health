import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'kt-subheader-askara',
  templateUrl: './subheader-askara.component.html',
  styleUrls: ['./subheader-askara.component.scss']
})
export class SubheaderAskaraComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  ifUrl(url){
    if (this.router.url.indexOf(url) > -1) {
      return true;
    } else {
      return false;      
    }
  }

}
