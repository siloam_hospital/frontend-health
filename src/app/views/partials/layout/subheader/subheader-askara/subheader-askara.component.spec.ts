import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubheaderAskaraComponent } from './subheader-askara.component';

describe('SubheaderAskaraComponent', () => {
  let component: SubheaderAskaraComponent;
  let fixture: ComponentFixture<SubheaderAskaraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubheaderAskaraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubheaderAskaraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
