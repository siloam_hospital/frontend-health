// Angular
import { AfterViewInit, Component, OnInit } from '@angular/core';
// Layout
import { LayoutConfigService, ToggleOptions } from '../../../core/_base/layout';
import { HtmlClassService } from '../html-class.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
@Component({
	selector: 'kt-weather',
	templateUrl: './weather.component.html',
	styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit, AfterViewInit {
	// Public properties
	headerLogo: string;
	headerStickyLogo: string;
	geolocationPosition: any;
	nameLocation: string;
	celcius:any;
	toggleOptions: ToggleOptions = {
		target: 'body',
		targetState: 'kt-aside--minimize',
		togglerState: 'kt-aside__brand-aside-toggler--active'
	};

	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 * @param htmlClassService: HtmlClassService
	 */
	constructor(private http: HttpClient,private layoutConfigService: LayoutConfigService, public htmlClassService: HtmlClassService) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.headerLogo = this.layoutConfigService.getLogo();
		this.headerStickyLogo = this.layoutConfigService.getStickyLogo();
		/*
		if (window.navigator && window.navigator.geolocation) {
			window.navigator.geolocation.getCurrentPosition(
				position => {
					this.geolocationPosition = position,
					console.log(position)
					
						let httpOptions = {
							headers: new HttpHeaders({
							  'Content-Type': 'application/json;charset=utf-8',
							})
						};

						this.http.post(environment.APIUrl + "weather/curent",{
							'latitude':position['coords']['latitude'],
							'longitude':position['coords']['longitude']
						},httpOptions).subscribe(response => {
							console.log(response);
							if(response['status']){
								this.nameLocation=response['data']['name'];
								let temp = response['data']['main']['temp'];
								this.celcius = ( temp - 273.15).toFixed(2);
							}
						})

				},
				error => {
					switch (error.code) {
						case 1:
							console.log('Permission Denied');
							break;
						case 2:
							console.log('Position Unavailable');
							break;
						case 3:
							console.log('Timeout');
							break;
					}
				}
			);
		};
		*/
	}



	/**
	 * On destroy
	 */
	ngAfterViewInit(): void {

	}
}
