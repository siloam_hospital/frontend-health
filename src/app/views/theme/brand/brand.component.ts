// Angular
import {
	ChangeDetectorRef,
	AfterViewInit,
	Component,
	OnInit
} from '@angular/core';
// Layout
import {
	LayoutConfigService,
	ToggleOptions
} from '../../../core/_base/layout';
import {
	HtmlClassService
} from '../html-class.service';
import {
	HttpClient,
	HttpHeaders
} from '@angular/common/http';
import {
	environment
} from '../../../../environments/environment';
@Component({
	selector: 'kt-brand',
	templateUrl: './brand.component.html',
	styleUrls: ['./brand.component.scss']
})

export class BrandComponent implements OnInit, AfterViewInit {
	// Public properties
	headerLogo: string;
	headerStickyLogo: string;
	geolocationPosition: any;
	nameLocation: string;
	celcius: any;
	date: Date;
	toggleOptions: ToggleOptions = {
		target: 'body',
		targetState: 'kt-aside--minimize',
		togglerState: 'kt-aside__brand-aside-toggler--active'
	};

	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 * @param htmlClassService: HtmlClassService
	 */
	constructor(private cdr: ChangeDetectorRef,private http: HttpClient, private layoutConfigService: LayoutConfigService, public htmlClassService: HtmlClassService) {}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		/*
		let httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json;charset=utf-8',
			})
		};
		
		this.http.post(environment.APIUrl + "weather/curent", {
			'latitude':'1',
			'longitude': '1',
		}, httpOptions).subscribe(response => {
			 console.log(response);
			if (response['status']) {
				this.date = new Date();
				console.log(response['data']['data']['0']['location']['name']);
				this.nameLocation = response['data']['data']['0']['location']['name'];
				this.celcius = response['data']['temp'];
				this.cdr.detectChanges();
			}
		})
		*/

		this.headerLogo = this.layoutConfigService.getLogo();
		this.headerStickyLogo = this.layoutConfigService.getStickyLogo();
		/*
		if (window.navigator && window.navigator.geolocation) {
			window.navigator.geolocation.getCurrentPosition(
				position => {
					this.geolocationPosition = position;
					console.log(position);
			

					this.http.post(environment.APIUrl + "weather/curent", {
						'latitude': position['coords']['latitude'],
						'longitude': position['coords']['longitude']
					}, httpOptions).subscribe(response => {
						 console.log(response);
						if (response['status']) {
							this.date = new Date();
							console.log(response['data']['data']['0']['location']['name']);
							this.nameLocation = response['data']['data']['0']['location']['name'];
							this.celcius = response['data']['temp'];
							this.cdr.detectChanges();
						}
					})

				},
				error => {
					switch (error.code) {
						case 1:
							console.log('Permission Denied');
							break;
						case 2:
							console.log('Position Unavailable');
							break;
						case 3:
							console.log('Timeout');
							break;
					}
				}
			);
		};
		*/
	}



	/**
	 * On destroy
	 */
	ngAfterViewInit(): void {

	}
}
