// Angular
import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
// Layout
import { LayoutConfigService } from '../../../core/_base/layout';
// Object-Path
import * as objectPath from 'object-path';
import { ResizeService } from '../../../../app/size-detector/resize.service';
import { delay } from 'rxjs/operators';
import { ActivatedRoute,Router} from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../core/reducers';
import { NgbModule,NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Logout } from '../../../core/auth';
@Component({
	selector: 'kt-footer',
	templateUrl: './footer.component.html',
})
export class FooterComponent implements OnInit {
	// Public properties
	today: number = Date.now();
	fluid: boolean;
	device:string;

	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayouConfigService
	 */
	constructor(private modal: NgbModal,private modalService: NgbModal,private store: Store < AppState >,private router: Router,private layoutConfigService: LayoutConfigService,private resizeSvc: ResizeService,private route: ActivatedRoute,private cdr: ChangeDetectorRef) {
	
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		const config = this.layoutConfigService.getConfig();

		// footer width fluid
		this.fluid = objectPath.get(config, 'footer.self.width') === 'fluid';
	}
	gotoTab(link){
    	this.router.navigate(['/'+link]);
	}
	logout() {
		this.modal.dismissAll();
		localStorage.clear();
		this.store.dispatch(new Logout());
	}
}
