import { AfterViewInit,Component, OnInit,ElementRef,ViewChild,ChangeDetectorRef } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { AppService } from "../../../../app.service";
import { Router } from '@angular/router';
import moment from 'moment';
@Component({
  selector: 'kt-complete',
  templateUrl: './complete.component.html',
  styleUrls: ['./complete.component.scss']
})
export class CompleteComponent implements OnInit {
  complete_id:number=null;
  info=null;
  constructor(private cdr: ChangeDetectorRef,private translate: TranslateService,private router: Router,private appService: AppService) {
    if(localStorage.getItem("complete_id")==null) this.router.navigate(['/']);
    this.complete_id=Number(localStorage.getItem("complete_id"));
  }
  ngOnInit(): void {
    this.appService.post("form-web/personal",{complete_id:this.complete_id}).toPromise().then(res => {
      console.log(res);
      this.info=res['data'].length>0?res['data'][0]:null;
      console.log(this.info);
      this.cdr.detectChanges();
		})
  }

  addoneDay(date){
    return moment(date).add(1, 'days');
  }

}
