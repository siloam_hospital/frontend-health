import { PartialsModule } from "../../partials/partials.module";
import { TranslateModule } from '@ngx-translate/core';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDropzoneModule } from "ngx-dropzone";
import { RouterModule } from "@angular/router";
import { CoreModule } from "../../../core/core.module";
import { UserResolver } from "../../../core/auth/_data-sources/user.resolver";
import { NgbModule, NgbDateParserFormatter } from "@ng-bootstrap/ng-bootstrap";
import { MatDatepickerModule, MatDatepickerIntl } from '@angular/material/datepicker';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatBadgeModule } from "@angular/material/badge";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatCardModule } from "@angular/material/card";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatChipsModule } from "@angular/material/chips";
import { MatNativeDateModule, MatRippleModule } from "@angular/material/core";
import { NgSelectModule } from "@ng-select/ng-select";
import { 
  //MatDatepickerModule,
  MatDialogModule } from "@angular/material/dialog";
  import { MatExpansionModule } from "@angular/material/expansion";
  import { MatGridListModule } from "@angular/material/grid-list";
  import { MatIconModule } from "@angular/material/icon";
  import { MatInputModule } from "@angular/material/input";
  import { MatListModule } from "@angular/material/list";
  import { MatMenuModule } from "@angular/material/menu";
  import { MatProgressBarModule } from "@angular/material/progress-bar";
  import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
  import { MatRadioModule } from "@angular/material/radio";
  import { MatSelectModule } from "@angular/material/select";
  import { MatSidenavModule } from "@angular/material/sidenav";
  import { MatSlideToggleModule } from "@angular/material/slide-toggle";
  import { MatSliderModule } from "@angular/material/slider";
  import { MatSnackBarModule } from "@angular/material/snack-bar";
  import { MatSortModule } from "@angular/material/sort";
  import { MatTableModule } from "@angular/material/table";
  import { MatTabsModule } from "@angular/material/tabs";
  import { MatToolbarModule } from "@angular/material/toolbar";
  import { MatTooltipModule } from "@angular/material/tooltip";

  import { DashboardModule } from './dashboard/dashboard.component';
  import { PersonalComponent } from './personal/personal.component';
  import { CompleteComponent } from './complete/complete.component';
@NgModule({
	exports: [
		MatAutocompleteModule,
		MatButtonModule,
		MatButtonToggleModule,
		MatCardModule,
		MatCheckboxModule,
		MatChipsModule,
		MatDatepickerModule,
		MatDialogModule,
		MatExpansionModule,
		MatGridListModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatMenuModule,
		MatProgressBarModule,
		MatProgressSpinnerModule,
		MatRadioModule,
		MatRippleModule,
		MatSelectModule,
		MatSidenavModule,
		MatSlideToggleModule,
		MatSliderModule,
		MatSnackBarModule,
		MatTabsModule,
		MatToolbarModule,
		MatTooltipModule,
		MatNativeDateModule,
		MatBadgeModule,
		MatPaginatorModule,
		MatTableModule,
		MatSortModule
	],
	declarations: [],
})
export class MaterialModule {}

@NgModule({
  declarations: [DashboardModule,PersonalComponent,CompleteComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardModule,
        //resolve: { data: UserResolver},
      },
      {
        path: 'personal',
        component: PersonalComponent,
        //resolve: { data: UserResolver},
      },
      {
        path:'complete',
        component:CompleteComponent
      }
    ]),
    NgbModule,
    TranslateModule.forChild(),
    PartialsModule,
    MaterialModule,
    NgSelectModule
  ],
  exports: [
  ],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeModule { }
