import { Component, OnInit } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: 'kt-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardModule implements OnInit {
	constructor(private translate: TranslateService, ) {

	}

	ngOnInit() {

	}
}
