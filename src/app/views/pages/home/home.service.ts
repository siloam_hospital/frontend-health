import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";
import { EncrDecrService } from "../../../core/EncrDecr/encr-decr.service";
import { catchError, map } from 'rxjs/operators';
@Injectable({
	providedIn: "root"
})

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient,private EncrDecr: EncrDecrService) { }
  
}
