import { AfterViewInit,Component, OnInit,ElementRef,ViewChild,ChangeDetectorRef } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { FormBuilder, FormGroup,Validators} from "@angular/forms";
import { AppService } from "../../../../app.service";
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
	selector: 'kt-personal',
	templateUrl: './personal.component.html',
	styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit, AfterViewInit {
	@ViewChild('wizard', {
		static: true
	}) el: ElementRef;
	f_personal: FormGroup;
	master_hospitals = [];
	master_hospitals_loading: boolean = true;
	master_gender = []
	formStep = {
		0: ['name', 'gender', 'date_birth', 'email', 'phone', 'id_hospital'],
		1: ['reactive_result'],
		2: ['fever', 'breathing', 'cought', 'runny_nose', 'smell', 'taste', 'aches', 'vomiting','agreement'],
		3: ['live_someone','close_contact','with_someone','close_contact_recovered','treating_patients']
	}
	constructor(private router: Router, private translate: TranslateService, private fb: FormBuilder, private appService: AppService, private cdr: ChangeDetectorRef) {
		
	}

	ngOnInit(): void {
		this.master_gender = [{
				id: 0,
				name: this.translate.instant("GENDER.MALE")
			},
			{
				id: 1,
				name: this.translate.instant("GENDER.FEMALE")
			}
		]
		this.appService.get("form-web/hospitals").toPromise().then(res => {
			this.master_hospitals = res['data'];
			this.master_hospitals_loading = false;
			this.cdr.detectChanges();
		})
		this.create_form();
	}
	create_form(data = null) {
		this.f_personal = this.fb.group({
			name: [
				data == null ? null : data.name,
				Validators.compose([
					Validators.required
				])
			],
			gender: [
				data == null ? null : data.gender,
				Validators.compose([
					Validators.required
				])
			],
			date_birth: [
				data == null ? null : data.date_birth,
				Validators.compose([
					Validators.required
				])
			],
			email: [
				data == null ? null : data.email,
				Validators.compose([
					Validators.required,
					Validators.email
				])
			],
			phone: [
				data == null ? null : data.phone,
				Validators.compose([
					Validators.required,
				])
			],
			id_hospital: [
				data == null ? null : data.id_hospital,
				Validators.compose([
					Validators.required
				])
			], //STEP 2
			reactive_result: [
				data == null ? true : data.reactive_result,
				Validators.compose([
					Validators.required
				])
			], //STEP 2
			fever: [
				data == null ? true : data.fever,
				Validators.compose([
					Validators.required,
				])
			],
			breathing: [
				data == null ? true : data.breathing,
				Validators.compose([
					Validators.required,
				])
			],
			cought: [
				data == null ? true : data.cought,
				Validators.compose([
					Validators.required,
				])
			],
			runny_nose: [
				data == null ? true : data.runny_nose,
				Validators.compose([
					Validators.required,
				])
			],
			smell: [
				data == null ? true : data.smell,
				Validators.compose([
					Validators.required,
				])
			],
			taste: [
				data == null ? true : data.taste,
				Validators.compose([
					Validators.required,
				])
			],
			aches: [
				data == null ? true : data.aches,
				Validators.compose([
					Validators.required,
				])
			],
			vomiting: [
				data == null ? true : data.vomiting,
				Validators.compose([
					Validators.required,
				])
			],
			live_someone:[true],
			close_contact:[true],
			with_someone:[true],
			close_contact_recovered:[true],
			treating_patients:[true],
			agreement: [
				data == null ? null : data.agreement,
				Validators.compose([
					Validators.required,
				])
			]
		})
	}
	ngAfterViewInit(): void {
		// Initialize form wizard
		const wizard = new KTWizard(this.el.nativeElement, {
			startStep: 1
		});

		// Validation before going to next page
		let report = this.f_personal['controls'];
		let cdr = this.cdr;
		let formStep = this.formStep;
		wizard.on('beforeNext', function (wizardObj) {
			// if (wizardObj.currentStep == 1) {
			// 	formStep = ['name', 'gender', 'date_birth', 'phone', 'hospitals'];
			// } else if (wizardObj.currentStep == 2) {
			// 	formStep = ['reactive_result'];
			// } else if (wizardObj.currentStep == 3) {
			// 	formStep = ['fever', 'breathing', 'cought', 'runny_nose', 'smell', 'taste', 'aches', 'vomiting', 'agreement'];
			// }
			if (wizardObj.currentStep != 4) {
				Object.keys(formStep[wizardObj.currentStep - 1]).forEach(key => {
					if (!report[formStep[wizardObj.currentStep - 1][key]]['valid']) {
						report[formStep[wizardObj.currentStep - 1][key]].markAsTouched()
						wizardObj.stop();
					}
				})
			}
			cdr.detectChanges();
		});

		// Change event
		wizard.on('change', function (wizard) {
			setTimeout(function () {
				KTUtil.scrollTop();
			}, 500);
		});
	}

	isControlHasError(form: string, controlName: string, validationType: string): boolean {
		let control;
		if (form == 'f_personal') {
			control = this.f_personal.controls[controlName];
		}
		if (!control) {
			return false;
		}
		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}
	onSubmit() {
		let f_personal = this.f_personal.controls;
		let body = {};
		if (this.f_personal.invalid) {
			Object.keys(f_personal).forEach(controlName => f_personal[controlName].markAsTouched());
			return;
		}
		body['personal'] = {};
		body['symptoms'] = {};
		Object.keys(this.formStep).forEach(key => {
			Object.keys(this.formStep[key]).forEach(key2 => {
				if (Number(key) == 0) {
					if (this.formStep[key][key2] == 'gender' || this.formStep[key][key2] == 'id_hospital') {
						body['personal'][this.formStep[key][key2]] = f_personal[this.formStep[key][key2]].value['id']
					} else if (this.formStep[key][key2] == 'date_birth') {
						let newDate = f_personal[this.formStep[key][key2]].value;
						body['personal'][this.formStep[key][key2]] = new Date(newDate['year'], newDate['month'], newDate['day']).toISOString().slice(0, 10)
					} else {
						body['personal'][this.formStep[key][key2]] = f_personal[this.formStep[key][key2]].value
					}
				} else {
					body['symptoms'][this.formStep[key][key2]] = f_personal[this.formStep[key][key2]].value
				}
			})
		})
		console.log(body);
		this.appService.post("form-web/health", body).toPromise().then(res => {
			console.log(res);
			Swal.fire({
				title: res['message'],
				icon: res['status'] == false ? "error" : "success",
				onClose: () => {
					if (res['status']) {
						localStorage.setItem("complete_id", res['data'])
						this.router.navigate(['/complete']);
					}
				}
			})
		})
	}
}
