import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './error.component';
import { RouterModule } from '@angular/router';
import { UserResolver } from "../../../core/auth/_data-sources/user.resolver";
@NgModule({
  declarations: [
    ErrorComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
				path: "",
				component: ErrorComponent,
			//	resolve: { data: UserResolver }
			}
    ])
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ErrorModule { }
