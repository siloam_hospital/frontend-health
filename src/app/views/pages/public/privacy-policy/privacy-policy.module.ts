import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivacyPolicyComponent } from './privacy-policy.component';
import { RouterModule } from '@angular/router';
import { UserResolver } from "../../../../core/auth/_data-sources/user.resolver";
@NgModule({
  declarations: [
    PrivacyPolicyComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
				path: "",
				component: PrivacyPolicyComponent,
				resolve: { data: UserResolver }
			}
    ])
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrivacyPolicyModule { }
