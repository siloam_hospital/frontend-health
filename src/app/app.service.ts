


import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, map } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { EncrDecrService } from "../app/core/EncrDecr/encr-decr.service";

@Injectable({
	providedIn: "root"
})
export class AppService {
	httpOptions: any = [];
	apiToken:any;

	constructor(private http: HttpClient,private EncrDecr: EncrDecrService) {
	  this.apiToken = this.EncrDecr.get(localStorage.getItem(environment.authTokenKey));
  
	  this.httpOptions = {
		headers: new HttpHeaders({
		  'Content-Type': 'application/json;charset=utf-8',
		  //'Authorization': 'Bearer '+environment.DBToken,
		})
	  };
	}
	post(endPoint,body): Observable<any> {
		let type=false;
		let dataBody=body;
		environment.APIUrl.split("/").findIndex(item => {
			if (item === "v2") {
				type=true;
				dataBody={data:this.EncrDecr.bodyEncrypt(body,type)};
				return true;
			}
		})
	  return this.http.post<any>(environment.APIUrl + endPoint, dataBody,this.httpOptions).pipe(
		map((res) => {
			return this.EncrDecr.response(res);
		}),
		catchError(err => {
			return err;
		})
	);
	}
	get(endPoint): Observable<any> {
	  return this.http.get<any>(environment.APIUrl + endPoint,this.httpOptions).pipe(
		map((res) => {
			console.log("ok");
			
			return this.EncrDecr.response(res);
		}),
		catchError(err => {
			return err;
		})
	);
	}

	delete(endPoint): Observable<any> {
		return this.http.delete<any>(environment.APIUrl + endPoint,this.httpOptions).pipe(
		  map((res) => {
			  return this.EncrDecr.response(res);
		  }),
		  catchError(err => {
			  return err;
		  })
	  );
	  }
	uploadSingleImage(image, userId): Observable<any> {
		return this.http.post(
			environment.APIUrl + "upload/image/" + userId,
			image
		).pipe(
			map((res) => {
				return this.EncrDecr.response(res);
			}),
			catchError(err => {
				return err;
			})
		);
  }
  uploadAllImage(image, userId): Observable<any> {
	return this.http.post(
		environment.APIUrl + "upload/images/",
		image
	).pipe(
		map((res) => {
			return this.EncrDecr.response(res);
		}),
		catchError(err => {
			return err;
		})
	);
}
  }