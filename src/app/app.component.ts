import { Subscription } from 'rxjs';
// Angular
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit,Input } from '@angular/core';
import { NavigationEnd, NavigationError, Router } from '@angular/router';
// Layout
import { LayoutConfigService, SplashScreenService, TranslationService } from './core/_base/layout';
// language list
import { locale as idLang } from './core/_config/i18n/id';
import { locale as enLang } from './core/_config/i18n/en';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { LocationService } from './core/_base/crud/utils/location.service';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'body[kt-root]',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {
	title = 'Siloam Hospitals';
	loader: boolean;
	development: boolean = true;
	private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param translationService: TranslationService
	 * @param router: Router
	 * @param layoutConfigService: LayoutCongifService
	 * @param splashScreenService: SplashScreenService
	 */
	constructor(private translationService: TranslationService,
		private router: Router,
		private layoutConfigService: LayoutConfigService,
		private splashScreenService: SplashScreenService,
		private modal: NgbModal,
		private modalService: NgbModal,
		private locationSvc: LocationService,
	) {
		// this.locationSvc.startWatch();
		// register translations
		this.translationService.loadTranslations(idLang, enLang);
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		// this.locationSvc.position$.subscribe(v => {
		// 	console.log('Pos:', v);
		// });
		if (!this.locationSvc.isStarted) {
			this.locationSvc.startWatch();
		}
		// enable/disable loader
		this.loader = this.layoutConfigService.getConfig('loader.enabled');

		const routerSubscription = this.router.events.subscribe(event => {
			if (event instanceof NavigationError) {
				console.error('Navigation Error:', event);
				this.router.navigate(['/error']);
			} else if (event instanceof NavigationEnd) {
				// hide splash screen
				this.splashScreenService.hide();

				// scroll to top on every route change
				window.scrollTo(0, 0);

				// to display back the body content
				setTimeout(() => {
					document.body.classList.add('kt-page--loaded');

				}, 500);

			}
		});
		this.unsubscribe.push(routerSubscription);

	}
	ngOnDestroy() {
		this.unsubscribe.forEach(sb => sb.unsubscribe());
	}
}
