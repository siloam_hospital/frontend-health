import { SCREEN_SIZE } from './screen-size.enum';

export enum ScreenType {
    XS = 'xs',
    SM = 'sm',
    MD = 'md',
    LG = 'lg',
    XL = 'xl',
}

export enum DeviceType {
    MOBILE = 'mobile',
    TABLET = 'desktop',
    DESKTOP = 'desktop',
}

export interface IScreenSize {
    id: SCREEN_SIZE;
    name: ScreenType | string;
    css: string;
    device: DeviceType | string;
}
