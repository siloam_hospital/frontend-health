import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, first } from 'rxjs/operators';
import { IScreenSize, ScreenType, DeviceType } from './screen-size.interface';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
// import { SCREEN_SIZE } from './screen-size.enum';
// import { DeviceDetectorService } from 'ngx-device-detector';

@Injectable()
export class ResizeService {

  get onResize$(): Observable<IScreenSize> {
    return this.resizeSubject.asObservable().pipe(distinctUntilChanged());
  }

  private resizeSubject: Subject<IScreenSize>;
  private now: IScreenSize;

  constructor(private bo: BreakpointObserver) {
    this.resizeSubject = new Subject();
    // this.bo.observe([
    //   Breakpoints.XSmall,
    //   Breakpoints.Small,
    //   Breakpoints.Medium,
    //   Breakpoints.Large,
    //   Breakpoints.XLarge,
    // ]).pipe(
    //   map((v): IScreenSize => {
    //     if (v.breakpoints[Breakpoints.XSmall]) {
    //       return {
    //         id: SCREEN_SIZE.XS,
    //         name: ScreenType.XS,
    //         css: 'd-block d-sm-none',
    //         device: DeviceType.MOBILE,
    //       };
    //     } else if (v.breakpoints[Breakpoints.Small]) {
    //       return {
    //         id: SCREEN_SIZE.SM,
    //         name: ScreenType.SM,
    //         css: 'd-none d-sm-block d-md-none',
    //         device: DeviceType.MOBILE,
    //       };
    //     } else if (v.breakpoints[Breakpoints.Medium]) {
    //       return {
    //         id: SCREEN_SIZE.MD,
    //         name: ScreenType.MD,
    //         css: 'd-none d-md-block d-lg-none',
    //         device: DeviceType.TABLET,
    //       };
    //     } else if (v.breakpoints[Breakpoints.Large]) {
    //       return {
    //         id: SCREEN_SIZE.LG,
    //         name: ScreenType.LG,
    //         css: 'd-none d-lg-block d-xl-none',
    //         device: DeviceType.DESKTOP,
    //       };
    //     } else {
    //       return {
    //         id: SCREEN_SIZE.XL,
    //         name: ScreenType.XL,
    //         css: 'd-none d-xl-block',
    //         device: DeviceType.DESKTOP,
    //       };
    //     }
    //   })
    // ).subscribe(this.resizeSubject);
    this.now = null;
  }
  //onResize(size: SCREEN_SIZE) {
  onResize(size: IScreenSize) {
    // if (this.devSvc.isDesktop()) {
    //   size.device = 'desktop';
    // }
    //  if (this.devSvc.isTablet()) {
    //   size.device = 'tablet';
    // }
    //  if (this.devSvc.isMobile()) {
    //   size.device = 'mobile';
    // }
    // console.log('Device Info:', this.devSvc.getDeviceInfo());
    this.now = size;
    this.resizeSubject.next(size);
  }

  currentSize(): IScreenSize {
    // if (this.bo.isMatched(Breakpoints.XSmall)) {
    //   return {
    //     id: SCREEN_SIZE.XS,
    //     name: ScreenType.XS,
    //     css: 'd-block d-sm-none',
    //     device: DeviceType.MOBILE,
    //   };
    // } else if (this.bo.isMatched(Breakpoints.Small)) {
    //   return {
    //     id: SCREEN_SIZE.SM,
    //     name: ScreenType.SM,
    //     css: 'd-none d-sm-block d-md-none',
    //     device: DeviceType.MOBILE,
    //   };
    // } else if (this.bo.isMatched(Breakpoints.Medium)) {
    //   return {
    //     id: SCREEN_SIZE.MD,
    //     name: ScreenType.MD,
    //     css: 'd-none d-md-block d-lg-none',
    //     device: DeviceType.TABLET,
    //   };
    // } else if (this.bo.isMatched(Breakpoints.Large)) {
    //   return {
    //     id: SCREEN_SIZE.LG,
    //     name: ScreenType.LG,
    //     css: 'd-none d-lg-block d-xl-none',
    //     device: DeviceType.DESKTOP,
    //   };
    // } else {
    //   return {
    //     id: SCREEN_SIZE.XL,
    //     name: ScreenType.XL,
    //     css: 'd-none d-xl-block',
    //     device: DeviceType.DESKTOP,
    //   };
    // }
    return this.now;
  }
}