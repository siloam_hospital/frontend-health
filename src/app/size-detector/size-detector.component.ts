import { Component, ViewChild, ElementRef, AfterViewInit, HostListener } from '@angular/core';
import { ResizeService } from './resize.service';
import { SCREEN_SIZE } from './screen-size.enum';

@Component({
  selector: 'app-size-detector',
  templateUrl: './size-detector.component.html',
  styleUrls: ['./size-detector.component.scss']
})
export class SizeDetectorComponent implements AfterViewInit {
  prefix = 'is-';
  sizes = [
    {
      id: SCREEN_SIZE.XS, name: 'xs',
      css: `d-block d-sm-none`,
      device:'mobile'
    },
    {
      id: SCREEN_SIZE.SM, name: 'sm',
      css: `d-none d-sm-block d-md-none`,
      device:'mobile'
    },
    {
      id: SCREEN_SIZE.MD, name: 'md',
      css: `d-none d-md-block d-lg-none`,
      device:'tablet'
    },
    {
      id: SCREEN_SIZE.LG, name: 'lg',
      css: `d-none d-lg-block d-xl-none`,
      device:'desktop'
    },
    {
      id: SCREEN_SIZE.XL, name: 'xl',
      css: `d-none d-xl-block`,
      device:'desktop'
    },
  ];

  constructor(private elementRef: ElementRef<HTMLDivElement>, private resizeSvc: ResizeService) { }

  @HostListener("window:resize", [])
  private onResize() {
    this.detectScreenSize();
  }

  ngAfterViewInit() {
    this.detectScreenSize();
  }

  private detectScreenSize() {
    const currentSize = this.sizes.find(x => {
      const el = this.elementRef.nativeElement.querySelector(`.${this.prefix}${x.id}`);
      const isVisible = window.getComputedStyle(el).display != 'none';

      return isVisible;
    });
    // Patch for orientation bug
    // let base = getComputedStyle(document.documentElement);
    // let width = parseInt(base.width, 10);
    // let height = parseInt(base.height, 10);
    // console.log('Pre-check:', { width, height, 'width > height': width > height });
    // if (width > height) {
    //   console.log('Force size to desktop');
    //   currentSize.device = 'desktop';
    // }
    this.resizeSvc.onResize(currentSize);
  }

}