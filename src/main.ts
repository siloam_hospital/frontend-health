import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
/*
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
  if(window){
    console.log('%c NEED SOME INFORMATION ? CONTACT US TEAM IT PTBA ! ', 'background: #222; color: #FF0000');
    window.console.log=function(){};
  }
}*/
if (location.hostname != 'localhost') {
  window.console.log = () => { }
  window.console.warn = () => { }
  window.console.error = () => { }
  window.console.time = () => { }
  window.console.timeEnd = () => { }
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
