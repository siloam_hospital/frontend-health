import * as webpack from 'webpack';

export default {
    optimization: {
        concatenateModules: false,
    },
    cache: true,
    node: {
        crypto: true,
        stream: true,
    }
} as webpack.Configuration;
